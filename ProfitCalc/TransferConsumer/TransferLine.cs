﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransferConsumer
{
    public abstract class TransferLine
    {
        public string rawLine = string.Empty;

        public string sku = string.Empty;
        public string identifier = string.Empty;
        public string currency = string.Empty;

        public double price = 0;
        public double discount = 0;
        public int taxRate = 0;

        public int stock = 0;

        public int length = 0;
        public int width = 0;
        public int height = 0;

        public int weight = 0;

        public int packingUnit = 1;
        public int daysToShipment = 1; 

        public priceTypeEnum priceTypeEnum = priceTypeEnum.fixedPrice; 
    }
}
