﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransferConsumer
{
    public class GenericTransfer: TransferFile
    {
        public TransferConsumerTransferType transferConsumerTransferType; 

        public GenericTransfer(string url, TransferConsumerTransferType transferConsumerTransferType)
        {
            this.transferConsumerTransferType = transferConsumerTransferType; 

            if (string.IsNullOrEmpty(fileContent))
            {
                string path = url;
                if (transferConsumerTransferType == TransferConsumerTransferType.EAN)
                {
                    FillEanTransferLines(path);
                }
                else if (transferConsumerTransferType == TransferConsumerTransferType.ASIN)
                {
                    FillAsinTransferLines(path);
                }
            }
        }
    }
}
