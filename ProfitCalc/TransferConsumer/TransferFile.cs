﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace TransferConsumer
{
    public abstract class TransferFile
    {
        public string fileContent = string.Empty;
        public List<TransferLine> transferLines = new List<TransferLine>();

        protected void FillEanTransferLines(string path)
        {
            WebClient wc = new WebClient();
            transferLines.AddRange(wc.DownloadString(path).Trim().Replace("\r\n", "\r").Replace("\n", "\r").Split('\r').Select(x => new EanTransferLine(x)).Where(x => x.rawLine.Trim().Length > 0).ToList());
        }

        protected void FillAsinTransferLines(string path)
        {
            WebClient wc = new WebClient();
            string file = wc.DownloadString(path).Trim(); 
            transferLines.AddRange(file.Replace("\r\n", "\r").Replace("\n", "\r").Split('\r').Select(x => new AsinTransferLine(x)).Where(x => x.rawLine.Trim().Length > 0).ToList());
        }

        public List<string> GetListOfIdentifier()
        {
            return transferLines.Select(x => x.identifier).ToList();
        }


    }
}
