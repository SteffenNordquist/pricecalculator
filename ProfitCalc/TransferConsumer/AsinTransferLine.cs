﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TransferConsumer
{
    public class AsinTransferLine: TransferLine
    {

        public AsinTransferLine(string line)
        {
            rawLine = line;
            if (!string.IsNullOrEmpty(rawLine))
            {
                var tokens = line.Split('\t');

                sku = tokens[0];
                identifier = tokens[1];
                stock = int.Parse(tokens[2]);
                currency = tokens[3];
                price = double.Parse(tokens[4], CultureInfo.InvariantCulture);
                discount = double.Parse(tokens[5], CultureInfo.InstalledUICulture);
                taxRate = int.Parse(tokens[6]);

                int.TryParse(tokens[8], out length);
                int.TryParse(tokens[9], out width);
                int.TryParse(tokens[10], out height);
                int.TryParse(tokens[11], out weight);

                if (tokens[12] == "02")
                {
                    priceTypeEnum = priceTypeEnum.flexiblePrice;
                }

                daysToShipment = int.Parse(tokens[13]);
                packingUnit = int.Parse(tokens[14]);

            }
            
        }
    }
}
