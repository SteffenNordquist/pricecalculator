﻿using DN_Classes.Products;
using PriceCalculation.Shipping;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PriceTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int length = int.Parse(textBox1.Text);
                int width = int.Parse(textBox2.Text);
                int height = int.Parse(textBox3.Text);
                int weight = int.Parse(textBox4.Text);

                double buyPrice = double.Parse(textBox5.Text.Replace(",", "."), CultureInfo.InvariantCulture);

                ShippingCostFinder shippingCostFinder = new ShippingCostFinder(new SimpleSize(length, width, height, weight), buyPrice, checkBox1.Checked);
                var variation = shippingCostFinder.getShippingVariation();

                label5.Text = variation.name;
                label6.Text = variation.price.ToString();
            }
            catch
            { }

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
