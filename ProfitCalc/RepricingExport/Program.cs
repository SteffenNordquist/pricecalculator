﻿using AmazonRepricerDll;
using DN_Classes.AppStatus;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace RepricingExport
{
    class Program
    {
        public static MongoClient Client = null;
        public static MongoServer Server = null;
        public static MongoDatabase PDatabase = null;

        public static MongoCollection<RepricerEntity> mongoCollection;



        static void Main(string[] args)
        {
            using (ApplicationStatus appStatus = new ApplicationStatus("Repricing_RepricerExport"))
            {
                try
                {
                    Client = new MongoClient(Properties.Settings.Default.MongoString);
                    Server = Client.GetServer();
                    PDatabase = Server.GetDatabase("RepricerCollection");
                    mongoCollection = PDatabase.GetCollection<RepricerEntity>("flexibleitems");

                    var allNewBees = mongoCollection.FindAll().Where(x => x.lastPriceDate >= DateTime.Now.AddDays(-2)).Select(x => x.asin + "\t" + x.ean + "\t" + x.price.ToString(CultureInfo.InvariantCulture)).ToList();

                    File.WriteAllLines(Properties.Settings.Default.Path, allNewBees);
                    appStatus.Successful();
                }
                catch (Exception e)
                {
                    appStatus.AddMessagLine(e.ToString());
                }
            }

        }
    }
}
