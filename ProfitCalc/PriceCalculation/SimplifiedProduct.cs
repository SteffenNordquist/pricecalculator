﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class SimplifiedProduct: Product
    {
        public SimplifiedProduct(string line)
        {
            var tokens = line.Split();

            identifier = tokens[1];
            quantity = int.Parse(tokens[2]);
            currency = tokens[3];

            price = double.Parse(tokens[4], CultureInfo.InvariantCulture);
            discountRate = double.Parse(tokens[5], CultureInfo.InvariantCulture);
            taxRate = double.Parse(tokens[6], CultureInfo.InvariantCulture);

            int.TryParse(tokens[11], out weight);

            if (tokens[12] == "04")
            {
                priceType = PriceTypeCalc.fixPrice;
            }
            else
            {
                priceType = PriceTypeCalc.flexiblePrice;
            }
        }
    }
}
