﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class ShippingType
    {
        string type = "undefined";
        public double price = 0.0;

        public int maxPackageWeigt = 600;


        double brief500 = 1.13;
        double brief1000 = 1.93;

        double dpd1000 = 2.63;


        bool bookSendingPossible = false;

        public ShippingType(Product product)
        {
            string packingName = product.productSize.packing.name;
            //Brief
            if (product.productSize.Height < 20 && product.productSize.Weight < 500 && (packingName != "" && packingName != "K/10"))
            {
                if (product.buyPrice < 25)
                {
                    type = "Brief";
                    price = brief500;
                }
                else
                {
                    type = string.Format("Paket_{0}_1000", packingName);
                    price = dpd1000;
                }
            }
            //Maxibrief
            else if (product.productSize.Height < 50 && product.productSize.Weight < 1000 && (packingName != "" && packingName != "K/10"))
            {
                if (product.buyPrice < 25)
                {
                    type = "Brief Maxi";
                    price = brief1000;
                }
                else
                {
                    type = string.Format("Paket_{0}_1000", packingName);
                    price = dpd1000;
                }
            }
            else if (product.productSize.Height >= 50 && product.productSize.Weight < 500 && (packingName != "" && packingName != "K/10") && product.priceType == PriceTypeCalc.fixPrice)
            {
                if (product.buyPrice < 25)
                {
                    type = "Büchersendung";
                    price = 1;
                }
                else
                {
                    type = string.Format("Paket_{0}_1000", packingName);
                    price = dpd1000;
                }
            }
            else if (product.productSize.Height >= 50 && product.productSize.Weight < 1000 && (packingName != "" && packingName != "K/10") && product.priceType == PriceTypeCalc.fixPrice)
            {

                if (product.buyPrice < 25)
                {
                    type = "Büchersendung Maxi";
                    price = 1.65;
                }
                else
                {
                    type = string.Format("Paket_{0}_1000", packingName);
                    price = dpd1000;
                }
            }
            else
            {

                if (product.productSize.packing != null)
                {
                    if (product.productSize.Weight < 1000 - product.productSize.packing.weight)
                    {
                        type = string.Format("Paket_{0}_1000", packingName);
                        price = 2.9;
                    }
                    else if (product.productSize.Weight < 2000 - product.productSize.packing.weight)
                    {
                        type = string.Format("Paket_{0}_2000", packingName);
                        price = 3.1;
                    }
                    else if (product.productSize.Weight < 3000 - product.productSize.packing.weight)
                    {
                        type = string.Format("Paket_{0}_3000", packingName);
                        price = 3.45;
                    }
                    else if (product.productSize.Weight < 6000 - product.productSize.packing.weight)
                    {
                        type = string.Format("Paket_{0}_6000", packingName);
                        price = 3.5;
                    }
                    else if (product.productSize.Weight < 31000 - product.productSize.packing.weight)
                    {
                        type = string.Format("Paket_{0}_10000", packingName);
                        price = 3.61;
                    }
                }
                else
                {
                    if (product.productSize.Weight < 1000 - maxPackageWeigt)
                    {
                        type = "Paket_undefinedSize_1000";
                        price = 2.9;
                    }
                    else if (product.productSize.Weight < 2000 - maxPackageWeigt)
                    {
                        type = "Paket_undefinedSize_2000";
                        price = 3.1;
                    }
                    else if (product.productSize.Weight < 3000 - maxPackageWeigt)
                    {
                        type = "Paket_undefinedSize_3000";
                        price = 3.45;
                    }
                    else if (product.productSize.Weight < 6000 - maxPackageWeigt)
                    {
                        type = "Paket_undefinedSize_6000";
                        price = 3.5;
                    }
                    else if (product.productSize.Weight < 31000 - maxPackageWeigt)
                    {
                        type = "Paket_undefinedSize_10000";
                        price = 3.61;
                    }
                    else
                    {
                        price = 4.81;
                    }
                }
            }


            //Warensendung Überschreibung


            if (product.buyPrice < 25 && product.taxRate == 19 && product.productSize.Weight < 500 && product.productSize.Length >= 100 && product.productSize.Length < 350 && product.productSize.Width >= 70 && product.productSize.Width < 300 && product.productSize.Height < 150)
            {
                if (price > 1.93 || type.StartsWith("Büchersendung"))
                {
                    type = "Warensendung";
                    price = 1.90;
                }
            }

        }


        public override string ToString()
        {
            return type;
        }

    }
}
    
