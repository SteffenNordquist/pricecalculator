﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public class ShippingCostFinder
    {
        private SimpleSize simpleSize;
        private double buyPrice;

        List<Carrier> buyPriceBelowBenchmark = new List<Carrier>();
        List<Carrier> buyPriceAboveBenchmark = new List<Carrier>(); 

        public ShippingCostFinder(SimpleSize simpleSize, double buyPrice, bool isBook)
        {
            this.simpleSize = simpleSize;
            this.buyPrice = buyPrice;
            if (isBook)
            {
                buyPriceBelowBenchmark.Add(new DhlPaket(simpleSize));
                buyPriceBelowBenchmark.Add(new DpLetter(simpleSize));
                //buyPriceBelowBenchmark.Add(new DpWarensendendung(simpleSize));
                //buyPriceBelowBenchmark.Add(new DpBookSending(simpleSize));

                buyPriceAboveBenchmark.Add(new DhlPaket(simpleSize));
            }
            else {
                buyPriceBelowBenchmark.Add(new DhlPaket(simpleSize));
                buyPriceBelowBenchmark.Add(new DpLetter(simpleSize));
                //buyPriceBelowBenchmark.Add(new DpWarensendendung(simpleSize));

                buyPriceAboveBenchmark.Add(new DhlPaket(simpleSize));
            }
        }
        

        public double getShippingPrice(double missingSizePrice)
        {
            if (simpleSize.isValid)
            {
                if (buyPrice <= 25)
                {
                    var carriers = buyPriceBelowBenchmark.Where(x => x.isAvailable());
                    if (carriers.Count() > 0)
                    {
                        return carriers.OrderBy(x => x.getShippingVariation().price).First().getPrice();
                    }
                }
                else
                {
                    var carriers = buyPriceAboveBenchmark.Where(x => x.isAvailable());
                    if (carriers.Count() > 0)
                    {
                        return carriers.OrderBy(x => x.getShippingVariation().price).First().getPrice();
                    }
                }
            }
            else
            {
                return missingSizePrice; 
            }
            return missingSizePrice * 2; 
        }



        public Carrier getCarrier()
        {
            if (simpleSize.isValid)
            {
                if (buyPrice <= 25)
                {
                    var carriers = buyPriceBelowBenchmark.Where(x => x.isAvailable());
                    if (carriers.Count() > 0)
                    {
                        return carriers.OrderBy(x => x.getShippingVariation().price).First();
                    }
                }
                else
                {
                    var carriers = buyPriceAboveBenchmark.Where(x => x.isAvailable());
                    if (carriers.Count() > 0)
                    {
                        return carriers.OrderBy(x => x.getShippingVariation().price).First();
                    }
                }
            }
            else
            {
                return null;
            }
            return null;
        }

        public ShippingVariation getShippingVariation()
        {
            if (simpleSize.isValid)
            {
                if (buyPrice <= 25)
                {
                    var variations = buyPriceBelowBenchmark.Where(x => x.isAvailable());
                    if (variations.Count() > 0)
                    {
                        return variations.OrderBy(x => x.getPrice()).First().getShippingVariation();
                    }
                }
                else
                {
                    var variations = buyPriceAboveBenchmark.Where(x => x.isAvailable());
                    if (variations.Count() > 0)
                    {
                        return variations.OrderBy(x => x.getPrice()).First().getShippingVariation();
                    }
                }
            }
            return new ShippingVariation(new SimpleSize(0,0,0,0),4.00, "Size Error: 4,00€");
        }
    }
}
