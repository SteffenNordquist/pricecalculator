﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public class SimpleSize
    {
        public int length { private set;  get; }
        public int width { private set; get; }
        public int height { private set; get; }
        public int weight { private set; get; }

        public bool isValid { private set; get; }

        public SimpleSize(List<int> sizes, int weight)
        {
            setSizes(sizes, weight); 
        }

        public SimpleSize(int length, int width, int height , int weight)
        {
            List<int> sizes = new List<int>();

            sizes.Add(length);
            sizes.Add(height);
            sizes.Add(width);

            

            setSizes(sizes, weight); 
        }


        private void setSizes(List<int> sizes, int weight)
        {
            sizes.Sort();
            sizes.Reverse();

            if (sizes.Count() == 3 && sizes.Where(x => x == 0).Count() == 0 && weight != null)
            {
                length = sizes[0];
                width = sizes[1];
                height = sizes[2];

                this.weight = weight; 

                isValid = true; 
            }
            else
            {
                isValid = false; 
            }
        }


        public bool fitsInside(SimpleSize inside)
        {
            if ((inside.length < length && inside.width < width && inside.height < height) && inside.weight <= weight && isValid && inside.isValid)
            {
                return true; 
            }
            return false; 
        }

    }
}
