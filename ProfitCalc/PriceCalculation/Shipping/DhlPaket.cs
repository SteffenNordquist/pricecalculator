﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public class DhlPaket: Carrier
    {
        public DhlPaket(SimpleSize productSize)
        {
            this.productSize = productSize;

            variations.Add(new ShippingVariation(new SimpleSize(1200, 600, 600, 1300), 2.75, "DHL Paket 1 kg"));
            //variations.Add(new ShippingVariation(new SimpleSize(1200, 600, 600, 1950), 3.25, "DHL Paket 2 kg"));
            variations.Add(new ShippingVariation(new SimpleSize(1200, 600, 600, 3300), 3.20, "DHL Paket 3 kg"));
            //variations.Add(new ShippingVariation(new SimpleSize(1200, 600, 600, 5950), 3.66, "DHL Paket 6 kg"));
            variations.Add(new ShippingVariation(new SimpleSize(1200, 600, 600, 10300), 4.20, "DHL Paket 10 kg"));
            variations.Add(new ShippingVariation(new SimpleSize(1200, 600, 600, 31450), 4.90, "DHL Paket 31,5 kg"));
        }
    }
}
