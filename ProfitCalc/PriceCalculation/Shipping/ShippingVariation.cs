﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public class ShippingVariation
    {
        public SimpleSize variationSize;
        public double price;
        public string name;

        public ShippingVariation(SimpleSize size, double price, string name)
        {
            this.variationSize = size;
            this.price = price;
            this.name = name; 
        }
    }
}
