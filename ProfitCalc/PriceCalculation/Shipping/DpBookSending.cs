﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public class DpBookSending: Carrier
    {
        public DpBookSending(SimpleSize productSize)
        {
            variations.Add(new ShippingVariation(new SimpleSize(343, 290, 150, 490), 1.0, "Büchersendung klein 1.00" ));
            variations.Add(new ShippingVariation(new SimpleSize(343, 290, 150, 990), 1.65, "Büchersendung 1.65"));

            this.productSize = productSize;            
        }
    }
}
