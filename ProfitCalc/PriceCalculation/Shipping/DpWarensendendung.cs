﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public class DpWarensendendung: Carrier
    {
        public DpWarensendendung(SimpleSize productSize)
        {
            //variations.Add(new ShippingVariation(new SimpleSize(353, 300, 150, 40), 0.90, "Warensendung klein 0.90" ));
            //variations.Add(new ShippingVariation(new SimpleSize(353, 300, 150, 490), 1.75, "Warensendung 500g 1.75 (rabattiert von 1.90)"));
            //variations.Add(new ShippingVariation(new SimpleSize(353, 300, 50, 990), 2.05, "Warensendung 500g 5cm Höhe 2.05 (rabattiert von 2.20)"));
            //variations.Add(new ShippingVariation(new SimpleSize(353, 300, 150, 990), 2.20, "Warensendung 1000g 15cm 2.20 (rabattiert von 2.35)"));

            this.productSize = productSize;            
        }
    }
}
