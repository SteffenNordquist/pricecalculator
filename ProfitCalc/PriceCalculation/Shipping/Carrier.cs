﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public abstract class Carrier: IShipping 
    {
        public SimpleSize productSize {protected set; get;}
        protected ShippingVariation choosenShippingVariation = null;
        protected List<ShippingVariation> variations = new List<ShippingVariation>();

        public ShippingVariation getShippingVariation()
        {
            if (choosenShippingVariation == null)
            {
                var fittingShippingVariations = variations.Where(x => x.variationSize.fitsInside(productSize));
                if (fittingShippingVariations.Count() > 0)
                {
                    choosenShippingVariation = fittingShippingVariations.OrderBy(x => x.price).First();
                    return choosenShippingVariation;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return choosenShippingVariation;
            }
        }

        public bool isAvailable()
        {
            return getShippingVariation() != null;
        }

        public double getPrice()
        {
            return getShippingVariation() == null ? 0 : choosenShippingVariation.price;
        }
    }
}
