﻿using DN_Classes.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriceCalculation.Shipping
{
    public class FbaNonMedia:Carrier
    {
        /// <summary>
        /// http://services.amazon.de/programme/versand-durch-amazon/preisgestaltung.html
        /// </summary>
        /// <param name="productSize"></param>
        public FbaNonMedia(SimpleSize productSize)
        {

            //Kleiner Briefumschlag (20 g)	≤ 20 x 15 x 1 cm	0 – 100 g	1,60 €
            variations.Add(new ShippingVariation(new SimpleSize(200, 150, 10, 80), 1.60, "Kleiner Briefumschlag "));

            /*Standard-Briefumschlag 
            (40 g)	≤ 33 x 23 x 2,5 cm	            
            0 - 100 g	1,72 €
            101 - 250 g	1,73 €
            251 - 500 g	1,77 €*/
            variations.Add(new ShippingVariation(new SimpleSize(330, 230, 25, 60), 1.72, "Standard-Briefumschlag 100g"));
            variations.Add(new ShippingVariation(new SimpleSize(330, 230, 25, 210), 1.73, "Standard-Briefumschlag 250g"));
            variations.Add(new ShippingVariation(new SimpleSize(330, 230, 25, 460), 1.77, "Standard-Briefumschlag 500g"));

            //Großer Briefumschlag (40 g)	≤ 33 x 23 x 5 cm	0 – 1.000 g	2,14 €
            variations.Add(new ShippingVariation(new SimpleSize(330, 230, 50, 960), 2.14, "Standard-Briefumschlag 1000g"));

            /*  Standard-Paket 
                (100 g)	≤ 45 x 34 x 26 cm	
                0 - 250 g	2,32 €
                251 - 500 g	2,41 €
                501 – 1.000 g	2,98 €
             * 
                1.001 – 1.500 g	3,04 €
                1.501 – 2.000 g	3,10 €
                2.001 – 3.000 g	4,18 €
             * 
                3.001 – 4.000 g	4,19 €
                4.001 – 5.000 g	4,19 €
                5.001 – 6.000 g	4,27 €
             * 
                6.001 – 7.000 g	4,27 €
                7.001 – 8.000 g	4,40 €
                8.001 – 9.000 g	4,40 €
             * 
                9.001 – 10.000 g	4,40 €
                10.001 – 11.000 g	4,40 €
                11.001 – 12.000 g	4,41 €
             */

            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 150), 2.32, "Standard-Paket 250g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 400), 2.41, "Standard-Paket 500g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 900), 2.98, "Standard-Paket 1000g"));

            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 1400), 3.04, "Standard-Paket 1500g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 1900), 3.10, "Standard-Paket 2000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 2900), 4.18, "Standard-Paket 3000g"));

            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 3900), 4.19, "Standard-Paket 4000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 4900), 4.19, "Standard-Paket 5000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 5900), 4.27, "Standard-Paket 6000g"));

            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 6900), 4.27, "Standard-Paket 7000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 7900), 4.40, "Standard-Paket 8000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 8900), 4.40, "Standard-Paket 9000g"));

            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 9900), 4.40, "Standard-Paket 10000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 10900), 4.40, "Standard-Paket 11000g"));
            variations.Add(new ShippingVariation(new SimpleSize(450, 340, 260, 11900), 4.41, "Standard-Paket 12000g"));

            this.productSize = productSize; 
        }
    }
}
