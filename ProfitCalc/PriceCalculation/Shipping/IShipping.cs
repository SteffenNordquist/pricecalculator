﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation.Shipping
{
    public interface IShipping
    {
        bool isAvailable();
        double getPrice(); 
    }
}
