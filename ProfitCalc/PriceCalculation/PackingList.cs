﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class PackingList
    {
        public Dictionary<string, Packing> packingListDict = new Dictionary<string, Packing>();

        public PackingList()
        {
           // string[] lines = File.ReadAllLines(path);
            string[] lines = { "C/3\t225\t145\t11\t0.044",
                                  "D/4\t265\t175\t15\t0.055", 
                                  "E/5\t265\t215\t19\t0.066", 
                                  "G/7\t340\t225\t23\t0.09", 
                                  "H/8\t360\t265\t28\t0.10",
                                  "K/10\t480\t345\t46\t0.18"};


            foreach (var line in lines)
            {
                Packing packing = new Packing(line);
                packingListDict.Add(packing.name, packing);
            }
        }

        public Packing getPacking(string name)
        {
            return packingListDict[name];
        }

    }
}
