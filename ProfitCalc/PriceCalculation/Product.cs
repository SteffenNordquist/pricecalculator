﻿using DN_Classes;
using DN_Classes.Amazon.Orders;
using DN_Classes.Conditions;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using DN_Classes.Supplier;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class Product
    {
        public string identifier = "";
        public IdentifierType identifierType = IdentifierType.EAN;

        public string sku = ""; 
        public int quantity = 0;
        public int availableQuantity = 0;
        public string currency = "EUR";

        public double price = 0;
        public double discountRate = 0;
        public double taxRate = 0;

        public int weight = 1000;

        public double buyPrice = 0;
        public double weightFee = 0;
        public double shippingIncome = 0;

        public double shippingCosts = 3.01;
        public double packingCosts = 0.20;

        public double sellPrice;

        public ProductSize productSize;

        public PriceTypeCalc priceType;

        public ContractConditions contractConditions;
        public ProfitConditions profitConditions;
        public PlatformConditions platformConditions;

        public int fulfillmentLatency = 2;
        public DateTime fullFillmentLatencyChangedDate; 


        public Product(string line, ContractConditions contractConditions, ProfitConditions profitConditions, PlatformConditions platformConditions, PackingList packingList, IdentifierType identifierType, int maxQuantity)
        {
            this.identifierType = identifierType; 
            this.contractConditions = contractConditions;
            this.profitConditions = profitConditions;
            this.platformConditions = platformConditions; 


            var tokens = line.Split();

            identifier = tokens[1];
            sku = tokens[0];

            if (identifier == "4005556044931")
            { }
            quantity = 0;
            int.TryParse(tokens[2], out quantity);

            availableQuantity = quantity;
            quantity = Math.Min(quantity, maxQuantity);

            if (quantity != 0)
            {
                
                currency = tokens[3];

                price = double.Parse(tokens[4], CultureInfo.InvariantCulture);
                discountRate = double.Parse(tokens[5], CultureInfo.InvariantCulture);
                taxRate = double.Parse(tokens[6], CultureInfo.InvariantCulture);

                int.TryParse(tokens[11], out weight);

                buyPrice = PriceCalc.getBuyPriceFromDiscount(price, taxRate, discountRate);
                weightFee = PriceCalc.getWeightFee(weight, contractConditions.WeightFee);
                shippingIncome = PriceCalc.getNetShippingIncome(0, 0, platformConditions.DefaultPlatformFeeRate, taxRate);

                productSize = new ProductSize(tokens[8], tokens[9], tokens[10], tokens[11], packingList);


                shippingCosts = new ShippingType(this).price;
                packingCosts = productSize.packing.price;

                if (tokens[12] == "04")
                {
                    priceType = PriceTypeCalc.fixPrice;
                }
                else if (tokens[12] == "02")
                {
                    priceType = PriceTypeCalc.flexiblePrice;
                }
                else {
                    priceType = PriceTypeCalc.fixPrice;
                }

                //fulfillmentLatency = int.Parse(tokens[13]);
            }           
        }


        public void CalcSellPrice(double shippingIncome)
        {
            if (this.identifier == "5050582974737")
            { }

            this.shippingIncome = shippingIncome; 
            sellPrice = PriceCalc.getSellPrice(this, profitConditions, contractConditions, platformConditions);
            price = sellPrice; 
        }

        public double getProfit(double sellPrice, double shippingIncome)
        {
            if (this.identifier == "9783906021102")
            { 
            }
            this.price = sellPrice;
            this.shippingIncome = shippingIncome; 
            return PriceCalc.getProfit(this); 
            
        }

        public Product(IProduct iProduct, ContractConditions contractConditions, ProfitConditions profitConditions, PlatformConditions platformConditions, PackingList packingList)
        {
            this.contractConditions = contractConditions;
            this.profitConditions = profitConditions;
            this.platformConditions = platformConditions; 


            identifier = iProduct.GetEanList().Where(x=> x.Length == 13).First();
            quantity = iProduct.getStock();
            currency = iProduct.getCurrency();

            price = iProduct.getPrice();
            discountRate = iProduct.getDiscount();
            taxRate = iProduct.getTax();

            weight = (int)iProduct.getWeight();

            buyPrice = PriceCalc.getBuyPriceFromDiscount(price, taxRate, discountRate);
            weightFee = PriceCalc.getWeightFee(weight, contractConditions.WeightFee);
            shippingIncome = PriceCalc.getNetShippingIncome(0, 0, platformConditions.DefaultPlatformFeeRate, taxRate);

            productSize = new ProductSize(iProduct.getLength(), iProduct.getWidth(), iProduct.getHeight(), weight, packingList);
            packingCosts = productSize.packing.price;

            if (platformConditions.PlatformCategory == PlatformCategory.amazonBooks)
            {
                shippingIncome = 3; 
            }

            shippingCosts = new ShippingType(this).price;

            if (iProduct.getPriceType() == PriceType.fixedPrice)
            {
                this.priceType = PriceTypeCalc.fixPrice;
            }
            else {
                this.priceType = PriceTypeCalc.flexiblePrice;
            }
        }

        public Product()
        {

        }

        public Product(AmazonOrder singleItem, ConditionSet conditionSet, BuyPriceRow cheapestItem, ProductSize productMeasurements,  double shippingcosts)
        {
            identifier = singleItem.SellerSKU.ToString();
            quantity = 1;
            currency = "EUR";

            price = singleItem.ItemPrice.ToDouble();
            sellPrice = singleItem.ItemPrice.ToDouble();
            discountRate = 0;
            this.taxRate = cheapestItem.tax;

            this.profitConditions = conditionSet.Profit; 
            this.contractConditions = conditionSet.Contract;
            this.platformConditions = conditionSet.Platform; 

            weight = productMeasurements.Weight;

            buyPrice = cheapestItem.price;
            weightFee = PriceCalc.getWeightFee(weight, contractConditions.WeightFee);
            shippingIncome = PriceCalc.getNetShippingIncome(0, 0, platformConditions.DefaultPlatformFeeRate, taxRate);

            productSize = productMeasurements;
            packingCosts = productSize.packing.price;

            if (platformConditions.PlatformCategory == PlatformCategory.amazonBooks)
            {
                shippingIncome = 3;
            }

            shippingCosts = new ShippingType(this).price;

            if (cheapestItem.fixedPrice == "04")
            {
                this.priceType = PriceTypeCalc.fixPrice;
            }
            else
            {
                this.priceType = PriceTypeCalc.flexiblePrice;
            }
        }

		//public Product(string ebayIdentifier, double ebayPrice, ConditionSet conditionSet, BuyPriceRow cheapestItem, ProductSize productMeasurements, double shippingcosts)
		//{
		//	identifier = ebayIdentifier;
		//	quantity = 1;
		//	currency = "EUR";

		//	price = ebayPrice;
		//	sellPrice = ebayPrice;
		//	discountRate = 0;
		//	this.taxRate = cheapestItem.tax;

		//	this.profitConditions = conditionSet.Profit;
		//	this.contractConditions = conditionSet.Contract;
		//	this.platformConditions = conditionSet.Platform;

		//	weight = productMeasurements.Weight;

		//	buyPrice = cheapestItem.price;
		//	weightFee = PriceCalc.getWeightFee(weight, contractConditions.WeightFee);
		//	shippingIncome = PriceCalc.getNetShippingIncome(0, 0, platformConditions.DefaultPlatformFeeRate, taxRate);

		//	productSize = productMeasurements;
		//	packingCosts = productSize.packing.price;

		//	if (platformConditions.PlatformCategory == PlatformCategory.amazonBooks)
		//	{
		//		shippingIncome = 3;
		//	}

		//	shippingCosts = new ShippingType(this).price;

		//	if (cheapestItem.fixedPrice == "04")
		//	{
		//		this.priceType = PriceTypeCalc.fixPrice;
		//	}
		//	else
		//	{
		//		this.priceType = PriceTypeCalc.flexiblePrice;
		//	}
		//}

		public Product(string ean, double itemPrice, ConditionSet conditionSet, BuyPriceRow cheapestItem, ProductSize itemSize, double shippingIncome)
		{
			identifier = ean;
            quantity = 1;
            currency = "EUR";

			price = itemPrice;
			sellPrice = itemPrice;
            discountRate = 0;
            this.taxRate = cheapestItem.tax;

            this.profitConditions = conditionSet.Profit; 
            this.contractConditions = conditionSet.Contract;
            this.platformConditions = conditionSet.Platform; 

            weight = itemSize.Weight;

            buyPrice = cheapestItem.price;
            weightFee = PriceCalc.getWeightFee(weight, contractConditions.WeightFee);
            this.shippingIncome = PriceCalc.getNetShippingIncome(shippingIncome, 0, platformConditions.DefaultPlatformFeeRate, taxRate);

            productSize = itemSize;
            packingCosts = productSize.packing.price;

            if (platformConditions.PlatformCategory == PlatformCategory.amazonBooks)
            {
				this.shippingIncome = 3;
            }

            shippingCosts = new ShippingType(this).price;

            if (cheapestItem.fixedPrice == "04")
            {
                this.priceType = PriceTypeCalc.fixPrice;
            }
            else
            {
                this.priceType = PriceTypeCalc.flexiblePrice;
            }
        }

    }

	


    public enum PriceTypeCalc
    {
        fixPrice, flexiblePrice
    }

    public enum IdentifierType
    {
        EAN, ASIN
    }
}
