﻿using DN_Classes.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class Packing : PackingEntity
    {

        public Packing(ProductSize productSize)
        {
            if (productSize.isValid)
            {
                name = productSize.packingName;
                lenght = productSize.packing.lenght;
                width = productSize.packing.width;
                weight = productSize.packing.weight;
                price = productSize.packing.price;

                netPrice = price / 1.19;
            }
        }
   

        public Packing(string packingLine)
        {
            string[] pack = packingLine.Split('\t');
            name = pack[0];
            lenght = int.Parse(pack[1]);
            width = int.Parse(pack[2]);
            weight = int.Parse(pack[3]);
            price = double.Parse(pack[4], CultureInfo.InvariantCulture);

            netPrice = price / 1.19;
        }


        public Packing()
        {
            name = "";
            lenght = 9999;
            width = 9999;
            weight = 600;
            price = 0.50 * 0.19;
            netPrice = 0.50;
        }
    }
}
