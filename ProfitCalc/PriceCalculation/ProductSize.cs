﻿using DN_Classes.Products;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class ProductSize: SimpleSize
    {
        public string packingName = "";

        private PackingList packingList;
        public Packing packing;
	    public bool isFakeSize; 

        public ProductSize(string lengthString, string widthString, string heightString, string weightString, PackingList packingL)
        {
            try
            {
                int t_length = 0;
                int t_width = 0;
                int t_height = 0;
                int t_weight = 0;

                int.TryParse(lengthString, out t_length);
                int.TryParse(widthString, out t_width);
                int.TryParse(heightString, out t_height);
                int.TryParse(weightString, out t_weight);

                List<int> sizes = new List<int>();
                sizes.Add(t_length);
                sizes.Add(t_height);
                sizes.Add(t_width);
                setSizes(sizes, t_weight);

                packingList = packingL;
                packing= getPacking(packingList);
            }
            catch { }
        }

        public ProductSize(int lenght, int width, int height, int weight, PackingList packingList)
        {
            this.Length = lenght;
            this.Width = width;
            this.Height = height;
            this.Weight = weight;
            this.packingList = packingList;

            packing = getPacking(packingList);
        }


        public ProductSize()
        {
        }
        

        public Packing getPacking(PackingList packingL)
        {
            packingList = packingL;
            if (packingList != null)
            {
                int effectiveLength = Math.Max(Length, Width);
                int effectiveWidth = Math.Min(Length, Width);

                double packingLength = effectiveLength + 2 * Height * 1.2;
                double packingWidth = effectiveWidth + Height * 1.2;


                foreach (var packing in packingList.packingListDict)
                {
                    if (packing.Value.lenght > packingLength && packing.Value.width > packingWidth)
                    {
                        return packing.Value;
                    }
                }
            }
            else
            {
                return new Packing();
            }

            return new Packing();
        }


        public string getPackingName()
        {
            Packing packing = getPacking(packingList);

            if (packing != null)
            {
                return packing.name;
            }
            return "";
        }




        public bool isComplete()
        {
            if (Length * Width * Height * Weight == 0)
            {
                return false;
            }
            else
            {
                return true; 
            }
        }



        public void addSizesFromMeasurements(Measurements measurements)
        {
            this.Length = measurements.length;
            this.Width = measurements.width;
            this.Height = measurements.height;
            this.Weight = measurements.weight; 
        }


    }
}
