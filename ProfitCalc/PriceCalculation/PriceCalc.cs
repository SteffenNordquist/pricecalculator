﻿using DN_Classes.Conditions;
using DN_Classes.Products;
using DN_Classes.Supplier;
using PriceCalculation.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PriceCalculation
{
    public class PriceCalc
    {
       public static double getBuyPriceFromDiscount(double recommendedSellPrice, double taxRate, double discountRate)
       {
           double taxFactor = (100 + taxRate) / 100;
           double discountValue = recommendedSellPrice * discountRate / 100;  
           double buyPrice = (recommendedSellPrice - discountValue) / taxFactor;
           return buyPrice; 
       }

        /// <summary>
        /// Weight in gram 
        /// Fee in KG
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="weightFeeInKG"></param>
        /// <returns></returns>
       public static double getWeightFee(int weight, double weightFeeInKG) {
           return weight * weightFeeInKG / 1000; 
       }


        public static double getNetShippingIncome(double shippingIncome, double fixedReduction, double platformFee,  double taxRate)
        {
            return shippingIncome / ((100 + taxRate)/ 100)- fixedReduction  - shippingIncome * platformFee; 
        }

        public static double calcPaymentFee(PlatformConditions platformConditions, double sellPrice, double shippingIncome)
        {
            return platformConditions.PaymentFeeFix + platformConditions.PaymentFeeRate * (sellPrice + shippingIncome) / 100;
        }


        public static double getSellPrice(Product product, ProfitConditions profitConditions, ContractConditions contractConditions, PlatformConditions platformConditions )
        {
            if (product.priceType == PriceTypeCalc.flexiblePrice)
            {
                double sellPrice = product.buyPrice * 3;

                if (product.buyPrice < 3)
                {
                    sellPrice = product.buyPrice * 7;
                }

                double profit = profitConditions.ProfitWanted + 1;

                double platformFees = 0;
                double packingCosts = product.productSize.packing.price;

                double riskCosts = 0;
                double netSellPrice = 999;
                double weightFee = getWeightFee(product.weight, contractConditions.WeightFee);

                //discounting will be done in transfer file
                //double discountedBuyPrice = product.buyPrice - product.buyPrice * contractConditions.Discount / 100;
                //discountedBuyPrice = discountedBuyPrice - discountedBuyPrice * contractConditions.Bonus / 100;

                var shippingCostFinder = new ShippingCostFinder(new SimpleSize(product.productSize.Length, product.productSize.Width, product.productSize.Height, product.productSize.Weight), product.price , product.priceType == PriceTypeCalc.fixPrice);
                double shippingCosts = shippingCostFinder.getShippingPrice(4.00);

                if (shippingCostFinder.getCarrier() != null && shippingCostFinder.getCarrier().GetType().Name == "DhlPaket")
                {
                    riskCosts = 0;
                }
                else
                {
                    riskCosts = product.buyPrice * 2 * (profitConditions.RiskFactor / 100);
                }


                double dataQualityPunishment = 0; 

                if (product.productSize.Length * product.productSize.Width * product.productSize.Height * product.productSize.Weight == 0)
                {
                    dataQualityPunishment = 2.7;
                }

                while (profit > profitConditions.ProfitWanted)
                {
                    sellPrice = sellPrice - 0.01;
                                       

                    //paypal = calcPaymentFee(platformConditions, sellPrice, product.shippingIncome);
                    //platformFees = sellPrice * platformConditions.DefaultPlatformFeeRate / 100;


                    //netSellPrice = sellPrice / ((100 + product.taxRate) / 100);
                    //riskCosts = sellPrice * (profitConditions.RiskFactor / 100);


                    //profit = netSellPrice - discountedBuyPrice - riskCosts - product.packingCosts - product.shippingCosts + product.shippingIncome - platformFees - paypal - profitConditions.ItemHandlingCosts - weightFee;



                    double paymentFee = calcPaymentFee(platformConditions, sellPrice, product.shippingIncome);
                    platformFees = (sellPrice + product.shippingIncome) * platformConditions.DefaultPlatformFeeRate / 100;


                    netSellPrice = sellPrice / ((100 + product.taxRate) / 100);
                    double netShippingIncome = product.shippingIncome / ((100 + product.taxRate) / 100);


                    double fullPlatformFee = platformFees + paymentFee;
                    double fixCosts = product.packingCosts + shippingCosts + riskCosts + profitConditions.ItemHandlingCosts + weightFee;


                    profit = netSellPrice - product.buyPrice + netShippingIncome - fullPlatformFee - fixCosts - dataQualityPunishment;

                    if (profit <= profitConditions.ProfitWanted)
                    { }
                }

                return sellPrice;
            }
            else {
                return product.price;
            }
        }



        public static double getProfit(Product product)
        {

            if (product.identifier == "5050582974737")
            { }
            double sellPrice = product.price;
            if (product.sellPrice != 0)
            {
                sellPrice = product.sellPrice; 
            }

            double packingCosts = product.productSize.packing.price;

            double weightFee = getWeightFee(product.weight, product.contractConditions.WeightFee);
            double riskCosts = 0;

            // discount will happen on transfermaker
            //double discountedBuyPrice = product.buyPrice - product.buyPrice * product.contractConditions.Discount / 100;
            //discountedBuyPrice = discountedBuyPrice - discountedBuyPrice * product.contractConditions.Bonus / 100;

            var shippingCostFinder = new ShippingCostFinder(new SimpleSize(product.productSize.Length, product.productSize.Width, product.productSize.Height, product.productSize.Weight), product.price, product.priceType == PriceTypeCalc.fixPrice);
            double shippingCosts = shippingCostFinder.getShippingPrice(4.00);

            if (shippingCostFinder.getCarrier() != null && shippingCostFinder.getCarrier().GetType().Name == "DhlPaket")
            {
                riskCosts = 0;
            }
            else
            {
                riskCosts = product.buyPrice * 2 * (product.profitConditions.RiskFactor / 100);
            }
				//paymnetfee or paypal fee
            double paymentFee = calcPaymentFee(product.platformConditions, sellPrice, product.shippingIncome);
            double platformFees = (sellPrice + product.shippingIncome) * product.platformConditions.DefaultPlatformFeeRate / 100;


            double netSellPrice = sellPrice / ((100 + product.taxRate) / 100);
            double netShippingIncome = product.shippingIncome / ((100 + product.taxRate) / 100);


            double fullPlatformFee = platformFees + paymentFee;
            double fixCosts = product.packingCosts + shippingCosts + riskCosts + product.profitConditions.ItemHandlingCosts + weightFee;

            double dataQualityPunishment = 0;

            if (product.productSize.Length * product.productSize.Width * product.productSize.Height * product.productSize.Weight == 0)
            {
                dataQualityPunishment = 2.5;
            }

            double profit = netSellPrice - product.buyPrice + netShippingIncome - fullPlatformFee - fixCosts - dataQualityPunishment;


            return Math.Round(profit, 2);


        }
    

    
    
    }
}
