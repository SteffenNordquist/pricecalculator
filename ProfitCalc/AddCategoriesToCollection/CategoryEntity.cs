﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddCategoriesToCollection
{
    public class CategoryEntity
    {
        [BsonId]
        public string asin { get; set; }
        public string mainCategoryId { get; set; }
        public string mainCategoryRank { get; set; }
        public DateTime lastUpdated { get; set; }

        public CategoryEntity()
        { }

        public CategoryEntity(string asin)
        {
            this.asin = asin;
        }
    }
}
