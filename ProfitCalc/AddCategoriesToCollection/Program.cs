﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProfitCalcEbay;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TransferConsumer;

namespace AddCategoriesToCollection
{
    class Program
    {
           public static MongoClient Client;
        public static MongoServer Server;
        public static MongoDatabase PDatabase;

        public static MongoCollection<CategoryEntity> mongoCollection;
        //public static MongoCollection<BsonDocument> mongoCollectionBson;

        static void Main(string[] args)
        {
           
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/ProductDatabase");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("Amazon");
            mongoCollection = PDatabase.GetCollection<CategoryEntity>("AmazonCategory");

            AddNewFlexiblePricedProductsToCollection();
        
        }

        private static void AddNewFlexiblePricedProductsToCollection()
        {
            //ProductFinder pf = new ProductFinder();
            //var flexiblePriceList = pf.findAllFlexiblePrice(); 

            List<GenericTransfer> genericTransferList = new List<GenericTransfer>();
            // genericTransferList.Add(new GenericTransfer("http://148.251.0.235:8080/berktransfer.txt",TransferConsumer.TransferConsumerTransferType.ASIN));
            //genericTransferList.Add(new GenericTransfer("http://148.251.0.235:8080/gallaytransfer.txt", TransferConsumer.TransferConsumerTransferType.ASIN));
            genericTransferList.Add(new GenericTransfer("http://148.251.0.235:8080/Knvtransfer.txt", TransferConsumer.TransferConsumerTransferType.EAN));
            //genericTransferList.Add(new GenericTransfer("http://148.251.0.235:8080/libritransfer.txt", TransferConsumer.TransferConsumerTransferType.EAN));
            //genericTransferList.Add(new GenericTransfer("http://148.251.0.235:8080/bremertransfer.txt", TransferConsumer.TransferConsumerTransferType.EAN));

            List<string> asinList = new List<string>();

            var list = File.ReadAllLines("C:\\Inventory\\InventoryReport.txt").Where(x => x.Length > 0).Select(x => new AmazonInventoryLine(x));

            Dictionary<string, string> eanAsinDict = new Dictionary<string, string>();

            foreach (var item in list)
            {
                if (!eanAsinDict.ContainsKey(item.sku))
                {
                    eanAsinDict.Add(item.sku, item.asin);
                }
                //.ToDictionary(x => x.sku, x=> x.asin);
            }

            foreach (var transfer in genericTransferList)
            {
                if (transfer.transferConsumerTransferType == TransferConsumerTransferType.ASIN)
                {
                    asinList.AddRange(transfer.transferLines.Select(x => x.identifier));
                }
                if (transfer.transferConsumerTransferType == TransferConsumerTransferType.EAN)
                {
                    ParallelOptions po = new ParallelOptions();
                    po.MaxDegreeOfParallelism = 8; 
                    Parallel.ForEach(transfer.transferLines.Where(x => x.priceTypeEnum == priceTypeEnum.flexiblePrice), po ,line =>
                    {
                        if (eanAsinDict.ContainsKey(line.identifier))
                        {
                            // asinList.Add(eanAsinDict[line.identifier]);
                            var asin = eanAsinDict[line.identifier];
                         
                            if (!Exists(asin))
                            {
                                mongoCollection.Insert(new CategoryEntity(asin));
                                Console.WriteLine("insterted: " + asin);
                            }
                            else
                            {
                                Console.WriteLine("already exists: " + asin);
                            }
                        }
                    });
                }
            }
        }

        private static bool Exists(string asin)
        {
            bool exists = mongoCollection.Find(Query.EQ("_id", asin)).Count() > 0;
            return exists;
        }
    }
}
