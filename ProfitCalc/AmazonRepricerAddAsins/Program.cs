﻿using AmazonRepricerDll;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AmazonRepricerAddAsins
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadAsinsForRepricing();
        }

        private static void ReadAsinsForRepricing()
        {
            RepricerDB repricerDB = new RepricerDB();

            var asinstext = File.ReadAllText("C:\\temp\\rememberasin.txt");

            Regex regex = new Regex(@"[A-Z,0-9]{10}");
            List<string> asins = regex.Matches(asinstext).Cast<Match>().Select(x => x.Value).ToList();

            Parallel.ForEach(asins, new ParallelOptions { MaxDegreeOfParallelism = 8 }, asin =>
            {
                repricerDB.addAsin(asin);
                Console.WriteLine(asin);
            });
        }
    }
}
