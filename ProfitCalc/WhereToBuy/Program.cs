﻿using DN_Classes.Conditions;
using DN_Classes.Products.Price;
using DN_Classes.Supplier;
using PriceCalculation;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WhereToBuy
{
    class Program
    {

        static StringBuilder resultStringBuilder = new StringBuilder();
      //  static List<string> forImport = new List<string>();
        static void Main(string[] args)
        {

            ProductFinder productFinder = new ProductFinder();
            //DisplayPricesForEan(productFinder, "4002051604080");

            //     this.name + "\t" + this.stock.ToString() + "\t" + this.price.ToString() + "\t" + weightFee.ToString() + "\t" + sku + "\t" + this.tax.ToString() + "\t" + suppliername + "\t" + this.fixedPrice;

            //forImport.Add("ean\tstock\tprice\tweightfee\tsku\ttax\tsupplier\tpricetype");
            Regex regex = new Regex(@"\d{13}");

            var eanRaw = File.ReadAllText("eans.txt");
            foreach (Match match in regex.Matches(eanRaw))
            {
                DisplayPricesForEan(productFinder, match.Value.Replace("-","").Trim());
            }
            File.WriteAllText("results.txt", resultStringBuilder.ToString());

            Process.Start("results.txt");
       
        }

        private static void DisplayPricesForEan(ProductFinder productFinder, string ean)
        {
            Console.WriteLine(ean);
            var products = productFinder.FindAllProductsByEan(ean, new List<string>());
            resultStringBuilder.AppendLine(ean);


            List<BuyPriceRow> buyPriceRowList = new List<BuyPriceRow>();

            foreach (var product in products)
            {
                if (product != null) {
                    string supplierName = cleanName(product.GetType().ToString());

                    ContractConditions contractConditions = new ContractConditions(0, 0, 0);//null;

                    if (supplierName == "LIBRI")
                    {
                        contractConditions = new ContractConditions(0.34, 2, 0);
                    }
                    if (supplierName == "KNV")
                    {
                        contractConditions = new ContractConditions(0.16, 2, 1.3);
                    }
                    if (supplierName == "BVW")
                    {
                        contractConditions = new ContractConditions(0.1, 0, 0);
                    }
                    if (supplierName == "GALLAY")
                    {
                        contractConditions = new ContractConditions(0, 0, 0);
                    }
                    if (supplierName == "BERK")
                    {
                        contractConditions = new ContractConditions(0, 0, 0);
                    }
                    if (supplierName == "KOSMOS")
                    {
                        contractConditions = new ContractConditions(0, 6, 2);
                    }
                    if (supplierName == "JPC")
                    {
                        contractConditions = new ContractConditions(0, 0, 0);
                    }
                    if (supplierName == "VTECH")
                    {
                        contractConditions = new ContractConditions(0, 6, 2);
                    }
                    if (supplierName == "RAVENSBURGER")
                    {
                        contractConditions = new ContractConditions(0, 6, 3);
                    }
                    double buyPrice = PriceCalc.getBuyPriceFromDiscount(product.getPrice(), product.getTax(), product.getDiscount());
                    double weightFee = PriceCalc.getWeightFee((int)product.getWeight(), contractConditions.WeightFee);
                    double discountedBuyPrice = buyPrice - buyPrice * contractConditions.Discount / 100;
                    discountedBuyPrice = discountedBuyPrice - discountedBuyPrice * contractConditions.Bonus / 100;

                    double finalPrice = discountedBuyPrice + weightFee;
                   string  FixedPrice= (product.getPriceType() ==  PriceType.fixedPrice) ?"04":"02";
                   buyPriceRowList.Add(new BuyPriceRow(ean, product.getStock(), Math.Round(finalPrice, 2), weightFee, product.getSKU(), FixedPrice, product.getTax(), supplierName));
                 
                
                }
             
            }
           

            var listInStock = buyPriceRowList.Where(x => x.stock > 0).OrderBy(x => x.price).ToList();
            var outOfStock = buyPriceRowList.Where(x => x.stock == 0).OrderBy(x => x.price).ToList();
            var finalList = listInStock;

            finalList.AddRange(outOfStock);

            foreach (var listItem in finalList)
            {
                //Insert(listItem.ToString(), listItem.name);
                if (finalList.IndexOf(listItem) != 0)
                {
                   //original does not contain ean
                    resultStringBuilder.AppendLine(ean+"\t"+listItem.ToString() + "\t(" + (Math.Round(listItem.price - finalList[0].price, 2) * -1) + ")\t");
                }
                else
                {
                    //original does not contain ean
                    resultStringBuilder.AppendLine(ean + "\t" + listItem.ToString());
                }

            }
            resultStringBuilder.AppendLine("");
            resultStringBuilder.AppendLine("----------------------------");
            resultStringBuilder.AppendLine("");
        }

        //static void Insert(string newline, string ean) {
        //    bool insert = true;
        //    foreach (string line in forImport) {
        //        if (line.Contains(ean)) {
        //            insert = false;
        //            break;
        //        }
        //    }

        //    if (insert)
        //        forImport.Add(newline);
        //}
        static string cleanName(string name)
        {

            if (name.ToUpper().Contains("LIBRI"))
            {
                return "LIBRI";
            }
            if (name.ToUpper().Contains("HOF"))
            {
                return "HOFFMANN";
            }
            if (name.ToUpper().Contains("SCHMIDT"))
            {
                return "SCHMIDT";
            }
            if (name.ToUpper().Contains("BVW"))
            {
                return "BVW";
            }
            if (name.ToUpper().Contains("KNV"))
            {
                return "KNV";
            }
            if (name.ToUpper().Contains("GALLAY"))
            {
                return "GALLAY";
            }
            if (name.ToUpper().Contains("BERK"))
            {
                return "BERK";
            }
            if (name.ToUpper().Contains("KOSMOS"))
            {
                return "KOSMOS";
            }
            if (name.ToUpper().Contains("JPC"))
            {
                return "JPC";
            }
            if (name.ToUpper().Contains("VTECH"))
            {
                return "VTECH";
            }
            if (name.ToUpper().Contains("RAVENSBURGER"))
            {
                return "RAVENSBURGER";
            }

            return string.Empty; 
        }

    }
}
