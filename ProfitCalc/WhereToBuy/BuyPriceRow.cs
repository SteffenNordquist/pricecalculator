﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WhereToBuy
{
    public class BuyPriceRow
    {
        public string name;
        public int stock;
        public double price;
        public double weightFee;
        public string sku;
        public string fixedPrice;
        public double tax;
        private string suppliername;
        public BuyPriceRow(string name, int stock, double price, double weightFee, string sku)
        {
            this.name = name;
            this.stock = stock;
            this.price = price;
            this.weightFee = weightFee;
            this.sku = sku; 
            
        }

        //not the original
        public BuyPriceRow(string name, int stock, double price, double weightFee, string sku, string fixedPrice, double tax,string suppliername)
        {
            this.name = name;
            this.stock = stock;
            this.price = price;
            this.weightFee = weightFee;
            this.sku = sku;
            this.fixedPrice = fixedPrice;
            this.tax = tax;
            this.suppliername = suppliername;
        }

        //public override string ToString()
        //{
        //    return fillBlanks(name, 10) + "\t" + fillBlanks(stock.ToString(), 10) + "\t" + fillBlanks(price.ToString(), 10) + "\t" + fillBlanks(weightFee.ToString(), 10) + "\t" + sku ; 
        //}

        public override string ToString()
        {//
          //  return this.name + "\t" + this.stock.ToString() + "\t" + this.price.ToString() + "\t" + weightFee.ToString() + "\t" + sku + "\t" + this.tax.ToString() + "\t" + suppliername + "\t" + this.fixedPrice;
            return fillBlanks(name, 10) + "\t" + fillBlanks(stock.ToString(), 10) + "\t" + fillBlanks(price.ToString(), 10) + "\t" + fillBlanks(weightFee.ToString(), 10) + "\t" + sku;
        }

        private string fillBlanks(string name, int blanks)
        {
            int chars = name.Count();

            int more = Math.Max(0, blanks - chars);

            for (int i = 0; i < more; i++)
            {
                name += " ";
            }

            return name;
        }
    }
}
