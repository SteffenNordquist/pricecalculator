﻿
using DN_Classes.Products.Price;
using ProcuctDB.Libri;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EbayPrepare
{
    class Program
    {
        static void Main(string[] args)
        {
            writeFlexibleEansToFile();
        }



        private static void writeFlexibleEansToFile()
        {
            var database = new LibriDB("148.251.0.235");
            var eans = File.ReadAllLines("ebayReady.txt").ToList();

            List<string> flexibleEans = new List<string>();
            foreach (var ean in eans)
            {
                try
                {
                    if (database.GetMongoProductByEan(ean).getPriceType() == PriceType.flexiblePrice)
                    {
                        flexibleEans.Add(ean);
                        Console.WriteLine(eans.IndexOf(ean) + "\t" + flexibleEans.Count);
                    }
                }catch
                {
                    Console.WriteLine("ooks");
                }
            }
            File.WriteAllLines("flexibleEans.txt", flexibleEans);       
        }


        private static void getEbayReadyToFile()
        {
            var database = new LibriDB("148.251.0.235");
            var ebayReadyEans = database.GetEbayReady(0,0).Select(x => x.GetEan());
            File.WriteAllLines("ebayReady.txt", ebayReadyEans);    
        }
    }
}
