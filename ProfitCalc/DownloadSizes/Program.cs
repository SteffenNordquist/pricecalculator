﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DownloadSizes
{
    class Program
    {   
        public static MongoClient Client = null;
        public static MongoServer Server = null;
        public static MongoDatabase PDatabase = null;

        public static MongoCollection<BsonDocument> mongoCollection;

        static void Main(string[] args)
        {
            Client = new MongoClient(@"mongodb://148.251.0.235");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("ProductDatabase");
            mongoCollection = PDatabase.GetCollection<BsonDocument>("productsizes");

            int counter = 0;

            MongoCollection<BsonDocument> productCollection = PDatabase.GetCollection<BsonDocument>("products");
            var query = Query.And(Query.NE("doc.product.supplydetail.stockdate", "2015124"), Query.NE("doc.product.supplydetail.stock.j350", 0));
         
            var items = productCollection.Find(query).SetFields("_id").ToList();

            //long count = items.Count();

            BsonDocument bson1 = new BsonDocument();
            bson1.Add("j350", 0);
            var update = Update.Set("doc.product.supplydetail.stock", bson1);
            //productCollection.Update(query, update);


            int updateCounter = 0;

            foreach (var item in items)
            {
                var id = item["_id"];
                var query2 = Query.EQ("_id", id);
                productCollection.Update(query2, update);

                if (updateCounter++.ToString().EndsWith("00"))
                {
                    Console.WriteLine(updateCounter);
                }
            }

            //var lines = mongoCollection.FindAll().Select(x => getMeasurementString(x, ref counter)).ToList();
            //File.WriteAllLines("C:\\temp\\measurements.txt", lines);        
        }

        private static string getMeasurementString(BsonDocument doc, ref int counter )
        {
            string ean = doc["ean"].AsString; 

            int length = doc["measure"]["length"].AsInt32;
            int width = doc["measure"]["width"].AsInt32;
            int height = doc["measure"]["height"].AsInt32;
            int weight = doc["measure"]["weight"].AsInt32;

            if (counter.ToString().EndsWith("000"))
            {
                Console.WriteLine(counter);
            }

            counter++; 
            return string.Format("{0}\t{1}\t{2}\t{3}\t{4}", ean, length,width, height, weight);


            
        }

    }
}
