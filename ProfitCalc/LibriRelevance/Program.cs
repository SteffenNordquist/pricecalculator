﻿

using ProcuctDB;
using ProcuctDB.Libri;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferConsumer;

namespace LibriRelevance
{
    class Program
    {
        static void Main(string[] args)
        {
            LibriTransfer libriTransfer = new LibriTransfer();
            var flexibleEans = libriTransfer.transferLines.Where(x => x.priceTypeEnum == priceTypeEnum.flexiblePrice).Select(x => x.identifier).ToList();
            getRelevanceForEanList(flexibleEans); 
        }

        private static void FindRelevanceForEanListFile()
        {
            var eanList = File.ReadAllLines("C:\\temp\\ebayReadyEan.txt");

            getRelevanceForEanList(eanList);
        }

        private static void getRelevanceForEanList(IEnumerable<string> eanList)
        {
            var db = new LibriDB();

            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = 8;

            List<string> list = new List<string>();

            Parallel.ForEach(eanList, po, ean =>
            {
                var libri = (LibriEntity)db.GetMongoProductByEan(ean);
                int relvance = int.Parse(libri.getRelevanceAsString());

                list.Add(ean + "\t" + relvance);

                Console.WriteLine(ean + "\t" + relvance);

            });
            File.WriteAllLines("relevance.txt", list);

            Process.Start("relevance.txt");
        }
    }
}
