﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonRepricerDll
{
    public class RepricerDB
    {
        public static MongoClient Client = null;
        public static MongoServer Server = null;
        public static MongoDatabase PDatabase = null;

        public static MongoCollection<RepricerEntity> mongoCollection;


        public RepricerDB()
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/RepricerCollection");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("RepricerCollection");
            mongoCollection = PDatabase.GetCollection<RepricerEntity>("flexibleitems",WriteConcern.Unacknowledged);



        }


        public List<RepricerEntity> FindAll(int max)
        {
            return mongoCollection.FindAll().SetLimit(max).ToList();
        }

        public void addAsin(string asin)
        {
            RepricerEntity repricerEntity = new RepricerEntity();
            repricerEntity.asin = asin;

            //if (mongoCollection.Find(Query.EQ("asin", asin)).Count() == 0)
            //{
            //    mongoCollection.Insert(repricerEntity);
            //}
            mongoCollection.Insert(repricerEntity);
        }





    }
}
