﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonRepricerDll
{
    public class CompetitivePriceEntity
    {
        public ObjectId Id { get; set; }
        public DateTime timeStamp{get; set; }
        public string asin { get; set;  }
        public double price { get; set; }
        public string categoryName { get; set; }
        public string categoryRank { get; set; }


        public CompetitivePriceEntity(string asin, double price)
        {
            timeStamp = DateTime.Now;
            this.asin = asin;
            this.price = price; 
        }

        public CompetitivePriceEntity(string asin, double price, string categoryName, string categoryRank)
        {
            timeStamp = DateTime.Now;
            this.asin = asin;
            this.price = price;
            this.categoryName = categoryName;
            this.categoryRank = categoryRank; 
        }
    }
}
