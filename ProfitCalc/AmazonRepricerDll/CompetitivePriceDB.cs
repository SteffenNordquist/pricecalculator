﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonRepricerDll
{

    public class CompetitivePriceDB
    {
        public static MongoClient Client = null;
        public static MongoServer Server = null;
        public static MongoDatabase PDatabase = null;

        public static MongoCollection<CompetitivePriceEntity> mongoCollection;


        public CompetitivePriceDB()
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/RepricerCollection");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("RepricerCollection");
            mongoCollection = PDatabase.GetCollection<CompetitivePriceEntity>("competitivePrice");

        }


        public void insertCompetitivePrice(string asin, double price, string categortyName, string categoryRank)
        {
            DateTime dateTime = DateTime.Now;
            var priceitems = mongoCollection.Find(Query.EQ("asin", asin)).OrderByDescending(x => x.timeStamp);

            if (priceitems.Count() > 0)
            {
                if (priceitems.First().timeStamp < DateTime.Now.AddDays(-7))
                {
                    mongoCollection.Insert(new CompetitivePriceEntity(asin, price, categortyName, categoryRank));
                }
            }
            else
            {
                mongoCollection.Insert(new CompetitivePriceEntity(asin, price, categortyName, categoryRank));
            }
        }


        public CompetitivePriceEntity FindByAsin(string asin)
        {
            return mongoCollection.FindOne(Query.EQ("asin", asin));
        }
    }
}
