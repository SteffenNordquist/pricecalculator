﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonRepricerDll
{
    [BsonIgnoreExtraElements]
    public class RepricerEntity
    {
        public ObjectId Id { get; set; }
        public string ean = string.Empty;
        public string asin = string.Empty;
        public double price = 0.0;
        public double priceAmazon = 0.0;
        public double priceFBA = 0.0;
        public DateTime lastRun = new DateTime();
        public DateTime lastPriceDate = new DateTime();
        public List<string> sellerList = new List<string>(); 
    }
}
