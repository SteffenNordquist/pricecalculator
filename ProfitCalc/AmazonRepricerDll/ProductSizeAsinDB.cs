﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonRepricerDll
{
    public class ProductSizeAsinDB
    {
        public static MongoClient Client = null;
        public static MongoServer Server = null;
        public static MongoDatabase PDatabase = null;

        public static MongoCollection<ProductSizeAsinEntity> mongoCollection;


        public ProductSizeAsinDB()
        {
            Client = new MongoClient(@"mongodb://client148:client148devnetworks@148.251.0.235/ProductDatabase");
            Server = Client.GetServer();
            PDatabase = Server.GetDatabase("ProductDatabase");
            mongoCollection = PDatabase.GetCollection<ProductSizeAsinEntity>("productSizesAsin");
        }

        public void insertProductSizeAsin(ProductSizeAsinEntity productSize)
        {
            if (AsinExists(productSize.asin))
            {
            }
            else
            {
                mongoCollection.Insert(productSize);
            }
        }


        public bool AsinExists(string asin)
        {
            var priceitems = mongoCollection.Find(Query.EQ("asin", asin));

            if (priceitems.Count() > 0)
            {
                return true;
            }
            else {
                return false;
            }
        }


        public ProductSizeAsinEntity FindByAsin(string asin)
        {
            return mongoCollection.FindOne(Query.EQ("asin", asin));
        }
    }
}
