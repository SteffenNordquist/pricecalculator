﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmazonRepricerDll
{
    public class ProductSizeAsinEntity
    {
        public ObjectId Id { get; set; }
        public string asin { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int weight { get; set; }


        public ProductSizeAsinEntity(string asin, int length, int width, int height, int weight)
        {
            List<int> measures = new List<int>();
            measures.Add(length);
            measures.Add(width);
            measures.Add(height);

            measures = measures.OrderByDescending(x => x).ToList();

            this.length = measures[0];
            this.width = measures[1];
            this.height = measures[2];

            this.weight = weight;
            this.asin = asin; 

        }

    }
}
