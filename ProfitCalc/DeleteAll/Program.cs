﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DeleteAll
{
    class Program
    {
        static string transferXmlDirectory = Properties.Settings.Default.TransferXmlDirectory;
        static string inventoryDirectory = Properties.Settings.Default.InventoryDirectory;

        static void Main(string[] args)
        {            
            DeleteAllFilesInXMLFolder();
            var allLines = File.ReadAllLines(inventoryDirectory).Where(x => x.Length >0).Skip(1).Select(x => x.Split('\t')[0]).ToList();
            CreateProductFiles(allLines); 
        }

        private static void DeleteAllFilesInXMLFolder()
        {
            DirectoryInfo di = new DirectoryInfo(transferXmlDirectory);
            if (!di.Exists)
            {
                di.Create();
            }

            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
        }

        private static int makeEnvelopeFile(StringBuilder productList, int fileCount, string messageType)
        {
            string Envelope_Template = File.ReadAllText("Templates\\Envelope_Template.xml");
            string envelope = Envelope_Template.Replace("@PRODUCTLIST@", productList.ToString()).Replace("@MESSAGETYPE@", messageType).Replace("@MERCHANTID@", Properties.Settings.Default.MerchantId);
            File.WriteAllText(string.Format(transferXmlDirectory + @"\{1}{0}.xml", ++fileCount, messageType), envelope);
            productList.Clear();
            return fileCount;
        }
       

        private static void CreateProductFiles(List<string> productList)
        {
            string Product_Template = File.ReadAllText("Templates\\Delete_Product_Template.xml");
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Product";

            foreach (var product in productList)
            {
                string productMessage = Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product);
                messageList.AppendLine(productMessage);
                itemCount++;

                if (itemCount == 10000)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }
    }
}
