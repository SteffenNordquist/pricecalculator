﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AmazonSalesReport
{
    public class SalesReport
    {
        public List<SalesReportLine> salesReportList = new List<SalesReportLine>();

        public SalesReport(string path)
        {
           salesReportList= File.ReadAllLines(path).Where(x=>x.Length >0).Skip(1).Select(x => new SalesReportLine(x)).ToList(); 
        }
    }
}
