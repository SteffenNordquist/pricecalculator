﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace AmazonSalesReport
{
    public class SalesReportLine
    {
        public string order_id = string.Empty;
        public string order_item_id = string.Empty;
        public string purchase_date = string.Empty;
        public string payments_date = string.Empty;
        public string buyer_email_buyer_name = string.Empty;
        public string buyer_phone_number = string.Empty;
        public string sku_product_name = string.Empty;
        public string quantity_purchased = string.Empty;
        public string currency = string.Empty;
        public double item_price = 0;
        public string item_tax = string.Empty;
        public string shipping_price = string.Empty;
        public string shipping_tax = string.Empty;
        public string ship_service_level = string.Empty;
        public string recipient_name = string.Empty;
        public string ship_address_1 = string.Empty;
        public string ship_address_2 = string.Empty;
        public string ship_address_3 = string.Empty;
        public string ship_city = string.Empty;
        public string ship_state = string.Empty;
        public string ship_postal_code = string.Empty;
        public string ship_country = string.Empty;
        public string ship_phone_number = string.Empty;
        public string bill_address_1 = string.Empty;
        public string bill_address_2 = string.Empty;
        public string bill_address_3 = string.Empty;
        public string bill_city = string.Empty;
        public string bill_state = string.Empty;
        public string bill_postal_code = string.Empty;
        public string bill_country = string.Empty;
        public string delivery_start_date = string.Empty;
        public string delivery_end_date = string.Empty;
        public string delivery_time_zone = string.Empty;
        public string delivery_Instructions = string.Empty;
        public string sales_channel = string.Empty;

        public SalesReportLine(string line)
        {
            var tokens = line.Split('\t');
            sku_product_name = tokens[7];
            item_price = double.Parse(tokens[11], CultureInfo.InvariantCulture);
        }
    }
}
