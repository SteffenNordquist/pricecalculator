﻿using DN_Classes.Conditions;
using DN_Classes.Supplier;
using PriceCalculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitCalcEbay
{
    public interface IPlatformConditionFactory
    {
        PlatformConditions getPlatFormConditionByEan(string EAN);
        PlatformConditions getPlatFormConditionByPlatformId(string PlatformId);
    }
}
