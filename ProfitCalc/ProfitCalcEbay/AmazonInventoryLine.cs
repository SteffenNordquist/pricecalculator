﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProfitCalcEbay
{
    public class AmazonInventoryLine
    {
        public string sku;
		public string asin;
		public string quantity;
		public string price;
        public string shippingFulfillmentLatency; 


	
		/// <summary>
		/// will send a quantity cleanup to clear old // default true
		/// </summary>
		public bool sendCleanUp = true; 		

		public AmazonInventoryLine(string line)
		{
			string[] itemlist = line.Split('\t');
			sku = itemlist[0];
			asin = itemlist[1];
			price = itemlist[2];
			quantity = itemlist[3];

            if (itemlist.Count() > 4)
            {
                shippingFulfillmentLatency = itemlist[4];
            }

			sendCleanUp = !(quantity == "0");
		}

        public AmazonInventoryLine(string sku, string asin, string price, string quantity, string shippingFulfillmentLatency)
		{
			this.sku = sku;
			this.asin = asin;
			this.price = price;
			this.quantity = quantity;
			sendCleanUp = !(quantity == "0");
            this.shippingFulfillmentLatency = shippingFulfillmentLatency;
		}
		
		public override string ToString()
		{
			return sku + "\t" + asin + "\t" + price + "\t" + quantity + "\t" + shippingFulfillmentLatency;
		}

		public void noCleaUp()
		{
			sendCleanUp = false;
		}
    }
}
