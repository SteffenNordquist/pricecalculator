﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;

namespace ProfitCalcEbay
{
    class CompetitivePriceAmazon
    {
        public Dictionary<string, double> prices = new Dictionary<string, double>(); 

        public CompetitivePriceAmazon()
        {
            WebClient wc = new WebClient();
            string url = Properties.Settings.Default.CompetititvePriceUrl;

            try {
                var lines = wc.DownloadString(url).Trim().Replace("\r\n", "@").Split('@');
                foreach (var line in lines)
                {
                    var token = line.Split("\t".ToCharArray()); 
                    prices.SafeAdd(token[0], double.Parse(token[2],CultureInfo.InvariantCulture));
                }
            }
            catch { }
            
        }
    }
}
