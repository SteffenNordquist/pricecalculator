﻿using DN_Classes.Agent;
using DN_Classes.Conditions;
using DN_Classes.Supplier;
using PriceCalculation;
using ProcuctDB;
using ProcuctDB.Knv;
using ProcuctDB.Libri;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using TransferConsumer;

namespace ProfitCalcEbay
{
    class Program
    {

        static string conditionNote = "";
        static string merchantId = "";


        static Dictionary<string, Measurements> measurementsDict = new Dictionary<string, Measurements>();
        static XmlDocument settingsDoc = new XmlDocument();
        static CompetitivePriceAmazon competitivePriceAma = new CompetitivePriceAmazon();

        static string transferXmlDirectory = Properties.Settings.Default.TransferXmlDirectory;
        static string inventoryDirectory = Properties.Settings.Default.InventoryDirectory;


        static Random random = new Random();

        static void Main(string[] args)
        {

            settingsDoc.Load(Properties.Settings.Default.SettingsXML);
            conditionNote = settingsDoc.SelectSingleNode("//conditionNote").InnerText;
            merchantId = settingsDoc.SelectSingleNode("//merchantId").InnerText;



            if (Properties.Settings.Default.StartMode == "ConditionNoteChange")
            {
                ConditionNoteChange();
            }
            else if (Properties.Settings.Default.StartMode == "DeleteAll")
            {
                DeleteAll();
            }
            else
            {
                RegularUpdating();
            }

            
        }




        private static void refreshCategoriesFile(string path, TimeSpan maxAge, string url)
        {
            FileInfo fileInfo = new FileInfo(path);
            if (fileInfo.LastWriteTime.Add(maxAge) < DateTime.Now)
            {
                WebClient webClient = new WebClient();
                try
                {
                    webClient.DownloadFile(url, path);
                    Console.WriteLine("categories successfully downloaded");
                }
                catch(Exception e)
                {
                    Console.WriteLine(string.Format("Could not download categories from {0} to {1}. Reason: {2}", url, path, e.ToString())); 
                }
            }else
            {
            Console.WriteLine("categories is up to date"); 
            }
        }


        private static void ConditionNoteChange()
        {
            string renameFolder = "C:\\amaRename\\";
            DeleteAllFilesInXMLFolder(renameFolder);

            var inventoryDict = getInventoryDict();
            var inventoryToRename = inventoryDict.Where(x => x.Value.quantity != "0").Select(x => x.Value).ToList();
            var inventoryToDelete = inventoryDict.Where(x => x.Value.quantity == "0").Select(x => x.Value).ToList();

            int fileCount = CreateProductFiles(inventoryToRename, 0);
            CreateDeleteFiles(inventoryToDelete, fileCount);
        }


        private static void DeleteAll()
        {
            string renameFolder = transferXmlDirectory;
            DeleteAllFilesInXMLFolder(renameFolder);

            var inventoryDict = getInventoryDict();
            var inventoryToDelete = inventoryDict.Select(x => x.Value).ToList();

            int fileCount = 0;
            CreateDeleteFiles(inventoryToDelete, fileCount);
        }

        private static void RegularUpdating()
        {
            refreshCategoriesFile(Properties.Settings.Default.CategoryTXT, TimeSpan.FromHours(12), Properties.Settings.Default.CategoryExport);          


            var inventoryDict = getInventoryDict();

            List<string> measurementLines = new List<string>();
            if (new FileInfo(Properties.Settings.Default.MeasurementsTXT).Exists)
            {
                File.ReadAllLines(Properties.Settings.Default.MeasurementsTXT).ToList();
            }

            measurementsDict = new Dictionary<string, Measurements>();
            foreach (var line in measurementLines)
            {
                var tokens = line.Split('\t');
                DictionaryExtensions.SafeAdd(measurementsDict, tokens[0], new Measurements(int.Parse(tokens[1]), int.Parse(tokens[2]), int.Parse(tokens[3]), int.Parse(tokens[4])));

            }



            DeleteAllFilesInXMLFolder();

            PackingList packingList = new PackingList();

            Dictionary<string, Product> finalProducts = new Dictionary<string, Product>();

            Agent agent;

            if (Properties.Settings.Default.AgentID.Length > 0)
            {
                agent = new Agent(int.Parse(Properties.Settings.Default.AgentID));
            }
            else 
            {
                agent = new Agent();
            }
            
            SupplierDB supplierDB = new SupplierDB();
            SupplierEntity supplierEntity = null;

            foreach (var supplier in agent.agentEntity.agent.platforms.Find(x=>x.name == "amazon").suppliers)
            {
                    supplierEntity = supplierDB.GetSupplier(supplier.name);
                    ProfitConditions profitConditions = new ProfitConditions(supplier.ProfitWanted, supplier.RiskFactor, supplier.ItemHandlingCosts);

                    //not sure with platform condition


                    TransferConsumerTransferType transferConsumerType = TransferConsumerTransferType.ASIN;

                    int minQuantity = 0;

                    if (supplier.name.Contains("KNV") || supplier.name.Contains("LIBRI") || supplier.name.Contains("BVW"))
                    {
                        transferConsumerType = TransferConsumerTransferType.EAN;

                        minQuantity = Properties.Settings.Default.MinQuantityAvailableKNV; 
                    }

                                      

                    var transferFile = supplierEntity.GetTransferFile();
                    finalProducts = GenericHandling(1, inventoryDict, packingList, finalProducts, transferFile , supplierEntity.contractCondition, profitConditions, supplierEntity.platformCondition, transferConsumerType, supplier.DaysToDeliveryTimestamp, supplier.GetDaysToDelivery(), minQuantity);
                
            }




            List<Product> finalList = new List<Product>();
            finalList = finalProducts.Select(x => x.Value).ToList();


            finalList = finalList.Where(x => x.quantity > 0)
                .Where(x => x.price < Properties.Settings.Default.PriceLimit).ToList();


            CreateProductFiles(finalList, inventoryDict);
            CreatePriceFiles(finalList, inventoryDict);
            CreateInventoryFiles(finalList, inventoryDict);
        }




        private static Dictionary<string, Product> GenericHandling(int minimumQuantity, Dictionary<string, AmazonInventoryLine> inventoryDict, PackingList packingList, Dictionary<string, Product> finalProducts, string transferUrl, ContractConditions contractConditions, ProfitConditions profitConditions, PlatformConditions defaultPlaformConditions, TransferConsumerTransferType transferConsumerTransferType, DateTime deliveryTimeChangedDate, int deliveryTime, int minQuantity)
        {
            if(transferUrl.Contains("jtl"))
            {}
            IdentifierType identifierType = IdentifierType.ASIN;

            if (transferConsumerTransferType == TransferConsumerTransferType.EAN)
            {
                identifierType = IdentifierType.EAN;
            }
            var lines = new GenericTransfer(transferUrl,transferConsumerTransferType).transferLines.Where(x => x.stock > 0).Select(x => x.rawLine).ToArray();

            IPlatformConditionFactory platformConditionFactory = new AmazonPlatformConditionFactory(defaultPlaformConditions, inventoryDict);

            List<Product> profitableProducts = GetProfitableFixedPriceProductsByPlatformId(platformConditionFactory, profitConditions, contractConditions, packingList, lines, identifierType);
            List<Product> flexibleProducts = CalcPriceByPlatformId(platformConditionFactory, profitConditions, contractConditions, packingList, lines, identifierType);
            RePricing(ref flexibleProducts, inventoryDict);

            profitableProducts.AddRange(flexibleProducts);

            //min Quantities
            profitableProducts = profitableProducts.Where(x => x.quantity >= minimumQuantity).ToList();

            profitableProducts.ForEach(x => x.fulfillmentLatency = deliveryTime);
            profitableProducts.ForEach(x => x.fullFillmentLatencyChangedDate = deliveryTimeChangedDate);

            profitableProducts.ForEach(x => x.quantity = x.availableQuantity >= minQuantity ? x.quantity : 0);

            finalProducts = AddProducts(finalProducts, profitableProducts);
            return finalProducts;
        }


        private static void RePricing(ref List<Product> flexibleProducts,Dictionary<string, AmazonInventoryLine> inventoryDict )
        {
            foreach (var flexibleProduct in flexibleProducts)
            {
                if (flexibleProduct.identifier == "B00610472U")
                { }

                if (flexibleProduct.identifierType == IdentifierType.ASIN || inventoryDict.ContainsKey(flexibleProduct.identifier))
                {
                    string asin = "";

                    if (flexibleProduct.identifierType == IdentifierType.ASIN)
                    {
                        asin = flexibleProduct.identifier;
                    }
                    else
                    {
                        asin = inventoryDict[flexibleProduct.identifier].asin;
                    }

                    if (asin == "B00610472U")
                    { }
                    double competitivePrice = 0;

                    if (competitivePriceAma.prices.ContainsKey(asin))
                    {
                        competitivePrice = competitivePriceAma.prices[asin];
                    }

                    if (competitivePrice == 0)
                    {
                        competitivePrice = flexibleProduct.price + 4 + flexibleProduct.price * 0.35;
                    }

                    double shipping = 3;
                    double randomCents = ((double)random.Next(4)) / 100;
                    double randomizedCompetitive = competitivePrice - randomCents - 0.01;


                    if (flexibleProduct.price < randomizedCompetitive - shipping)
                    { }

                    flexibleProduct.price = Math.Round(Math.Max(flexibleProduct.price, randomizedCompetitive - shipping), 2);



                }
            }
        }



        private static void AggressiveRePricing(ref List<Product> flexibleProducts, Dictionary<string, AmazonInventoryLine> inventoryDict, int percentBelow)
        {
            foreach (var flexibleProduct in flexibleProducts)
            {

                if (flexibleProduct.identifier == "B00610472U")
                { }

                if (flexibleProduct.identifierType == IdentifierType.ASIN || inventoryDict.ContainsKey(flexibleProduct.identifier))
                {
                    string asin = "";

                    if (flexibleProduct.identifierType == IdentifierType.ASIN)
                    {
                        asin = flexibleProduct.identifier;
                    }
                    else
                    {
                        asin = inventoryDict[flexibleProduct.identifier].asin;
                    }

                    if (competitivePriceAma.prices.ContainsKey(asin))
                    {
                        if (asin == "B00610472U")
                        { }
                        double competitivePrice = competitivePriceAma.prices[asin];

                        if (competitivePrice == 0)
                        {
                            competitivePrice = flexibleProduct.price + 3 + flexibleProduct.price * 0.08;
                        }

                        double shipping = 3;
                        double randomCents = ((double)random.Next(4)) / 100;
                        double randomizedCompetitive = competitivePrice - randomCents - 0.01;


                        double aggressivePrice = randomizedCompetitive * (100 - percentBelow) / 100 - shipping;
                       // double finalCompetitivePrice = randomizedCompetitive - shipping;


                        if (flexibleProduct.price <= aggressivePrice)
                        {
                            flexibleProduct.price = Math.Round(Math.Max(flexibleProduct.price, aggressivePrice), 2);
                        }
                        else {
                            flexibleProduct.price = Math.Round(flexibleProduct.price, 2);
                        }
                        //else if (flexibleProduct.price > aggressivePrice && flexibleProduct.price <= finalCompetitivePrice)
                        //{
                        //    flexibleProduct.price = Math.Round(flexibleProduct.price, 2);
                        //}
                        //else 
                        //{
                        //    flexibleProduct.price = Math.Round(Math.Max(flexibleProduct.price, finalCompetitivePrice), 2);
                        //}


                    }
                }
            }
        }

        private static Dictionary<string, AmazonInventoryLine> getInventoryDict()
        {
            var inventoryLines = File.ReadAllLines(inventoryDirectory +"\\InventoryReport.txt").Where(x => x.Split('\t').Count() > 3).Select(x => new AmazonInventoryLine(x)).ToList();
            var inventoryDict = inventoryLines.GroupBy(x => x.sku).Where(x => !x.Key.StartsWith("ignore")).ToDictionary(x => x.Key, y => y.First());
            return inventoryDict;
        }


        private static Dictionary<string, Product> AddProducts(Dictionary<string, Product> finalProducts, List<Product> profitableProducts)
        {
            foreach (var product in profitableProducts)
            {
                string identifier = "";

                if (product.identifierType == IdentifierType.ASIN)
                {
                    identifier = product.sku;
                }
                else
                {
                    identifier = product.identifier;
                }

                if (identifier == "B00VV7IC74")
                { }
                if (!finalProducts.ContainsKey(identifier))
                {
                    finalProducts.Add(identifier, product);
                }
                else
                {
                    var price = product.price;
                    var foundPrice = finalProducts[identifier].price;
                    if (foundPrice > product.price)
                    {
                        finalProducts[identifier] = product;
                    }
                }
            }

            return finalProducts; 
        }

        private static void singleProfit(ProfitConditions profitConditions, PackingList packingList, PlatformConditions amazonBooks, ContractConditions knvContractconditions)
        {
            var knvDB = new KnvDB("148.251.0.235");

            var price = 7.63;

            var iProduct = knvDB.GetMongoProductByEan("9783808532270");

            var p2 = new Product(iProduct, knvContractconditions, profitConditions, amazonBooks, packingList);
            p2.CalcSellPrice(3);

            var sellPrice = p2.price;

            if (iProduct != null)
            {
                var p = new Product(iProduct, knvContractconditions, profitConditions, amazonBooks, packingList);

                double profit = p.getProfit(3.7, 3);


                if (p.quantity == 0)
                { }
            }
        }

        private static void CreateProfitOverview(ProfitConditions profitConditions, PackingList packingList, PlatformConditions amazonBooks, ContractConditions knvContractconditions)
        {
            var bestLines = File.ReadAllLines("C:\\temp\\crazy.txt").ToList();
            bestLines.RemoveAt(0);


            var knvDB = new KnvDB("148.251.0.235");

            foreach (var line in bestLines)
            {

                var tokens = line.Split('\t');
                var sku = tokens[7];
                var price = double.Parse(tokens[11]);

                var iProduct = knvDB.GetMongoProductByEan(sku);

                //var Iproduct2 = knvDB.GetMongoProductByEan(sku);
                //var p2 = new Product(iProduct, knvContractconditions, profitConditions, amazonBooks, packingList);

                //p2.CalcSellPrice(3); 

                if (iProduct != null)
                {
                    var p = new Product(iProduct, knvContractconditions, profitConditions, amazonBooks, packingList);

                    double profit = p.getProfit(price, 3);
                }
            }
        }





        private static void DeleteAllFilesInXMLFolder()
        {
            DeleteAllFilesInXMLFolder(transferXmlDirectory); 
        }


        private static void DeleteAllFilesInXMLFolder(string directoryName)
        {
            DirectoryInfo di = new DirectoryInfo(directoryName);
            if (!di.Exists)
            {
                di.Create();
            }

            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }
        }


        private static void CreateProductFiles(List<Product> productList, Dictionary<string, AmazonInventoryLine> inventoryDict)
        {
            string Product_Template = File.ReadAllText("Templates\\Product_Template.xml");
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Product";

            foreach (var product in productList)
            {
            
                
                if (product.identifierType == IdentifierType.EAN)
                {
                    if (!inventoryDict.ContainsKey(product.identifier))
                    {
                        string productMessage = Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.identifier).Replace("@TYPE@", product.identifierType.ToString()).Replace("@VALUE@", product.identifier).Replace("@CONDITIONNOTE@", conditionNote);
                        messageList.AppendLine(productMessage);
                        itemCount++;
                    }
                }
                else {
                    if (!inventoryDict.ContainsKey(product.sku))
                    {
                        string productMessage = Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", product.sku).Replace("@TYPE@", product.identifierType.ToString()).Replace("@VALUE@", product.identifier).Replace("@CONDITIONNOTE@", conditionNote);
                        messageList.AppendLine(productMessage);
                        itemCount++;
                    }
                }

                if (itemCount == 15000)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }

        private static string GetIdentifier(Product product)
        {
            string identifier = "";
            if (product.identifierType == IdentifierType.ASIN)
            {
                identifier = product.sku;
            }
            else
            {
                identifier = product.identifier;
            }

            return identifier; 
        }


        private static int CreateProductFiles(List<AmazonInventoryLine> inventoryList, int fileCount)
        {
            string Product_Template = File.ReadAllText("Templates\\Product_Template.xml");
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;


            int itemCount = 0;

            string messageType = "Product";

            foreach (var inventoryLine in inventoryList)
            {

                string productMessage = Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", inventoryLine.sku).Replace("@TYPE@", "ASIN").Replace("@VALUE@", inventoryLine.asin).Replace("@CONDITIONNOTE@", conditionNote);
                    messageList.AppendLine(productMessage);
                    itemCount++;
                
                if (itemCount == 15000)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType, @"C:\amaRename\");
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType, @"C:\amaRename\");
            }

            return fileCount;
        }


        private static int CreateDeleteFiles(List<AmazonInventoryLine> inventoryList, int fileCount)
        {
            string Product_Template = File.ReadAllText("Templates\\Delete_Product_Template.xml");
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;


            int itemCount = 0;

            string messageType = "Product";

            foreach (var inventoryLine in inventoryList)
            {

                string productMessage = Product_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", inventoryLine.sku);
                messageList.AppendLine(productMessage);
                itemCount++;

                if (itemCount == 15000)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType, transferXmlDirectory);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType, transferXmlDirectory);
            }

            return fileCount; 
        }

        private static void CreateInventoryFiles(List<Product> productList, Dictionary<string, AmazonInventoryLine> inventoryDict)
        {
            string Inventory_Template = File.ReadAllText("Templates\\Inventory_Template.xml");
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Inventory";


            foreach (var product in productList)
            {

                string identifier = "";

                if (product.identifierType == IdentifierType.ASIN)
                {
                    identifier = product.sku;
                }
                else
                {
                    identifier = product.identifier;
                }

                if (identifier == "9783730700969")
                { }

                bool found = inventoryDict.ContainsKey(identifier);

                int inventoryQuantity = 0; 
                if(found)
                {
                int.TryParse(inventoryDict[identifier].quantity, out inventoryQuantity);
                }
                

                if (!found || found && product.quantity.ToString() != inventoryDict[identifier].quantity && (product.quantity < 5 || inventoryQuantity < 5))
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", identifier).Replace("@QUANTITY@", product.quantity.ToString()).Replace("@FULFILLMENTLATENCY@", product.fulfillmentLatency.ToString());
                   
                    messageList.AppendLine(productMessage);
                    if (itemCount++ == 15000 - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }


                if (found)
                {
                    inventoryDict[identifier].sendCleanUp = false;
                }
                //else if (found && inventoryDict[product.ean].quantity != "0")
                //{
                        
                //}
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }

            itemCount = 0;

            // make lines quantitiy 0, that hava cleanup
            foreach (var iLines in inventoryDict)
            {
                if (iLines.Value.sendCleanUp)
                {
                    string productMessage = Inventory_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", iLines.Value.sku).Replace("@QUANTITY@", "0").Replace("@FULFILLMENTLATENCY@", "1");
                    messageList.AppendLine(productMessage);
                    if (itemCount++ == 15000 - 1)
                    {
                        fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                        itemCount = 0;
                    }
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
            }
        }


        private static void CreatePriceFiles(List<Product> productList, Dictionary<string, AmazonInventoryLine> inventoryDict)
        {
            StringBuilder messageList = new StringBuilder();
            long messageId = DateTime.Now.Ticks;

            string Price_Template = File.ReadAllText("Templates\\Price_Template.xml");

            int fileCount = 0;
            int itemCount = 0;

            string messageType = "Price";





            foreach (var product in productList)
            {
                string identifier = GetIdentifier(product);
                bool found = inventoryDict.ContainsKey(identifier);
                double inventoryPrice = 0.0;
                
                if(found)
                {
                    //string asin = inventoryDict[identifier].asin; 
                    double.TryParse(inventoryDict[identifier].price, NumberStyles.Any,CultureInfo.InvariantCulture,out inventoryPrice); 
                }
                
                double priceDifference = Math.Abs(inventoryPrice - product.price);           
              
                if (!found || (priceDifference > 0.06 && product.quantity > 0) || inventoryPrice == 0.0)
                {
                    string productMessage = Price_Template.Replace("@ID@", messageId++.ToString()).Replace("@SKU@", identifier).Replace("@CURRENCY@", product.currency).Replace("@STANDARDPRICE@", Math.Round(product.price,2).ToString(CultureInfo.InvariantCulture));

                    messageList.AppendLine(productMessage);
                    itemCount++;
                }
                if (itemCount == 15000)
                {
                    fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                    itemCount = 0;
                }
            }
            if (messageList.Length > 0)
            {
                fileCount = makeEnvelopeFile(messageList, fileCount, messageType);
                messageList.Clear();
            }
        }


        private static int makeEnvelopeFile(StringBuilder productList, int fileCount, string messageType)
        {
            return makeEnvelopeFile(productList, fileCount, messageType, transferXmlDirectory);
        }

        private static int makeEnvelopeFile(StringBuilder productList, int fileCount, string messageType, string folderName)
        {
            if (!new DirectoryInfo(folderName).Exists)
            {
                new DirectoryInfo(folderName).Create();
            }
            string Envelope_Template = File.ReadAllText("Templates\\Envelope_Template.xml");
            string envelope = Envelope_Template.Replace("@PRODUCTLIST@", productList.ToString()).Replace("@MESSAGETYPE@", messageType).Replace("@MERCHANTID@", merchantId);
            File.WriteAllText(string.Format(folderName + @"{1}{0}.xml", ++fileCount, messageType), envelope);
            productList.Clear();
            return fileCount;
        }
       

        private static void ProfitTest(ProfitConditions profitConditions, PackingList packingList, PlatformConditions amazonBooks, ContractConditions knvContractconditions)
        {
            //var libriDB = new LibriDB("148.251.0.235");
            var knvDB = new KnvDB("148.251.0.235");
            var iProduct = knvDB.GetMongoProductByEan("4260026935052");
            var p = new Product(iProduct, knvContractconditions, profitConditions, amazonBooks, packingList);

            p.CalcSellPrice(3);
            double profit = p.getProfit(p.price, 3);
        }

        private static List<Product> CalcPrice(IPlatformConditionFactory platformConditionFactory, ProfitConditions profitConditions, ContractConditions libri, PackingList packingList, string[] lines)
        {
            var flexiblePriceProducts = lines.Where(x => x.Contains("\t")).Select(x => new Product(x, libri, profitConditions, platformConditionFactory.getPlatFormConditionByEan(x.Split('\t')[1]), packingList, IdentifierType.EAN, Properties.Settings.Default.MaxQuantity)).Where(x => x.priceType == PriceTypeCalc.flexiblePrice && x.quantity > 0).ToList();

            foreach (var flexiblePriceProduct in flexiblePriceProducts)
            {
                AddProductSizesIfMissing(flexiblePriceProduct);
                flexiblePriceProduct.CalcSellPrice(3);
            }

            return flexiblePriceProducts; 
        }


        private static List<Product> CalcPriceByPlatformId(IPlatformConditionFactory platformConditionFactory, ProfitConditions profitConditions, ContractConditions libri, PackingList packingList, string[] lines, IdentifierType identifierType)
        {
            var flexiblePriceProducts = lines.Select(x => new Product(x, libri, profitConditions, platformConditionFactory.getPlatFormConditionByPlatformId(x.Split('\t')[1]), packingList, identifierType, Properties.Settings.Default.MaxQuantity)).Where(x => x.priceType == PriceTypeCalc.flexiblePrice && x.quantity > 0).ToList();

            
            foreach (var flexiblePriceProduct in flexiblePriceProducts)
            {
                if (flexiblePriceProduct.sku.Contains("4004117233531"))
                { }
                AddProductSizesIfMissing(flexiblePriceProduct);
                flexiblePriceProduct.CalcSellPrice(3);
            }

            return flexiblePriceProducts;
        }

        private static void AddProductSizesIfMissing(Product product)
        {
            string identifier = GetIdentifier(product); 
            if (!product.productSize.isComplete())
            {
                if (measurementsDict.ContainsKey(identifier))
                {
                    product.productSize.addSizesFromMeasurements(measurementsDict[identifier]);
                }
            }
        }

        private static List<Product> GetProfitableFixedPriceProducts(IPlatformConditionFactory platformConditionFactory, ProfitConditions profitConditions, ContractConditions contractCondtions, PackingList packingList, string[] lines)
        {
            
            List<Product> profitableProducts = new List<Product>(); 
            var fixPriceProducts = lines.Where(x => x.Length > 0 ).Select(x => new Product(x, contractCondtions, profitConditions, platformConditionFactory.getPlatFormConditionByEan(x.Split('\t')[1]), packingList, IdentifierType.EAN,Properties.Settings.Default.MaxQuantity)).Where(x => x.priceType == PriceTypeCalc.fixPrice && x.quantity > 0).ToList();
            foreach (var fixPriceProduct in fixPriceProducts)
            {
                AddProductSizesIfMissing(fixPriceProduct);

                if (fixPriceProduct.getProfit(fixPriceProduct.price, 3) >= profitConditions.ProfitWanted)
                {
                    profitableProducts.Add(fixPriceProduct);
                }
            }

            return profitableProducts; 
        }


        private static List<Product> GetProfitableFixedPriceProductsByPlatformId(IPlatformConditionFactory platformConditionFactory, ProfitConditions profitConditions, ContractConditions contractCondtions, PackingList packingList, string[] lines,IdentifierType identifierType )
        {

            List<Product> profitableProducts = new List<Product>();
            var fixPriceProducts = lines.Select(x => new Product(x, contractCondtions, profitConditions, platformConditionFactory.getPlatFormConditionByPlatformId(x.Split('\t')[1]), packingList, identifierType, Properties.Settings.Default.MaxQuantity)).Where(x => x.priceType == PriceTypeCalc.fixPrice && x.quantity > 0).ToList();
            foreach (var fixPriceProduct in fixPriceProducts)
            {
                AddProductSizesIfMissing(fixPriceProduct);

                if (fixPriceProduct.getProfit(fixPriceProduct.price, 3) >= profitConditions.ProfitWanted)
                {
                    profitableProducts.Add(fixPriceProduct);
                }
            }

            return profitableProducts;
        }

        private static void GetFilteredList(PlatformConditions ebay, ProfitConditions profitConditions, ContractConditions libri, PackingList packingList)
        {
            var lines = File.ReadAllLines("C:\\temp\\libritransfer.txt").Where(x => new SimplifiedProduct(x).price < 2 && new SimplifiedProduct(x).priceType == PriceTypeCalc.fixPrice);
            var filteredProducts = lines.Select(x => new Product(x, libri, profitConditions, ebay, packingList, IdentifierType.EAN, Properties.Settings.Default.MaxQuantity)).ToList();
            var eans = string.Join("\r\n", filteredProducts.Select(x => x.identifier));
        }

        private static void FindRelevanceFromEANList(LibriDB libriDB)
        {
            var eansFromFile = File.ReadAllLines("C:\\temp\\miniEAN.txt");

            List<string> relevanceList = new List<string>();
            foreach (var ean in eansFromFile)
            {
                var product = libriDB.GetMongoProductByEan(ean);
                var libriProduct = (LibriEntity)product;
                int relevance = int.Parse(libriProduct.getRelevanceAsString());
                relevanceList.Add(ean + "\t" + relevance);
            }
            File.WriteAllLines("C:\\temp\\miniRel.txt", relevanceList);
        }
    }

    public static class DictionaryExtensions
    {
        public static void SafeAdd<TKey, TValue>(this Dictionary<TKey, TValue> dict,
                                                 TKey key, TValue value)
        {
            dict[key] = value;
        }
    }
}
