﻿using DN_Classes.Conditions;
using DN_Classes.Supplier;
using PriceCalculation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProfitCalcEbay
{
    public class AmazonPlatformConditionFactory : IPlatformConditionFactory
    {

        private  Dictionary<string, string> categoryDict = new Dictionary<string, string>();
        private Dictionary<string, AmazonInventoryLine> inventoryDict = new Dictionary<string, AmazonInventoryLine>();

        private PlatformConditions defaultPlaformCondtions;


        public AmazonPlatformConditionFactory(PlatformConditions defaultPlaformCondtions, Dictionary<string, AmazonInventoryLine> inventoryDict)
        {
            this.inventoryDict = inventoryDict; 
            this.defaultPlaformCondtions = defaultPlaformCondtions;
            List<string> categoryLines = new List<string>();

            try
            {
                categoryLines = File.ReadAllLines(Properties.Settings.Default.CategoryTXT).ToList();
            }
            catch { }
            categoryDict = new Dictionary<string, string>();
            foreach (var line in categoryLines)
            {
                var tokens = line.Split('\t');
                DictionaryExtensions.SafeAdd(categoryDict, tokens[0], tokens[1]);
            }
        }


        public PlatformConditions getPlatFormConditionByEan(string EAN)
        {

            string ASIN = getASINfromEAN(EAN);
            return GetPlatformConditionByAsin(ASIN);
        }

        private PlatformConditions GetPlatformConditionByAsin(string ASIN)
        {
            if (categoryDict.ContainsKey(ASIN))
            {
                if (categoryDict[ASIN] == "toy_display_on_website")
                {
                    return new PlatformConditions(15, 0, 0);
                }
                else if (categoryDict[ASIN] == "musical_instruments_display_on_website")
                {
                    return new PlatformConditions(12, 0, 0);
                }
                else if (categoryDict[ASIN] == "pc_display_on_website")
                {
                    return new PlatformConditions(12, 0, 0);
                }
                else if (categoryDict[ASIN] == "ce_display_on_website")
                {
                    return new PlatformConditions(12, 0, 0);
                }
                else if (categoryDict[ASIN] == "office_product_display_on_website")
                {
                    return new PlatformConditions(15, 0, 0);
                }
                else if (categoryDict[ASIN] == "kitchen_display_on_website")
                {
                    return new PlatformConditions(15, 0, 0);
                }
                else if (categoryDict[ASIN] == "kitchen_display_on_website")
                {
                    return new PlatformConditions(20, 0, 0);
                }
                return defaultPlaformCondtions;
            }
            else
            {
                return defaultPlaformCondtions;
            }
        }

        public PlatformConditions getPlatFormConditionByPlatformId(string platformId)
        {
            return GetPlatformConditionByAsin(platformId);
        }


        private string getASINfromEAN(string EAN)
        {
            if (inventoryDict.ContainsKey(EAN))
            {
                return inventoryDict[EAN].asin;
            }
            else {
                return string.Empty;
            }
        }
    }
}
