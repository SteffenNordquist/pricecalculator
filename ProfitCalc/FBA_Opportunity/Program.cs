﻿using DN_Classes.Products;
using DN_Classes.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FBA_Opportunity
{
    class Program
    {
        static void Main(string[] args)
        {
            WareHouseQueries warehouseQueries = new WareHouseQueries();
            var items = warehouseQueries.GetWarehouseItemsBySupplier("HABA");

            ProductSizeDB productSizeDB = new ProductSizeDB(); 

            List<string> excludedSuppliers = new List<string>();
            excludedSuppliers.Add("KNV"); 

            foreach (var item in items)
            {
                //var products = productFinder.FindAllProductsByEan(item.product.ean, excludedSuppliers);
                var simpleSize = productSizeDB.GetProductSizeByEan(item.product.ean).GetSimpleSize();
                var volume = simpleSize.GetVolume();
            }
        }
    }
}
