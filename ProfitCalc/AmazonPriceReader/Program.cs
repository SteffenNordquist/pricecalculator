﻿using AmazonRepricerDll;
using DN_Classes.AppStatus;
using HtmlAgilityPack;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace AmazonPriceReader
{
    class Program
    {

        public static MongoClient Client = null;
        public static MongoServer Server = null;
        public static MongoDatabase PDatabase = null;

        public static MongoCollection<RepricerEntity> mongoCollection;

        


        static void downloadFile(IWebDriver driver, string url, string localPath)
        {
            var client = new WebClient();
            client.Headers[HttpRequestHeader.Cookie] = getCookieString(driver);
            client.DownloadFile(url, localPath);
        }
        static string getCookieString(IWebDriver driver)
        {
            var cookies = driver.Manage().Cookies.AllCookies;
            return string.Join("; ", cookies.Select(c => string.Format("{0}={1}", c.Name, c.Value)));
        }

        static void Main(string[] args)
        {


            while (true)
            {
                using (ApplicationStatus appStatus = new ApplicationStatus("Repricing_" + Environment.MachineName))
                {
                    try
                    {
                        //IWebDriver driver = new FirefoxDriver();
                        //driver.Url = "http://www.amazon.de";

                        string cookieString = "";// getCookieString(driver);

                        //driver.Close();
                        //driver.Dispose();

                        List<string> blackList = Properties.Settings.Default.SellerBlackList.Split(',').ToList();
                        //RepricerEntity repricerEntity = FindPrices(blackList, "B0056GJUE0", cookieString);



                        Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/RepricerCollection");
                        Server = Client.GetServer();
                        PDatabase = Server.GetDatabase("RepricerCollection");
                        mongoCollection = PDatabase.GetCollection<RepricerEntity>("flexibleitems", WriteConcern.Unacknowledged);



                        var newbees = Query.NotExists("lastRun");
                        var allNewBees = mongoCollection.Find(newbees).SetSkip(Properties.Settings.Default.skipPosition * 5000).SetLimit(5000).SetFields("asin", "lastRun");
                        findPrices(blackList, allNewBees, cookieString);

                        if (allNewBees.Count() == 0)
                        {
                            var oldies = mongoCollection.FindAll().SetFields("asin", "lastRun").ToList().OrderBy(x => x.lastRun).Skip(Properties.Settings.Default.skipPosition * 5000).Take(5000).ToList();

                            string oldestTime = oldies.First().lastRun.ToString("dd.MM.yyyy HH:mm");
                            Console.WriteLine(oldestTime);
                            Thread.Sleep(6000);

                            findPrices(blackList, oldies, cookieString);
                            Console.WriteLine(oldestTime);
                        }

                        appStatus.Successful();

                    }

                    catch (Exception e)
                    {
                        Console.WriteLine("general error\r\n" + e.ToString());
                        appStatus.AddMessagLine("general error\r\n" + e.ToString());

                        appStatus.AddMessagLine(e.ToString());

                    }
                }

            }



        }

        private static void findPrices(List<string> blackList, MongoCursor<RepricerEntity> allNewBees, string cookieString)
        {

            var asList = allNewBees.ToList();
            findPrices(blackList, asList, cookieString);
        }


        private static void findPrices(List<string> blackList, List<RepricerEntity> allNewBees, string cookieString)
        {
            DateTime thisRunTime = DateTime.Now;
            ParallelOptions po = new ParallelOptions();
            po.MaxDegreeOfParallelism = Properties.Settings.Default.parallel;

            Parallel.ForEach(allNewBees, po, newbee =>
            {
                double price = 0.0;

                string errorMessage = ""; 
                try
                {
                    RepricerEntity repricerEntity = FindPrices(blackList, newbee.asin, cookieString);
                    repricerEntity.ean = newbee.ean;
                    repricerEntity.asin = newbee.asin;
                    Console.WriteLine(newbee.lastRun + " " + repricerEntity.ean + " " + repricerEntity.price + " " + repricerEntity.priceFBA + " " + (repricerEntity.priceAmazon > 0 ? "A" + repricerEntity.priceAmazon : ""));

                    WritePrice(repricerEntity);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error:" + newbee.asin);
                    
                    if (!e.ToString().Contains("503") && !e.ToString().Contains("Captcha"))
                    {
                        RepricerEntity repricerEntity = new RepricerEntity();
                        repricerEntity.lastRun = thisRunTime;
                        repricerEntity.lastPriceDate = newbee.lastPriceDate;
                        repricerEntity.price = 0;
                        repricerEntity.priceFBA = 0;
                        repricerEntity.sellerList = new List<string>();
                        repricerEntity.asin = newbee.asin;

                        Console.WriteLine(newbee.lastRun + " " + repricerEntity.ean + " " + repricerEntity.price + " " + repricerEntity.priceFBA);

                        WritePrice(repricerEntity);

                    }
                    else {
                        Console.WriteLine(e.Message);
                    }

                    if (e.Message.Contains("Captcha"))
                    {
                        Thread.Sleep(5000);
                    }
                }
            });
        }

        private static void WritePrice(RepricerEntity repricerEntity)
        {
            try
            {
                var update = Update
                    .Set("price", Math.Round(repricerEntity.price, 2))
                    .Set("lastRun", repricerEntity.lastRun)
                    .Set("sellerList", new BsonArray(repricerEntity.sellerList))
                    .Set("lastPriceDate", repricerEntity.lastPriceDate)
                    .Set("priceFBA", repricerEntity.priceFBA)
                    .Set("priceAmazon", repricerEntity.priceAmazon);

                WriteConcernResult wcr = mongoCollection.Update(Query.EQ("asin", repricerEntity.asin), update);
            }
            catch
            {
                Console.WriteLine("DatabaseWritingError");
            }

        //    Console.WriteLine(repricerEntity.ean + " " + repricerEntity.price + " " + repricerEntity.priceFBA);
        }


        private static HtmlDocument DownloadHtml(string cookieString, string asin)
        {
            string urlTemplate = "http://www.amazon.de/gp/offer-listing/{0}/ref=olp_page_1?ie=UTF8&condition=new";
            WebClient wc = new WebClient();
            wc.Headers[HttpRequestHeader.Cookie] = cookieString;

            string html = wc.DownloadString(string.Format(urlTemplate, asin));
            if (html.Contains("Zeichen unten ein"))
            {
                throw new Exception("Captcha");
            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc; 
        }


        private static HtmlDocument DownloadHtml(string asin)
        {
            string urlTemplate = "http://www.amazon.de/gp/offer-listing/{0}/ref=olp_page_1?ie=UTF8&condition=new";
            WebClient wc = new WebClient();

            string html = wc.DownloadString(string.Format(urlTemplate, asin));
            if (html.Contains("Zeichen unten ein"))
            {
                throw new Exception();
            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            return doc;
        }


        private static RepricerEntity FindPrices(List<string> blackList, string asin,  string cookieString)
        {
            HtmlDocument htmlDocument = DownloadHtml(cookieString, asin);
            var offers = ListOffers(htmlDocument);

            double price = FindCheapestOffer(blackList, offers);
            double priceFBA = FindCheapestOfferFba(blackList, offers);
            double priceAma = FindAma(offers);
            List<string> sellerList = GetListOfSellers(blackList, offers);

            DateTime now = DateTime.Now; 

            RepricerEntity repricerEntity = new RepricerEntity() { 
            price = price, 
            priceFBA = priceFBA, 
            lastRun = now,
            lastPriceDate = now, 
            sellerList = sellerList,
            priceAmazon = priceAma,
            };

            return repricerEntity;
        }

        private static double FindAma(HtmlNodeCollection offers)
        {
            foreach (var node in offers)
            {
                var seller = getSellerFromString(node.OuterHtml);
                if (seller == null)
                {
                    var priceD = GetPrice(node);
                    return priceD;
                }
            }
            return 0;
        }

        private static List<string> GetListOfSellers(List<string> blackList, HtmlNodeCollection offers)
        {
            return offers.Select(x => getSellerFromString(x.OuterHtml)).Where(x => x != null).Where(x => !blackList.Contains(x)).ToList();
        }

        private static double FindCheapestOfferFba(List<string> blackList, HtmlNodeCollection offers)
        {
            double price = 0.0;
            foreach (var node in offers)
            {
                var seller = getSellerFromString(node.OuterHtml);
                var priceD = GetPrice(node);

                if (!blackList.Contains(seller) && node.InnerText.Contains("Versand durch Amazon.de"))
                {
                    return priceD;
                }
            }
            return price;
        }

        private static double FindCheapestOffer(List<string> blackList, HtmlNodeCollection offers)
        {
            double price = 0.0; 
            foreach (var node in offers)
            {
                var seller = getSellerFromString(node.OuterHtml);
                double shippingPrice = 0;
                var priceD = GetPrice(node);
                try
                {
                    shippingPrice = getDoubleFromString(node.SelectSingleNode(".//p[@class='olpShippingInfo']").InnerText.Trim(), 0);
                }catch
                {                    
                }

                if (shippingPrice >= 29)
                {
                    shippingPrice = 0; //bald verfügbar
                }
                if (!blackList.Contains(seller) && !node.InnerText.Contains("Wochen") && !node.InnerText.Contains("Dieser Artikel ist ab") && !node.InnerText.Contains("bestellbar mit") && !node.InnerText.Contains("Artikel ist bald") && !node.InnerText.Contains("1 bis 2 Monaten."))
                {
                    return priceD + shippingPrice;
                }
            }
            return price;
        }

        private static double GetPrice(HtmlNode node)
        {
            try
            {
                var priceD = getDoubleFromString(node.SelectSingleNode("div/span[@class='a-size-large a-color-price olpOfferPrice a-text-bold']").InnerText.Trim().Replace("EUR", ""), 0);
                return priceD;
            }
            catch {
                return 0.0;
            }
        }

        private static HtmlNodeCollection ListOffers(HtmlDocument htmlDocument)
        {
            var nodes = htmlDocument.DocumentNode.SelectNodes("//div[@class='a-row a-spacing-mini olpOffer']");
            return nodes;
        }

        private static string getSellerFromString(string value)
        {
            Regex regex = new Regex(@"seller=\w*");
            var match = regex.Match(value);

            if (match.Success)
            {
                return match.Value.Replace("seller=", "");
            }
            return null; 
        }

        private static double getDoubleFromString(string value, double defaultValue)
        {
            Regex regex = new Regex(@"\d*[,\.]\d*");
            var match = regex.Match(value);

            if (match.Success && match.Value.Replace(",", ".") != ".")
            {
                return double.Parse(match.Value.Replace(",", "."), CultureInfo.InvariantCulture);
            }
            else
            {
                return defaultValue;
            }
        }


    }

}