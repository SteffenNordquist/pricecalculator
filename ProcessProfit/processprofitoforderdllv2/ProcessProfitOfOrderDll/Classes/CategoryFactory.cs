﻿using DN_Classes.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Classes
{
	public class CategoryFactory
	{
		public Dictionary<string, PlatformConditions> valuedCategory = new Dictionary<string, PlatformConditions>();

		public CategoryFactory()
		{
			valuedCategory.Add("book_display_on_website", new PlatformConditions(15, 0, 0.56));
			valuedCategory.Add("music_display_on_website", new PlatformConditions(15, 0, 0.56));
			valuedCategory.Add("jewelry_display_on_website", new PlatformConditions(20, 0, 0));
			valuedCategory.Add("dvd_display_on_website", new PlatformConditions(15, 0, 0.56));
			valuedCategory.Add("toy_display_on_website", new PlatformConditions(15, 0, 0));
			valuedCategory.Add("musical_instruments_display_on_website", new PlatformConditions(12, 0, 0));
			valuedCategory.Add("pc_display_on_website", new PlatformConditions(12, 0, 0));
			valuedCategory.Add("ce_display_on_website", new PlatformConditions(12, 0, 0));
			valuedCategory.Add("office_product_display_on_website", new PlatformConditions(15, 0, 0));
			valuedCategory.Add("kitchen_display_on_website", new PlatformConditions(15, 0, 0));

		}

		public PlatformConditions getPlatformByCategory(string category)
		{
			try
			{
				return valuedCategory.Where(x => x.Key == category).Select(y => y.Value).ToList().First();
			}
			catch 
			{
				return new PlatformConditions(15, 0, 0);
			}
		}
	}
}
