﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using MongoDB.Bson;
using PriceCalculation;
using ProcessProfitOfOrderDll.Interfaces;
using ProcessProfitOfOrderDll.Classes.Entity;
using WhereToBuy;
using DN_Classes.Products.Price;
using DN_Classes.Ebay.Category;

namespace ProcessProfitOfOrderDll.Classes
{
	public class SetCalculatedProfit : CalculatedProfitEntity
	{
		private readonly List<string> _excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();
		private readonly BsonDocument _plus = new BsonDocument();
		private readonly ProfitRequirement _profitRequirement = new ProfitRequirement();

		/// <summary>
		/// Calculation for amazon.
		/// </summary>
		/// <param name="item">Pre-requisite values to get profit</param>
		/// <param name="amazonOrder">Single item order</param>
		public SetCalculatedProfit(IPreRequisite item, AmazonOrderEntity amazonOrder)
		{
			var category = item.GetCategories(item.GetCategoryRequirement()).First();

			var conditions = item.SetConditionSets(profitConditions: item.GetProfitCondition(),
					contractConditions: item.GetContractConditions(),
					platformCondition: item.GetPlatformCondition(
						category, null)
					);

			Agentname = item.GetAgentName(amazonOrder.Order.AgentId);

			Ean = item.GetEan();
			SellerSku = amazonOrder.Order.SellerSKU.AsString; 

			SetPurchaseDate(purchaseDate: amazonOrder.Order.PurchaseDate);
			SupplierName = "not available";
			AmazonOrder = amazonOrder.Order;

			
			#region 

			try
			{	
				Console.WriteLine("Finding the cheapest: " + Ean);
				// Get cheapest product from all our databases
				var cheapestItem = item.GetCheapestPrice(Ean, amazonOrder.Order.ASIN.AsString);
				
				// if it is one of the excluded supplier. It will not be calculated since we dont sell it at the first place
				if (_excludedSuppliersFromOrder.ConvertAll(x => x.ToUpper()).ToList().Contains(cheapestItem.suppliername.ToUpper()))
				{
					Console.WriteLine("item is not available.");
					_plus["excludeFromOrdering"] = true;
					throw new ItemNotAvailableException("Item " + Ean + " is not available");
				}

				Console.WriteLine("Item is found.");

				var itemSize = item.GetItemMeasurement(Ean, cheapestItem);
			   // Courier Cost
				var shippingCostVariations = item.GetShippingVariation(itemSize: itemSize, itemPrice: item.GetItemPrice(),
					isBook: string.Equals(cheapestItem.fixedPrice, "04", StringComparison.OrdinalIgnoreCase));

				// set product to get the product profit
				var product = new Product(ean: Ean,
					itemPrice: item.GetItemPrice(),
					conditionSet: conditions,
					cheapestItem: cheapestItem,
					itemSize: itemSize,
					shippingIncome: shippingCostVariations.price);
 
				var profit = product.
					getProfit(item.GetItemPrice(), item.GetShippingIncome(""));

				


				BsonDocument profitRequirement = _profitRequirement.GetProfitRequirementForAmazon(product, cheapestItem,
					shippingCostVariations, item.GetShippingIncome(""), item.GetItemPrice());
				_plus["ProfitRequirement"] = profitRequirement;
				plus = _plus;

				ArticleNumber = SellerSku.Contains("SWW")
					? cheapestItem.artnr.Split('_').Last()
					: cheapestItem.artnr;

				SupplierName = SetProperSupplierName(cheapestItem.suppliername);

				Platform = new SetPlatform(category, conditions.Platform, item.GetItemPrice());
				Income = new SetIncome(item.GetShippingIncome(""), cheapestItem, conditions.Platform, item.GetItemPrice());
				Item = new SetItem(cheapestItem, conditions.Profit, itemSize, profit);
				Tax = new SetTax(cheapestItem, item.GetItemPrice());
				Shipping = new SetShipping(shippingCostVariations.name, shippingCostVariations.price,
					conditions.Profit.ItemHandlingCosts, itemSize.packing.netPrice);
				Packing = new Packing(itemSize);
			}
			catch (Exception ex)
			{
				if (ex is NoProductFoundException || ex is ItemNotAvailableException)
				{
					SetNotAvailable(conditions, amazonOrder, null);
				}

				Console.WriteLine("item is not available.");
				//throw;
			}

			#endregion


		}

		/// <summary>
		/// Calculation for ebay
		/// </summary>
		/// <param name="item">Pre-requisite values to get profit</param>
		/// <param name="ebayOrder">Single item order</param>
		public SetCalculatedProfit(IPreRequisite item, Entity.EbayOrderEntity ebayOrder)
		{
			// get categories for feerule.
			var categories = item.GetCategories(item.GetCategoryRequirement());

			// get ebay fee percentage base from the given categories
			FeeRule feeRule = new EbayFeeFinder().GetPrimaryFeeRule(categories);
			
			var conditions = item.SetConditionSets(profitConditions: item.GetProfitCondition(),
					contractConditions: item.GetContractConditions(),
					platformCondition: item.GetPlatformCondition(
						"", feeRule)
					);

			Agentname = item.GetAgentName((ebayOrder.plus["AgentID"].AsInt32).ToString());

			Ean = item.GetEan();
			SellerSku = Ean;
			SupplierName = "not available";
			EbayOrder = new EbayOrderDetails(ebayOrder.order);

			try
			{
				// Get cheapest product from all our databases
				var cheapestItem = item.GetCheapestPrice(Ean, "");
				
				 // if it is one of the excluded supplier. It will not be calculated since we dont sell it at the first place
				if (_excludedSuppliersFromOrder.ConvertAll(x => x.ToUpper()).ToList().Contains(cheapestItem.suppliername.ToUpper()))
				{
					_plus["excludeFromOrdering"] = true;
					throw new ItemNotAvailableException("Item " + Ean + " is not available");
				}


				var itemSize = item.GetItemMeasurement(Ean, cheapestItem);

				var shippingCostVariations = item.GetShippingVariation(itemSize: itemSize, itemPrice: item.GetItemPrice(),
					isBook: string.Equals(cheapestItem.fixedPrice, "04", StringComparison.OrdinalIgnoreCase));

				// set product to get the product profit
				var product = new Product(ean: Ean,
					itemPrice: item.GetItemPrice(),
					conditionSet: conditions,
					cheapestItem: cheapestItem,
					itemSize: itemSize,
					shippingIncome: shippingCostVariations.price);

				var profit = product.
					getProfit(item.GetItemPrice(), item.GetShippingIncome(Agentname));

				// get all values to double check profit.
				BsonDocument profitRequirement = _profitRequirement.GetProfitRequirementForEbay(product, feeRule, item.GetItemPrice(), shippingCostVariations, item.GetShippingIncome(Agentname), cheapestItem);
				_plus["ProfitRequirement"] = profitRequirement;
				plus = _plus;

				ArticleNumber = SellerSku.Contains("SWW")
					? cheapestItem.artnr.Split('_').Last()
					: cheapestItem.artnr;

				SupplierName = SetProperSupplierName(cheapestItem.suppliername);

				// set all remaining data
				Platform = new SetPlatform(categories.First(), conditions.Platform, item.GetItemPrice());
				Income = new SetIncome(item.GetShippingIncome(Agentname), cheapestItem, conditions.Platform, item.GetItemPrice());
				Item = new SetItem(cheapestItem, conditions.Profit, itemSize, profit);
				Tax = new SetTax(cheapestItem, item.GetItemPrice());
				Shipping = new SetShipping(shippingCostVariations.name, shippingCostVariations.price,
					conditions.Profit.ItemHandlingCosts, itemSize.packing.netPrice);
				Packing = new Packing(itemSize);
			}
			catch (Exception ex)
			{

				// set not available orders
				if (ex is NoProductFoundException || ex is ItemNotAvailableException)
				{
					SetNotAvailable(conditions, null, ebayOrder);
				}

				Console.WriteLine("Not Found.");
				//throw;
			}

		}

		//private BsonDocument getProfitRequirementForEbay(Product product, FeeRule feeRule, double itemPrice, PriceCalculation.Shipping.ShippingVariation shippingCostVariations, double shippingIncome,BuyPriceRow cheapestBuyPriceRow)
		//{
		//	BsonDocument profitRequirement = new BsonDocument();
		//	double feeRuleDiscount = 0;

		//	try
		//	{
		//		feeRuleDiscount = feeRule.Percentage;
		//	}
		//	catch
		//	{
		//		// ignored
		//	}
		//	var shippingCost = shippingCostVariations.price;
		//	var weightFee = PriceCalc.getWeightFee(product.weight, product.contractConditions.WeightFee);
		//	var handlingCost = product.profitConditions.ItemHandlingCosts;
		//	var packingCost = product.productSize.packing.price;
		//	//var sellPrice = itemPrice;
		//	// shippingIncome
		//	double buyPrice = cheapestBuyPriceRow.price;
		//	double taxRate = cheapestBuyPriceRow.tax;
		//	double riskCosts;
		//	var shippingCostFinder = new ShippingCostFinder(new SimpleSize(product.productSize.Length, product.productSize.Width, product.productSize.Height, product.productSize.Weight), product.price, product.priceType == PriceTypeCalc.fixPrice);

		//	if (shippingCostFinder.getCarrier() != null && shippingCostFinder.getCarrier().GetType().Name == "DhlPaket")
		//	{
		//		riskCosts = 0;
		//	}
		//	else
		//	{
		//		riskCosts = product.buyPrice * 2 * (product.profitConditions.RiskFactor / 100);
		//	}
		//	double paymentFee = product.platformConditions.DefaultPlatformFeeRate;


		//	profitRequirement["ShippingCost"] = Convert.ToDouble(Math.Round(shippingCost, 2)); 	//  Convert.ToDouble(Math.Round(, 2)); 
		//	profitRequirement["RiskCost"] = Convert.ToDouble(Math.Round(riskCosts, 2));
		//	profitRequirement["HandlingCost"] = Convert.ToDouble(Math.Round(handlingCost, 2));
		//	profitRequirement["WeightFee"] = Convert.ToDouble(Math.Round(weightFee, 2));
		//	profitRequirement["PackingCost"] = 	  Convert.ToDouble(Math.Round(packingCost, 2)); 
		//	profitRequirement["BuyPrice"] =  Convert.ToDouble(Math.Round(buyPrice, 2)); 
		//	profitRequirement["SellPrice"] =  Convert.ToDouble(Math.Round(itemPrice, 2)); 
		//	profitRequirement["ShippingIncome"] =  Convert.ToDouble(Math.Round(shippingIncome, 2));
		//	profitRequirement["EbayFeeRate"] = Convert.ToDouble(Math.Round(feeRuleDiscount, 2)); 
		//	profitRequirement["PaypalFee"] = Convert.ToDouble(Math.Round(paymentFee, 2));
		//	profitRequirement["TaxRate"] = Convert.ToDouble(Math.Round(taxRate, 2)); 
		//	// Convert.ToDouble(Math.Round(, 2)); 

		//	return profitRequirement;
		//}

		public void SetNotAvailable(ConditionSet conditionSet, AmazonOrderEntity amazonOrder, Entity.EbayOrderEntity ebayOrder)
		{
			Platform = new SetPlatform("not available", conditionSet.Platform, -3.33);
			var emptyBuyPriceRow = new BuyPriceRow(Ean);
			Income = new SetIncome(-3.33, emptyBuyPriceRow, conditionSet.Platform, -3.33);
			Item = new SetItem(emptyBuyPriceRow, conditionSet.Profit, new ProductSize(), -3.33);
			Tax = new SetTax(emptyBuyPriceRow, -3.33);

			Shipping = new SetShipping("not available", 0, 0, 0);

		}

		private string SetProperSupplierName(string buypriceSupplierName)
		{
			var suppliername = buypriceSupplierName;

			if (SellerSku.Split('-').First() == "JTL")
			{
				suppliername = "JTL";
			}

			if (SellerSku.Split('-').First() == "SWW")
			{
				suppliername = "SWW";
			}
			else
			{
				if (SellerSku.Contains('_'))
				{
					suppliername = "SWW";
				}
			}

			return suppliername;
		}

		private void SetPurchaseDate(string purchaseDate)
		{
			if (purchaseDate.Contains("."))
			{
				PurchasedDate = DateTime.Parse(purchaseDate, new CultureInfo("de-DE"));
			}
			else
			{
				//PurchasedDate = DateTime.ParseExact(purchaseDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);

				if (purchaseDate.Contains("AM") || purchaseDate.Contains("PM"))
				{
					PurchasedDate = DateTime.ParseExact(purchaseDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
				}
				else
				{
					PurchasedDate = DateTime.ParseExact(purchaseDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
				}
			}
		}


	}
}
