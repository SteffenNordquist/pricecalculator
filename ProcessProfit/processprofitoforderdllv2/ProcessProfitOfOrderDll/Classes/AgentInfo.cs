﻿using DN_Classes.Agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Classes
{
	public class AgentInfo
	{
		public Agent agent = new Agent();

		public AgentInfo(Agent agent)
		{
			this.agent = agent;
		}

		public AgentInfo(string agentID)
		{
			if (!string.IsNullOrWhiteSpace(agentID))
			{
				int agentId = 0;
				int.TryParse(agentID, out agentId);
				this.agent = new Agent(agentId);
			}
		}

		public string getAgentName()
		{
			return this.agent.agentEntity.agent.agentname;
		}
	}
}
