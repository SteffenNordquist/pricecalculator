﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Entities;

namespace ProcessProfitOfOrderDll.Classes
{
	public class SetShipping : ShippingEntity
	{
		public SetShipping(string shippingName, double shippingCost, double handlingCost, double packingCost)
        {
            ShippingName = shippingName;
            ShippingCosts = Math.Round(shippingCost, 2);
            HandlingCosts = Math.Round(handlingCost, 2);
            PackingCosts = Math.Round(packingCost, 2);
        }
	}
}
