﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DN_Classes.Amazon.Orders;
using ProcessProfitOfOrderDll.Classes.Entity;
using ProcessProfitOfOrderDll.Interfaces;

namespace ProcessProfitOfOrderDll.Classes.Splitter
{
	public class AmazonOrdersSplitter : ISplitter
	{
		public IEnumerable<T> SplitByItems<T>(IEnumerable<T> orders)
		{
			List<AmazonOrderEntity> orders2 = (List<AmazonOrderEntity>) orders;
			var singleOrder = orders2.Where(x => x.Order.ASIN.IsString).ToList();
			var multiorders = orders2.Where(x => x.Order.ASIN.IsBsonArray).ToList();

			//UnprocessOrderEntity temp = new UnprocessOrderEntity(multiorders);
			List<AmazonOrderEntity> splitToMultiple = (List<AmazonOrderEntity>)SplitMultiorders(multiorders);

			//List<AmazonOrderEntity> tempAmazon = (List<AmazonOrderEntity>)splitToMultiple.ToList();
			//tempAmazon.AddRange(singleOrder);

			splitToMultiple.AddRange(singleOrder);

			List<AmazonOrderEntity> splitByQty = (List<AmazonOrderEntity>)SplitQty(splitToMultiple);



			return (IEnumerable<T>)splitByQty;
		}

		public IEnumerable<T> SplitQty<T>(IEnumerable<T> orders)
		{
			List<AmazonOrderEntity> extractAmazonOrderFromBothEntities = (List<AmazonOrderEntity>)orders;

			List<AmazonOrderEntity> totalOrders = new List<AmazonOrderEntity>();
			foreach (var order in extractAmazonOrderFromBothEntities)
			{
				try
				{
					int quantity = int.Parse(order.Order.QuantityOrdered.AsString);
					try
					{
					}
					catch
					{
						//var convertPrice = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(order.Order.ItemPrice.AsString);


					}
					var newItemPrice = double.Parse(order.Order.ItemPrice["Amount"].ToString(), CultureInfo.InvariantCulture) / quantity;

					for (int i = 0; i < quantity; i++)
					{
						var orderClone = (AmazonOrder)order.Order.Clone();
						orderClone.QuantityOrdered = 1;
						orderClone.ItemPrice = newItemPrice;

						totalOrders.Add(new AmazonOrderEntity(orderClone));
					}
				}
				catch
				{
					// ignored
				}
			}


			//UnprocessOrderEntity passed = new UnprocessOrderEntity(totalOrders);

			return (IEnumerable<T>)totalOrders;
		}

		public IEnumerable<T> SplitMultiorders<T>(IEnumerable<T> orders)
		{
			List<AmazonOrderEntity> extractAmazonOrderFromBothEntities = (List<AmazonOrderEntity>)orders;

			List<AmazonOrderEntity> temp = new List<AmazonOrderEntity>();


			foreach (var multiOrder in extractAmazonOrderFromBothEntities)
			{
				int count = multiOrder.Order.ASIN.AsBsonArray.Count();

				for (int i = 0; i < count; i++)
				{
					try
					{
						AmazonOrder amazonOrderTemplate = (AmazonOrder)multiOrder.Order.Clone();

						#region

						amazonOrderTemplate.ASIN = multiOrder.Order.ASIN.AsBsonArray[i];
						amazonOrderTemplate.SellerSKU = multiOrder.Order.SellerSKU.AsBsonArray[i];
						amazonOrderTemplate.OrderItemId = multiOrder.Order.OrderItemId.AsBsonArray[i];
						amazonOrderTemplate.Title = multiOrder.Order.Title.AsBsonArray[i];
						amazonOrderTemplate.QuantityOrdered = multiOrder.Order.QuantityOrdered.AsBsonArray[i];
						amazonOrderTemplate.QuantityShipped = multiOrder.Order.QuantityShipped.AsBsonArray[i];
						try
						{
							if (multiOrder.Order.ItemPrice.AsBsonValue[i].IsBsonDocument)
							{
								amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonValue[i];
							}
							else
							{
								amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonDocument;
							}
						}
						catch
						{
							amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonValue[0];
						}
						amazonOrderTemplate.ShippingPrice = multiOrder.Order.ShippingPrice.AsBsonValue[i];
						amazonOrderTemplate.GiftWrapPrice = multiOrder.Order.GiftWrapPrice.AsBsonValue[i];
						amazonOrderTemplate.ItemTax = multiOrder.Order.ItemTax.AsBsonValue[i];
						amazonOrderTemplate.ShippingTax = multiOrder.Order.ShippingTax.AsBsonValue[i];
						amazonOrderTemplate.GiftWrapTax = multiOrder.Order.GiftWrapTax.AsBsonValue[i];
						amazonOrderTemplate.ShippingDiscount = multiOrder.Order.ShippingDiscount.AsBsonValue[i];
						amazonOrderTemplate.PromotionDiscount = multiOrder.Order.PromotionDiscount.AsBsonValue[i];
						try
						{
							amazonOrderTemplate.ConditionNote = multiOrder.Order.ConditionNote.AsBsonValue[i];
						}
						catch
						{
							// ignored
						}

						amazonOrderTemplate.ConditionId = multiOrder.Order.ConditionId.AsBsonValue[i];
						amazonOrderTemplate.ConditionSubtypeId = multiOrder.Order.ConditionSubtypeId.AsBsonValue[i];

						#endregion

						temp.Add(new AmazonOrderEntity(amazonOrderTemplate));
					}
					catch
					{
						// ignored
					}
				}
			}

			return (IEnumerable<T>) temp;
		}
	}
}
