﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eBay.Service.Core.Soap;
using Newtonsoft.Json;
using ProcessProfitOfOrderDll.Classes.Entity;
using ProcessProfitOfOrderDll.Interfaces;
using ProcessProfitOfOrderDll.MarketPlatforms.Ebay;

namespace ProcessProfitOfOrderDll.Classes.Splitter
{
	public class EbayOrderSplitter : ISplitter
	{

		public IEnumerable<T> SplitByItems<T>(IEnumerable<T> orders)
		{
			List<EbayOrderEntity> orders2 = (List<EbayOrderEntity>)orders;
			var singleOrder = orders2.Where(x => x.order.TransactionArray.Count == 1).ToList();
			var multiOrder = orders2.Where(x => x.order.TransactionArray.Count > 1).ToList();

			List<EbayOrderEntity> splitToMultiple = (List<EbayOrderEntity>)SplitMultiorders(multiOrder);

			splitToMultiple.AddRange(singleOrder);
			List<EbayOrderEntity> splitByQty = (List<EbayOrderEntity>)SplitQty(splitToMultiple);

			//UnprocessOrderEntity temp = new UnprocessOrderEntity(multiOrder);
			//UnprocessOrderEntity splitToMultiple = SplitMultiorders(temp);

			//var tempEbay = splitToMultiple.Ebay.ToList();
			//tempEbay.AddRange(singleOrder);

			//splitToMultiple = new UnprocessOrderEntity(tempEbay);

			//var splitByQty = SplitQty((IEnumerable<T>)splitToMultiple);

			return (IEnumerable<T>)splitByQty;
		}

		public IEnumerable<T> SplitQty<T>(IEnumerable<T> orders)
		{
			List<EbayOrderEntity> totalOrders = new List<EbayOrderEntity>();

			List<EbayOrderEntity> orders2 = (List<EbayOrderEntity>) orders;

			foreach (var item in orders2)
			{
				try
				{
					EbayFieldSpecialCases ebayFields = new EbayFieldSpecialCases(item);
					int quantity = ebayFields.getQuantity();


					for (int i = 0; i < quantity; i++)
					{

						var orderClone = (EbayOrderEntity)item.Clone();
						int countItems = orderClone.order.TransactionArray.Count + quantity;

						foreach (dynamic transaction in orderClone.order.TransactionArray)
						{

							transaction.QuantityPurchased = 1;
						}

						totalOrders.Add(orderClone);
					}
				}
				catch { }
			}


			return (IEnumerable<T>)totalOrders;
		}

		public IEnumerable<T> SplitMultiorders<T>(IEnumerable<T> orders)
		{
			List<EbayOrderEntity> totalOrdersTemp = new List<EbayOrderEntity>();
			List<EbayOrderEntity> orders2 = (List<EbayOrderEntity>)orders;
			foreach (EbayOrderEntity multiOrder in orders2)
			{
				//var trans = multiOrder.order.TransactionArray.Cast<TransactionType>().ToList();
				List<TransactionType> trans = new List<TransactionType>();
				int countTrans = multiOrder.order.TransactionArray.Count;
				foreach (dynamic transaction in multiOrder.order.TransactionArray)
				{											   //EbayOrderEntity
					TransactionTypeCollection orderTrans = multiOrder.order.TransactionArray;

					string output = JsonConvert.SerializeObject(transaction);

					TransactionType deserializedProduct = JsonConvert.DeserializeObject<TransactionType>(output);

					trans.Add(deserializedProduct);
				}

				multiOrder.order.TransactionArray.Clear();

				foreach (TransactionType tran in trans)
				{
					multiOrder.order.TransactionArray.Clear();


					dynamic dymultiorder = new ExpandoObject();
					dymultiorder = multiOrder.order;

					string multijson = JsonConvert.SerializeObject(dymultiorder);

					OrderType deserializedOrder = JsonConvert.DeserializeObject<OrderType>(multijson);

					deserializedOrder.TransactionArray.Add(tran);


					EbayOrderEntity newOrder = new EbayOrderEntity { order = deserializedOrder, plus = multiOrder.plus };
					totalOrdersTemp.Add(newOrder);
				}

			}


			return (IEnumerable<T>)totalOrdersTemp;
		}
	}
}
