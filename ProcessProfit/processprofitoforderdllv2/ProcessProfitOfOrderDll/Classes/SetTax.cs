﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Entities;
using DN_Classes.Products.Price;

namespace ProcessProfitOfOrderDll.Classes
{
	public class SetTax: TaxEntity
    {

		public SetTax(BuyPriceRow buyPriceRow, double sellPrice)
        {

            ItemTax = sellPrice - sellPrice / ((100 + buyPriceRow.tax) / 100);
            ShippingIncomeTax = 3 - 3 / ((100 + buyPriceRow.tax) / 100);

            TaxRate = buyPriceRow.tax;
        }
    }
}
