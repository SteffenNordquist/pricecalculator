﻿using System;
using DN_Classes.Ebay.Category;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using MongoDB.Bson;
using PriceCalculation;
using PriceCalculation.Shipping;

namespace ProcessProfitOfOrderDll.Classes
{
	public class ProfitRequirement
	{
		//private BsonDocument _profitRequirement = new BsonDocument();

		public BsonDocument GetProfitRequirementForEbay(Product product, FeeRule feeRule, double itemPrice, PriceCalculation.Shipping.ShippingVariation shippingCostVariations, double shippingIncome, BuyPriceRow cheapestBuyPriceRow)
		{
			BsonDocument profitRequirement = new BsonDocument();
			double feeRuleDiscount = 0;

			try
			{
				feeRuleDiscount = feeRule.Percentage;
			}
			catch
			{
				// ignored
			}
			var shippingCost = shippingCostVariations.price;
			var weightFee = PriceCalc.getWeightFee(product.weight, product.contractConditions.WeightFee);
			var handlingCost = product.profitConditions.ItemHandlingCosts;
			var packingCost = product.productSize.packing.price;
			//var sellPrice = itemPrice;
			// shippingIncome
			double buyPrice = cheapestBuyPriceRow.price;
			double taxRate = cheapestBuyPriceRow.tax;
			double riskCosts;
			var shippingCostFinder = new ShippingCostFinder(new SimpleSize(product.productSize.Length, product.productSize.Width, product.productSize.Height, product.productSize.Weight), product.price, product.priceType == PriceTypeCalc.fixPrice);

			if (shippingCostFinder.getCarrier() != null && shippingCostFinder.getCarrier().GetType().Name == "DhlPaket")
			{
				riskCosts = 0;
			}
			else
			{
				riskCosts = product.buyPrice * 2 * (product.profitConditions.RiskFactor / 100);
			}
			double paymentFee = product.platformConditions.DefaultPlatformFeeRate;


			profitRequirement["ShippingCost"] = Convert.ToDouble(Math.Round(shippingCost, 2)); 	//  Convert.ToDouble(Math.Round(, 2)); 
			profitRequirement["RiskCost"] = Convert.ToDouble(Math.Round(riskCosts, 2));
			profitRequirement["HandlingCost"] = Convert.ToDouble(Math.Round(handlingCost, 2));
			profitRequirement["WeightFee"] = Convert.ToDouble(Math.Round(weightFee, 2));
			profitRequirement["PackingCost"] = Convert.ToDouble(Math.Round(packingCost, 2));
			profitRequirement["BuyPrice"] = Convert.ToDouble(Math.Round(buyPrice, 2));
			profitRequirement["SellPrice"] = Convert.ToDouble(Math.Round(itemPrice, 2));
			profitRequirement["ShippingIncome"] = Convert.ToDouble(Math.Round(shippingIncome, 2));
			profitRequirement["EbayFeeRate"] = Convert.ToDouble(Math.Round(feeRuleDiscount, 2));
			profitRequirement["PaypalFee"] = Convert.ToDouble(Math.Round(paymentFee, 2));
			profitRequirement["TaxRate"] = Convert.ToDouble(Math.Round(taxRate, 2));
			// Convert.ToDouble(Math.Round(, 2)); 

			return profitRequirement;
		}

		public BsonDocument GetProfitRequirementForAmazon(Product product, BuyPriceRow cheapestBuyPriceRow, PriceCalculation.Shipping.ShippingVariation shippingCostVariations,double shippingIncome, double itemPrice)
		{
			BsonDocument profitRequirement = new BsonDocument();

			double amazonFee = product.platformConditions.DefaultPlatformFeeRate;
			double softwareFee = 0;
			double taxRate = cheapestBuyPriceRow.tax;
			double discount = 0;
			double shippingCost = shippingCostVariations.price;
			double packingCost = product.productSize.packing.price;
			double fixCost = shippingCost + packingCost + 0.3;
			double buyPrice = cheapestBuyPriceRow.price;

			profitRequirement["AmazonFee"] = Convert.ToDouble(Math.Round(amazonFee, 2));
			profitRequirement["SoftwareFee"] = Convert.ToDouble(Math.Round(softwareFee, 2));
			profitRequirement["TaxRate"] = Convert.ToDouble(Math.Round(taxRate, 2));
			profitRequirement["Discount"] = Convert.ToDouble(Math.Round(discount, 2));
			profitRequirement["ShippingCost"] = Convert.ToDouble(Math.Round(shippingCost, 2));
			profitRequirement["PackingCost"] = Convert.ToDouble(Math.Round(packingCost, 2));
			profitRequirement["FixCost"] = Convert.ToDouble(Math.Round(fixCost, 2));
			profitRequirement["BuyPrice"] = Convert.ToDouble(Math.Round(buyPrice, 2));
			profitRequirement["ShippingIncome"] = Convert.ToDouble(Math.Round(shippingIncome, 2));
			profitRequirement["Sellprice"] = Convert.ToDouble(Math.Round(itemPrice, 2));
			//profitRequirement[""] = Convert.ToDouble(Math.Round(, 2));

			return profitRequirement;
		}
	}
}
