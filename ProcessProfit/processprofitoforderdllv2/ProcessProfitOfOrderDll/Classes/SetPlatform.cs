﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Conditions;
using DN_Classes.Entities;

namespace ProcessProfitOfOrderDll.Classes
{
	public class SetPlatform : PlatformEntity
    {
		public SetPlatform(string category, PlatformConditions platformConditions, double sellPrice)
        {
            Category = category;
            FeePercentage = Math.Round(platformConditions.DefaultPlatformFeeRate, 2);
            FeeinEuro = Math.Round(FeePercentage / 100 * (sellPrice + 3), 2);
            VariableFixcost = Math.Round(platformConditions.PaymentFeeFix, 2);
            PaymentFeeEuro = 0;
            PaymentFeePercentage = 0;
        }
    }
}
