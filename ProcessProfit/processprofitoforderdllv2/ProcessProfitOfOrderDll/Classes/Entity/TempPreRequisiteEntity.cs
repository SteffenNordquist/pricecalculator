﻿using System;
using System.Collections.Generic;
using System.Linq;
using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using DN_Classes.Products.Price;
using PriceCalculation;
using PriceCalculation.Shipping;
using ProcessProfitOfOrderDll.Interfaces;
using WhereToBuy;

namespace ProcessProfitOfOrderDll.Classes.Entity
{
	public class TempPreRequisiteEntity
	{
		public string Ean { get; set; }
		public double ItemPrice { get; set; }
		public string ItemOrderId { get; set; }
		public List<string> Categories { get; set; }
		public ProfitConditions ProfitConditions { get; set; }
		public ContractConditions ContractConditions { get; set; }
		public PlatformConditions PlatformConditions { get; set; }
		public ConditionSet ConditionSet { get; set; }
		public string AgentName { get; set; }
		public BuyPriceRow CheapestPriceRow { get; set; }
		public ProductSize ProductSize { get; set; }
		public ShippingVariation ShippingVariation { get; set; }		
		public double ShippingIncome { get; set; }
		public bool IsFixed { get; set; }
		public bool ExcludeFromOrdering { get; set; }
		public double Profit { get; set; }
		public AmazonOrderEntity AmazonOrder { get; set; }
		public EbayOrderEntity EbayOrder { get; set; }

		public TempPreRequisiteEntity(IPreRequisite item, AmazonOrderEntity amazonOrder)
		{
			var excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();

			this.Ean = item.GetEan();
			try
			{
				this.CheapestPriceRow = item.GetCheapestPrice(this.Ean, amazonOrder.Order.ASIN.AsString);

				if (excludedSuppliersFromOrder.ConvertAll(x => x.ToUpper()).ToList().Contains(CheapestPriceRow.suppliername.ToUpper()))
				{
					ExcludeFromOrdering = true;
					throw new ItemNotAvailableException("Item " + this.Ean + " is not available");
				}

				this.IsFixed = CheapestPriceRow.fixedPrice == "04" ? true : false;
				this.ItemPrice = item.GetItemPrice();
				this.Categories = item.GetCategories(item.GetCategoryRequirement());
				this.ItemOrderId = item.GetItemOrderId();
				this.ProfitConditions = item.GetProfitCondition();
				this.ContractConditions = item.GetContractConditions();
				this.PlatformConditions = item.GetPlatformCondition(Categories.First(), null);
				this.ConditionSet = item.SetConditionSets(ProfitConditions, ContractConditions, PlatformConditions);
				this.AgentName = item.GetAgentName(amazonOrder.Order.AgentId);
				this.ProductSize = item.GetItemMeasurement(Ean, CheapestPriceRow);
				this.ShippingVariation = item.GetShippingVariation(ProductSize, ItemPrice, IsFixed);
				this.ShippingIncome = item.GetShippingIncome("");
				this.Profit = new Product(this.Ean,
					this.ItemPrice,
					this.ConditionSet,
					this.CheapestPriceRow,
					this.ProductSize,
					this.ShippingIncome
					).getProfit(this.ItemPrice,this.ShippingIncome);
			}
			catch (Exception)
			{
				SetNotAvailable();
			}
			
			
			this.AmazonOrder = amazonOrder;
		}

		private void SetNotAvailable()
		{
			
		}

		public TempPreRequisiteEntity(IPreRequisite item, EbayOrderEntity ebayOrder)
		{
			var excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();

			this.Ean = item.GetEan();
			this.CheapestPriceRow = item.GetCheapestPrice(this.Ean, "");

			if (excludedSuppliersFromOrder.ConvertAll(x => x.ToUpper()).ToList().Contains(CheapestPriceRow.suppliername.ToUpper()))
			{
				this.IsFixed = CheapestPriceRow.fixedPrice == "04" ? true : false;
				this.ItemPrice = item.GetItemPrice();
				this.Categories = item.GetCategories(item.GetCategoryRequirement());
				FeeRule feeRule = new EbayFeeFinder().GetPrimaryFeeRule(this.Categories);
				this.ItemOrderId = item.GetItemOrderId();
				this.ProfitConditions = item.GetProfitCondition();
				this.ContractConditions = item.GetContractConditions();
				this.PlatformConditions = item.GetPlatformCondition(Categories.First(), feeRule);
				this.ConditionSet = item.SetConditionSets(ProfitConditions, ContractConditions, PlatformConditions);
				this.AgentName = item.GetAgentName((ebayOrder.plus["AgentID"].AsInt32).ToString());
				this.ProductSize = item.GetItemMeasurement(Ean, CheapestPriceRow);
				this.ShippingVariation = item.GetShippingVariation(ProductSize, ItemPrice, IsFixed);
				this.ShippingIncome = item.GetShippingIncome(this.AgentName);
			}

			this.EbayOrder = ebayOrder;
		}


	}
}
