﻿using DN_Classes.Amazon.Orders;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Classes.Entity
{
	[BsonIgnoreExtraElements]
	public class AmazonOrderEntity
	{
		public ObjectId id { get; set; }
		public AmazonOrder Order { get; set; }
		public BsonDocument plus { get; set; }

		public AmazonOrderEntity()
		{

		}

		public AmazonOrderEntity(AmazonOrder Order)
		{
			this.Order = Order;

		}
	}
}
