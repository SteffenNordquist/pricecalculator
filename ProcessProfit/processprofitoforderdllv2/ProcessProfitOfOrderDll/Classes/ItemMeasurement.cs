﻿using DN_Classes.Products.Price;
using MongoDB.Driver.Builders;
using PriceCalculation;
using ProcessProfitOfOrderDll.Interfaces;
using ProcuctDB.ProductSizes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Classes
{
	public class ItemMeasurement : IOrderItemMeasurements
	{
		ProductSizeDB productSizesDB = new ProductSizeDB();
		PackingList packingList = new PackingList();
		BuyPriceRow cheapestItem = new BuyPriceRow();
		string ean = "";
		bool fakeSizes = false;

		public ItemMeasurement(string ean, BuyPriceRow cheapestItem)
		{
			this.ean = ean;
			this.cheapestItem = cheapestItem;
		}

		public bool isfake()
		{
			return this.fakeSizes;
		}

		public ProductSize getItemSize()
		{
			int length;
			int width;
			int height;
			int weight;
			ProductSize productmeasurements;

			// default sizes
			length = 100;
			width = 100;
			height = 100;
			weight = 900;

			if (cheapestItem.simpleSize.isValid)
			{
				length = cheapestItem.simpleSize.Length;
				width = cheapestItem.simpleSize.Width;
				height = cheapestItem.simpleSize.Height;
				weight = cheapestItem.simpleSize.Weight;
			}
			else
			{
				var productsizesFromDb = productSizesDB.GetProductSizeByEan("ean");
				if (productsizesFromDb != null)
				{
					/// default if item has no measurements.
					if (productsizesFromDb.measure.length == 0 && productsizesFromDb.measure.width == 0 && productsizesFromDb.measure.height == 0 && productsizesFromDb.measure.weight == 0)
					{
						length = 100;
						width = 100;
						height = 100;
						weight = 900;
						this.fakeSizes = true;
					}
					else
					{
						length = productsizesFromDb.measure.length;
						width = productsizesFromDb.measure.width;
						height = productsizesFromDb.measure.height;
						weight = productsizesFromDb.measure.weight;
					}
				}
				else
				{
					this.fakeSizes = true;
				}

			}

			productmeasurements = new ProductSize(length.ToString(),
							   width.ToString(),
							  height.ToString(),
							  weight.ToString(),
									packingList);
			return productmeasurements;
		}
	}
}
