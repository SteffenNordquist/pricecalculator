﻿using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Classes
{
	public class ModifyPlatform
	{
		public string Category;
		public FeeRule feeRule = null;

		public ModifyPlatform(string category, FeeRule feeRule)
		{
			this.Category = category;
			this.feeRule = feeRule;
		}

		public bool isCategoryAvailable()
		{
			return Category != "";
		}

		public bool isFeeRuleAvailable()
		{
			return feeRule != null;
		}

		public PlatformConditions getPlatformCondition()
		{
			double paymentFeeRate = 0.017;
			double paymentFeeFix = 0.35;

			return isFeeRuleAvailable()
				? new PlatformConditions(feeRule.Percentage, paymentFeeRate, paymentFeeFix)
				: new CategoryFactory().getPlatformByCategory(Category);
		}
	}
}
