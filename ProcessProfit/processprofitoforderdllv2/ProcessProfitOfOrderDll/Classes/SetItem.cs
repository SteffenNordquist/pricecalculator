﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Products;
using DN_Classes.Products.Price;

namespace ProcessProfitOfOrderDll.Classes
{
	public class SetItem : ItemEntity
	{

		public SetItem(BuyPriceRow buyPriceRow, ProfitConditions profitConditions, SimpleSize simpleSize, double profit)
			{
				NewBuyPrice = Math.Round(buyPriceRow.price, 2);
				DiscountedBuyprice = Math.Round(buyPriceRow.price - buyPriceRow.weightFee, 2);
				Stock = buyPriceRow.stock;
				SetRiskCosts(buyPriceRow, profitConditions);
				Profit = Math.Round(profit, 2);
				SimpleSize = simpleSize;
			}

			private void SetRiskCosts(BuyPriceRow buyPriceRow, ProfitConditions profitConditions)
			{
				RiskCost = buyPriceRow.price < 25 ? Math.Round((buyPriceRow.price * 2) * profitConditions.RiskFactor / 100, 2) : 0;
			}
	}
}
