﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using ProcessProfitOfOrderDll.Classes;
using ProcessProfitOfOrderDll.Classes.Entity;
using ProcessProfitOfOrderDll.Classes.Splitter;
using ProcessProfitOfOrderDll.Interfaces;
using ProcessProfitOfOrderDll.MarketPlatforms.Amazon;

namespace ProcessProfitOfOrderDll.Configuration
{
	public class Config 
	{
		private readonly IUnityContainer _container = new UnityContainer();

		public void Initialer()
		{
			_container.RegisterType<IPreRequisite, AmazonEntity>();
			//_container.RegisterType<IPreRequisite, EbayOrderEntity>();
		}

		public T Resolve<T>()
		{
			return _container.Resolve<T>();
		}
	}
}
