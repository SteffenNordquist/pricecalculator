﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Interfaces
{
	public interface IMongoAction
	{
		/// <summary>
		/// Insert Calculated Item in database
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="items"></param>
		void Insert<T>(IEnumerable<T> items);

		/// <summary>
		/// Not available updater
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="items"></param>
		void Update<T>(IEnumerable<T> items);
	}
}
