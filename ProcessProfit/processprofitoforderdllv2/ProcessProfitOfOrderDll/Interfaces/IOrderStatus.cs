﻿using ProcessProfitOfOrderDll.Classes.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Interfaces
{
	public interface IOrderStatus : IMongoAction
	{	
		/// <summary>
		/// Not yet calculated orders
		/// </summary>
		/// <typeparam name="T"> Depending on market platform </typeparam>
		/// <returns></returns>
		IEnumerable<T> RawOrders<T>();

		/// <summary>
		/// 10 days before not available orders
		/// </summary>
		/// <typeparam name="T"> Depending on market platform</typeparam>
		/// <returns></returns>
		IEnumerable<T> UnavailableOrders<T>();

		/// <summary>
		/// Order API error. For the mean time, Ebay is not applicable
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="items"></param>
		/// <returns></returns>
		IEnumerable<T> ErrorOrders<T>(IEnumerable<T> items);
	}
}
