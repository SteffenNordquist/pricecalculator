﻿using PriceCalculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Interfaces
{
	public interface IOrderItemMeasurements
	{
		bool isfake();
		ProductSize getItemSize();
	}
}
