﻿using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using DN_Classes.Products.Price;
using PriceCalculation;
using PriceCalculation.Shipping;
using System.Collections.Generic;

namespace ProcessProfitOfOrderDll.Interfaces
{
	public interface IPreRequisite
	{
		/// <summary>
		///  Return ean
		/// </summary>
		/// <returns>string ean</returns>
		string GetEan();

		/// <summary>
		/// Fetch order each item price
		/// </summary>
		/// <returns></returns>
		double GetItemPrice();

		/// <summary>
		/// Item Order ID
		/// </summary>
		/// <returns></returns>
		string GetItemOrderId();

		/// <summary>
		/// List of categories.
		/// Amazon only need 1 category
		/// ebay need plenty of category depending on item (Category code)
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		List<string> GetCategories(string type);

		/// <summary>
		/// Get Profit condition. For the mean time it has default value for
		/// profitWanted, riskFactor, itemHandlingCost
		/// </summary>
		/// <returns></returns>
		ProfitConditions GetProfitCondition();

		/// <summary>
		/// Get contract conditions.
		/// It also has default value for
		/// weightFee,discount ,bonus
		/// </summary>
		/// <returns></returns>
		ContractConditions GetContractConditions();

		/// <summary>
		/// Platform condition depending on what category.
		/// for the mean time, ebay requires feerule and amazon requires category
		/// </summary>
		/// <param name="category"></param>
		/// <param name="feeRule"></param>
		/// <returns></returns>
		PlatformConditions GetPlatformCondition(string category,FeeRule feeRule);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="profitConditions"></param>
		/// <param name="contractConditions"></param>
		/// <param name="platformCondition"></param>
		/// <returns></returns>
		ConditionSet SetConditionSets(ProfitConditions profitConditions, ContractConditions contractConditions, PlatformConditions platformCondition);
		
		/// <summary>
		/// Get agent name by agent id
		/// </summary>
		/// <param name="agentId"></param>
		/// <returns></returns>
		string GetAgentName(string agentId);

		/// <summary>
		/// Get the cheapest item based on ean. 
		/// amazon requires asin and ebay is just ean.
		/// </summary>
		/// <param name="ean"></param>
		/// <param name="asin"></param>
		/// <returns></returns>
		BuyPriceRow GetCheapestPrice(string ean, string asin);

		/// <summary>
		/// For product size of the item from our database
		/// </summary>
		/// <param name="ean"></param>
		/// <param name="cheapestItem"></param>
		/// <returns></returns>
		ProductSize GetItemMeasurement(string ean, BuyPriceRow cheapestItem);

		/// <summary>
		/// Finds the courier by the given parameters 
		/// </summary>
		/// <param name="itemSize"></param>
		/// <param name="itemPrice"></param>
		/// <param name="isBook"></param>
		/// <returns></returns>
		ShippingVariation GetShippingVariation(ProductSize itemSize, double itemPrice, bool isBook);

		/// <summary>
		/// Get shipping income requries agent name for ebay
		/// but doesnt need in amazon
		/// </summary>
		/// <param name="agentName"></param>
		/// <returns></returns>
		double GetShippingIncome(string agentName);

		/// <summary>
		/// Returns the category requirement base on market platform. 
		/// This is used as a parameter for getting the category
		/// </summary>
		/// <returns></returns>
		string GetCategoryRequirement();


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		string GetAsin();

	}
}
