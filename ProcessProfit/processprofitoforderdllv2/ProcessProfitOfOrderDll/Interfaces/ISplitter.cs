﻿using ProcessProfitOfOrderDll.Classes.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Interfaces
{
	public interface ISplitter
	{
		/// <summary>
		/// Split Orders by quantity/stock or by items or by transaction/asin
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="orders"></param>
		/// <returns></returns>
		IEnumerable<T> SplitByItems<T>(IEnumerable<T> orders);

		/// <summary>
		/// Splits by qty or stocks
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="orders"></param>
		/// <returns></returns>
		IEnumerable<T> SplitQty<T>(IEnumerable<T> orders);

		/// <summary>
		/// Splits by items (asins or transaction array)
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="orders"></param>
		/// <returns></returns>
		IEnumerable<T> SplitMultiorders<T>(IEnumerable<T> orders);
	}
}
