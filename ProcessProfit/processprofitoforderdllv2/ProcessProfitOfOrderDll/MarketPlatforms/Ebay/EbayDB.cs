﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DN_Classes.Entities;
using Microsoft.Practices.ObjectBuilder2;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcessProfitOfOrderDll.Classes.Splitter;
using ProcessProfitOfOrderDll.Interfaces;
using ProcessProfitOfOrderDll.Properties;
using EbayOrderEntity = ProcessProfitOfOrderDll.Classes.Entity.EbayOrderEntity;

namespace ProcessProfitOfOrderDll.MarketPlatforms.Ebay
{
	public class EbayDB : IOrderStatus
	{
		public MongoCollection<EbayOrderEntity> Collection;
		public MongoCollection<CalculatedProfitEntity> CollectionFromCalculatedProfit;

		private readonly string _connectionString = Settings.Default.DBConnection;
		private readonly string _connectionStringFromCalculatedOrders = Settings.Default.DBConnectionCalculatedOrders;

		public EbayDB()
		{
			Collection = new MongoClient(_connectionString).GetServer()
				.GetDatabase("Orders")
				.GetCollection<EbayOrderEntity>("EbaySingleOrderItems");

			CollectionFromCalculatedProfit = new MongoClient(_connectionStringFromCalculatedOrders).GetServer()
				.GetDatabase("CalculatedProfit")
				.GetCollection<CalculatedProfitEntity>("EbaySingleCalculatedItemV2_copy");
		}

		public IEnumerable<T> RawOrders<T>()
		{
			var query = Query.NotExists("plus.OrderProcessed");

			//var query = Query.EQ("order.OrderID", "121904503747-1616576527002");
			var results = Collection.Find(query).ToList();

			return (IEnumerable<T>)results;
		}

		public IEnumerable<T> UnavailableOrders<T>()
		{
			var orderIdandEan = new Dictionary<string, string>();
			var rawOrders = new List<EbayOrderEntity>();
			var notAvailableOrders = new List<EbayOrderEntity>();

			if (CollectionFromCalculatedProfit != null)
				// 303-2694451-4561100	"SupplierName", "not available"	   304-4713809-6953903
				CollectionFromCalculatedProfit.Find(Query.EQ("SupplierName", "not available"))
					.SetFields("EbayOrder.OrderID", "Ean", "PurchasedDate").Where(order => order.PurchasedDate.Date > DateTime.Now.AddDays(-10).Date)
					.GroupBy(x => x.EbayOrder.OrderID).ForEach(a => orderIdandEan.Add(a.Key, a.First().Ean));

			var po = new ParallelOptions { MaxDegreeOfParallelism = 4 };

			Parallel.ForEach(orderIdandEan, po,
				each => { rawOrders.Add(Collection.FindOne(Query.EQ("order.OrderID", each.Key.ToString()))); }
				);

			var ebayOrderSplitter = new EbayOrderSplitter();

			if (rawOrders.Count <= 0) return (IEnumerable<T>) notAvailableOrders;
			notAvailableOrders = (from splittedItem in ebayOrderSplitter.SplitByItems(rawOrders).ToList()
				from eachOrder in orderIdandEan
				where
					eachOrder.Key == splittedItem.order.OrderID &&
					eachOrder.Value == new Regex(@"\d{13}").Match(new EbayFieldSpecialCases(splittedItem.order).getEan()).ToString()
				select splittedItem).ToList();

			return (IEnumerable<T>)notAvailableOrders;
		}

		public IEnumerable<T> ErrorOrders<T>(IEnumerable<T> items)
		{
			// Order Error is not yet applicable in ebay.
			return items;
		}

		public void Insert<T>(IEnumerable<T> items)
		{
			foreach (var item in (List<CalculatedProfitEntity>)items)
			{
				if (item.EbayOrder != null)
				{
					CollectionFromCalculatedProfit.Insert(item);

					//
					// Remove comment if the testing is done. This is very important
					//

					//Collection.Update(Query.EQ("order.OrderID", item.EbayOrder.OrderID),
					//	MongoDB.Driver.Builders.Update.Set("plus.OrderProcessed", DateTime.Now));


				}
			}
		}

		public void Update<T>(IEnumerable<T> items)
		{
			foreach (var item in (List<CalculatedProfitEntity>)items)
			{
				if (item.Item.Profit != -3.33 && item.Shipping.ShippingName != "not available")
				{
					CollectionFromCalculatedProfit.Update(Query.And(Query.EQ("EbayOrder.OrderID", item.EbayOrder.OrderID),
											 Query.EQ("Ean", item.Ean)),
									MongoDB.Driver.Builders.Update.Set("Agentname", item.Agentname)
										  .Set("SellerSku", item.SellerSku)
										  .Set("SupplierName", item.SupplierName)
										  .Set("Ean", item.Ean)
										  .Set("ArticleNumber", item.ArticleNumber)
										  .Set("VE", item.VE)
										  .Set("PurchasedDate", item.PurchasedDate)
										  .Set("Platform", item.Platform.ToBsonDocument())
										  .Set("Income", item.Income.ToBsonDocument())
										  .Set("Item", item.Item.ToBsonDocument())
										  .Set("Tax", item.Tax.ToBsonDocument())
										  .Set("Shipping", item.Shipping.ToBsonDocument())
										  .Set("Packing", item.Packing.ToBsonDocument())
										  .Set("EbayOrder", item.EbayOrder.ToBsonDocument())
										  , UpdateFlags.Multi);
				}	
			}
		}
	}
}
