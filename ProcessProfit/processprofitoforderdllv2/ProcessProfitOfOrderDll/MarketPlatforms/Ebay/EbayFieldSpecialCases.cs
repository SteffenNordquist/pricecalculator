﻿using System.Dynamic;
using eBay.Service.Core.Soap;
using MongoDB.Bson;
using ProcessProfitOfOrderDll.Classes.Entity;

namespace ProcessProfitOfOrderDll.MarketPlatforms.Ebay
{
	public class EbayFieldSpecialCases
	{
		private EbayOrderEntity Ebay = new EbayOrderEntity();

		public EbayFieldSpecialCases(EbayOrderEntity Ebay)
		{
			this.Ebay = Ebay;
		}

		public EbayFieldSpecialCases(OrderType EbayOrderType)
		{
			Ebay.order = EbayOrderType;
			Ebay.plus = new BsonDocument();
		}

		public EbayFieldSpecialCases()
		{
			// TODO: Complete member initialization
		}

		internal int getQuantity()
		{
			TransactionTypeCollection orderTrans = Ebay.order.TransactionArray;

			int quantity = 0;


			foreach (var item in orderTrans)
			{
				dynamic getTransactionTemp = new ExpandoObject();
				getTransactionTemp = item;
				quantity = getTransactionTemp.QuantityPurchased;

				break;
			}
			return quantity;
		}

		internal string getEan()
		{
			TransactionTypeCollection orderTrans = Ebay.order.TransactionArray;

			string strSKU = "";

			foreach (var transaction in orderTrans)
			{
				dynamic dyTransaction = new ExpandoObject();
				dyTransaction = transaction;

				dynamic dyItem = new ExpandoObject();
				dyItem = dyTransaction.Item;

				strSKU = dyItem.SKU;
			}

			return strSKU;
		}

		internal double getItemPrice()
		{
			double price = 0;

			TransactionTypeCollection orderTrans = Ebay.order.TransactionArray;

			foreach (dynamic item in orderTrans)
			{
				price = item.TransactionPrice.Value;
				break;
			}

			return price;
		}

		internal string getItemOrderId()
		{
			string orderId = "";

			TransactionTypeCollection orderTrans = Ebay.order.TransactionArray;

			foreach (dynamic item in orderTrans)
			{
				orderId = item.OrderLineItemID;
				break;
			}

			return orderId;
		}
	}
}
