﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using DN_Classes.Products.Price;
using HtmlAgilityPack;
using PriceCalculation;
using PriceCalculation.Shipping;
using ProcessProfitOfOrderDll.Classes;
using ProcessProfitOfOrderDll.Classes.Entity;
using ProcessProfitOfOrderDll.Interfaces;
using WhereToBuy;

namespace ProcessProfitOfOrderDll.MarketPlatforms.Ebay
{
	public class EbayEntity : EbayOrderEntity , IPreRequisite
	{
		EbayFieldSpecialCases ebayFields = new EbayFieldSpecialCases();

		public EbayEntity()
		{

		}
		public EbayEntity(EbayOrderEntity order)
		{
			this.order = order.order;
			plus = order.plus;
			ebayFields = new EbayFieldSpecialCases(this.order);
		}

		

		public string GetEan()
		{
			string ean = "";

			ean = new Regex(@"\d{13}").Match(ebayFields.getEan()).ToString();

			return ean;
		}

		public double GetItemPrice()
		{
			double itemPrice = 0;

			itemPrice = ebayFields.getItemPrice();

			return itemPrice;
		}

		public List<string> GetCategories(string type)
		{
			List<string> categories = new List<string>();
			string split = type.Split('-')[0];

			WebClient wc = new WebClient();
			try
			{
				string html = wc.DownloadString(@"http://www.ebay.de/itm/" + split);
				HtmlDocument doc = new HtmlDocument();
				doc.LoadHtml(html);

				string nodeFormat = ".//td[@class='vi-VR-brumblnkLst vi-VR-brumb-hasNoPrdlnks']/table/tbody/tr/td/h2/ul";
				var li = doc.DocumentNode.SelectSingleNode(@"//li[@class='bc-w']");


				foreach (var singleLI in doc.DocumentNode.SelectNodes(@"//li[@class='bc-w']"))
				{
					categories.Add(singleLI.SelectSingleNode(singleLI.XPath + "//a").Attributes["href"].Value.Split('/')[5]);
				}

			}
			catch
			{
				categories.Add("no category");
			}

			return categories;
		}

		public ProfitConditions GetProfitCondition()
		{
			double profitWanted = 0;
			double riskFactor = 1;
			double itemHandlingCost = 0.3;

			ProfitConditions defaultProfitConditions = new ProfitConditions(profitWanted, riskFactor, itemHandlingCost);

			return defaultProfitConditions;
		}

		public ContractConditions GetContractConditions()
		{
			double weightFee = 0;
			double discount = 0;
			double bonus = 0;
			ContractConditions contractconditions = new ContractConditions(weightFee, discount, bonus);

			return contractconditions;
		}

		public ConditionSet SetConditionSets(ProfitConditions profitConditions, ContractConditions contractConditions, PlatformConditions platformCondition)
		{
			return new ConditionSet(profitConditions, contractConditions, platformCondition);
		}

		public string GetItemOrderId()
		{
			string itemOrderId = "";

			itemOrderId = ebayFields.getItemOrderId();

			return itemOrderId;
		}

		public PlatformConditions GetPlatformCondition(string category, FeeRule feeRule)
		{
			if (feeRule == null)
			{
				category = "";
			}
			return new ModifyPlatform(category, feeRule).getPlatformCondition();
		}


		public string GetAgentName(string agentId)
		{
			return new AgentInfo(agentId).getAgentName();
		}


		public BuyPriceRow GetCheapestPrice(string ean, string asin)
		{
			var cheapestItem = new CheapestPrice().GetCheapestPriceByEan(ean, "");

			return cheapestItem;
		}


		public ProductSize GetItemMeasurement(string ean, BuyPriceRow cheapestItem)
		{
			ItemMeasurement itemMeasurement = new ItemMeasurement(ean, cheapestItem);
			var itemSize = itemMeasurement.getItemSize();
			itemSize.isFakeSize = itemMeasurement.isfake();

			return itemSize;
		}

		public ShippingVariation GetShippingVariation(ProductSize itemSize, double itemPrice, bool isBook)
		{
			var shippingCostVariation = new ShippingCostFinder(itemSize,
																				itemPrice,
																				isBook)
																				.getShippingVariation();

			return shippingCostVariation;
		}

		public double GetShippingIncome(string agentName)
		{
			double shippingIncome = string.Equals(agentName, "Albert Minin", StringComparison.CurrentCultureIgnoreCase) ? 2.9 : 0; ;	
			
			return shippingIncome;
		}


		public string GetCategoryRequirement()
		{
			return GetItemOrderId();
		}


		public string GetAsin()
		{
			return "";
		}
	}
}
