﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DN_Classes.Entities;
using Microsoft.Practices.ObjectBuilder2;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcessProfitOfOrderDll.Classes.Entity;
using ProcessProfitOfOrderDll.Classes.Splitter;
using ProcessProfitOfOrderDll.Interfaces;
using ProcessProfitOfOrderDll.Properties;

namespace ProcessProfitOfOrderDll.MarketPlatforms.Amazon
{
	public class AmazonDB : IOrderStatus
	{																									    
		public MongoCollection<AmazonOrderEntity> CollectionFromSingleOrder;
		public MongoCollection<CalculatedProfitEntity> CollectionFromCalculatedProfit;

		private readonly string _connectionStringFromSingleOrder = Settings.Default.DBConnection;
		private readonly string _connectionStringFromCalculatedOrders = Settings.Default.DBConnectionCalculatedOrders;

		public AmazonDB()
		{
			CollectionFromSingleOrder = new MongoClient(_connectionStringFromSingleOrder).GetServer()
				.GetDatabase("Orders")
				.GetCollection<AmazonOrderEntity>("SingleOrderItems");

			CollectionFromCalculatedProfit = new MongoClient(_connectionStringFromCalculatedOrders).GetServer()
				.GetDatabase("CalculatedProfit")
				.GetCollection<CalculatedProfitEntity>("SingleCalculatedItem_charley");
		}



		public IEnumerable<T> RawOrders<T>()
		{
			IMongoQuery query = Query.And(
												Query.NE("Order.OrderStatus", "Pending"),
												 Query.NE("Order.OrderStatus", "Canceled"),
												Query.NE("Order.FulfillmentChannel", "AFN"),
												 Query.NotExists("plus.OrderProcessed")
												);

			//IMongoQuery query = Query.EQ("Order.AmazonOrderId", "305-9214212-1811564 sample no value");
			var documents = CollectionFromSingleOrder.Find(query).ToList();

			return (IEnumerable<T>)documents;
		}

		public IEnumerable<T> UnavailableOrders<T>()
		{
			var orderIdandEan = new Dictionary<string, string>();
			var rawOrders = new List<AmazonOrderEntity>();
			var notAvailableOrders = new List<AmazonOrderEntity>();

			if (CollectionFromCalculatedProfit != null)
				// 303-2694451-4561100	"SupplierName", "not available"	   304-4713809-6953903
				CollectionFromCalculatedProfit.Find(Query.EQ("SupplierName", "not available"))
					.SetFields("AmazonOrder.AmazonOrderId", "Ean", "PurchasedDate").Where(order => order.PurchasedDate.Date > DateTime.Now.AddDays(-10).Date)
					.GroupBy(x => x.AmazonOrder.AmazonOrderId).ForEach(a => orderIdandEan.Add(a.Key, a.First().Ean));

			var po = new ParallelOptions {MaxDegreeOfParallelism = 4};

			Parallel.ForEach(orderIdandEan, po,
				each => { rawOrders.Add(CollectionFromSingleOrder.FindOne(Query.EQ("Order.AmazonOrderId", each.Key.ToString()))); }
				);


			var amazonSplitter = new AmazonOrdersSplitter();

			if (rawOrders.Count <= 0) return (IEnumerable<T>) notAvailableOrders;
			notAvailableOrders = (from splittedItem in amazonSplitter.SplitByItems(rawOrders).ToList()
				from eachOrder in orderIdandEan
				where
					eachOrder.Key == splittedItem.Order.AmazonOrderId &&
					eachOrder.Value == new Regex(@"\d{13}").Match(splittedItem.Order.SellerSKU.AsString).ToString()
				select splittedItem).ToList();


			return (IEnumerable<T>)notAvailableOrders;
		}

		public IEnumerable<T> ErrorOrders<T>(IEnumerable<T> items)
		{
			var docs = ((List<AmazonOrderEntity>)items).Where(x => x.Order.ASIN.IsBsonArray && !x.Order.ItemPrice.IsBsonArray).ToList();

			return (IEnumerable<T>)docs;
		}

		public void Insert<T>(IEnumerable<T> items)
		{
			foreach (var item in (List<CalculatedProfitEntity>)items)
			{
				if (item.AmazonOrder != null )		  
				{
					CollectionFromCalculatedProfit.Insert(item);

					//
					// Remove comment if the testing is done. This is very important
					//

					//CollectionFromSingleOrder.Update(Query.EQ("Order.AmazonOrderId", item.AmazonOrder.AmazonOrderId),
					//	MongoDB.Driver.Builders.Update.Set("plus.OrderProcessed", DateTime.Now));


				}
			}
			

		}

		public void Update<T>(IEnumerable<T> items)
		{
			foreach (var item in (List<CalculatedProfitEntity>)items)
			{
				if (item.Item.Profit != -3.33 && item.Shipping.ShippingName != "not available")
				{
					CollectionFromCalculatedProfit.Update(Query.And(Query.EQ("AmazonOrder.AmazonOrderId", item.AmazonOrder.AmazonOrderId),
											 Query.EQ("Ean", item.Ean)),
									MongoDB.Driver.Builders.Update.Set("Agentname", item.Agentname)
										  .Set("SellerSku", item.SellerSku)
										  .Set("SupplierName", item.SupplierName)
										  .Set("Ean", item.Ean)
										  .Set("ArticleNumber", item.ArticleNumber)
										  .Set("VE", item.VE)
										  .Set("PurchasedDate", item.PurchasedDate)
										  .Set("Platform", item.Platform.ToBsonDocument())
										  .Set("Income", item.Income.ToBsonDocument())
										  .Set("Item", item.Item.ToBsonDocument())
										  .Set("Tax", item.Tax.ToBsonDocument())
										  .Set("Shipping", item.Shipping.ToBsonDocument())
										  .Set("Packing", item.Packing.ToBsonDocument())
										  .Set("AmazonOrder", item.AmazonOrder.ToBsonDocument())
										  , UpdateFlags.Multi);
				}
			}	
		}
	}
}
