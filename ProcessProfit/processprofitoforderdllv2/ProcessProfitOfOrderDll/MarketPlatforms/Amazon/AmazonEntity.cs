﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using PriceCalculation;
using PriceCalculation.Shipping;
using ProcessProfitOfOrderDll.Classes;
using ProcessProfitOfOrderDll.Classes.Entity;
using ProcessProfitOfOrderDll.Interfaces;
using WhereToBuy;


namespace ProcessProfitOfOrderDll.MarketPlatforms.Amazon
{
	public class AmazonEntity : AmazonOrderEntity, IPreRequisite
	{
		public AmazonEntity(AmazonOrderEntity order)
		{

			Order = order.Order;
			plus = order.plus;

		}

		

		public string GetEan()
		{
			return new Regex(@"\d{13}").Match(Order.SellerSKU.AsString).ToString();
		}

		public double GetItemPrice()
		{
			var itemPrice = Order.GetItemPrice();

			return itemPrice;
		}

		public List<string> GetCategories(string type)
		{	
			CategoriesDB categoriesDb = new CategoriesDB();

			string categoryFromDatabase = categoriesDb.getAmazonCategory(type);

			List<string> categories = new List<string> {categoryFromDatabase};


			return categories;
		}

		public ProfitConditions GetProfitCondition()
		{
			double profitWanted = 0;
			double riskFactor = 1;
			double itemHandlingCost = 0.3;

			ProfitConditions defaultProfitConditions = new ProfitConditions(profitWanted, riskFactor, itemHandlingCost);

			return defaultProfitConditions;
		}

		public ContractConditions GetContractConditions()
		{
			double weightFee = 0;
			double discount = 0;
			double bonus = 0;
			ContractConditions contractconditions = new ContractConditions(weightFee, discount, bonus);

			return contractconditions;
		}

		public PlatformConditions GetPlatformCondition(string category, FeeRule feeRule)
		{

			return new ModifyPlatform(category, feeRule).getPlatformCondition();
		}

		public ConditionSet SetConditionSets(ProfitConditions profitConditions, ContractConditions contractConditions, PlatformConditions platformCondition)
		{
			return new ConditionSet(profitConditions, contractConditions, platformCondition);
		}


		public string GetItemOrderId()
		{
			string itemOrderId = "";

			return itemOrderId;
		}


		public string GetAgentName(string agentId)
		{
			return new AgentInfo(agentId).getAgentName();
		}


		public BuyPriceRow GetCheapestPrice(string ean, string asin)
		{
			var cheapestItem = new CheapestPrice().GetCheapestPriceByEan(ean, asin);

			return cheapestItem;
		}


		public ProductSize GetItemMeasurement(string ean, BuyPriceRow cheapestItem)
		{
			ItemMeasurement itemMeasurement = new ItemMeasurement(ean, cheapestItem);
			var itemSize = itemMeasurement.getItemSize();
			itemSize.isFakeSize = itemMeasurement.isfake();

			return itemSize;
		}

		public PriceCalculation.Shipping.ShippingVariation GetShippingVariation(ProductSize itemSize, double itemPrice, bool isBook)
		{
			var shippingCostVariation = new ShippingCostFinder(itemSize,
																				itemPrice,
																				isBook)
																				.getShippingVariation();

			return shippingCostVariation;
		}

		public double GetShippingIncome(string agentName)
		{
			// Default 3
			return 3;
		}


		public string GetCategoryRequirement()
		{
			return GetAsin();
		}


		public string GetAsin()
		{
			return Order.ASIN.AsString;
		}
	}
}
