﻿using DN_Classes.Entities;
using DN_Classes.Entities.ProcessProfitOrder;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessProfitOfOrderDll
{
	public class EbayCalculatedEntityV2
	{
		public ObjectId _id { get; set; }
		public string Agentname { get; set; }
		public string SellerSku { get; set; }
		public string SupplierName { get; set; }
		public string Ean { get; set; }
		public string ArticleNumber { get; set; }
		public int VE { get; set; }
		// public DateTime ProcessedDate { get; set; }
		public DateTime PurchasedDate { get; set; }
		public BsonDocument plus { get; set; }

		public PlatformEntity Platform { get; set; }
		public IncomeEntity Income { get; set; }
		public ItemEntity Item { get; set; }
		public TaxEntity Tax { get; set; }
		public ShippingEntity Shipping { get; set; }
		public PackingEntity Packing { get; set; }
		public EbayOrderType ebayorder { get; set; }


		//public class EbayOrder
		//{
		//	public string OrderID { get; set; }
		//	public DateTime CreatedTime { get; set; }
		//	public string SellerUserID { get; set; }
		//	public Total Total { get; set; }
		//	public TransactionArray TransactionArray { get; set; }

		//}

		//public class Total
		//{
		//	public int CurrencyID { get; set; }
		//	public int Value { get; set; }
		//}

		//public class TransactionArray
		//{
		//	public int QuantityPurchased { get; set; }
		//	public string OrderLineItemID { get; set; }
		//	public TransactionPrice TransactionPrice { get; set; }
		//	public TransactionItem item { get; set; }

		//}

		//public class TransactionPrice
		//{
		//	public int CurrencyID { get; set; }
		//	public double Value { get; set; }
		//}

		//public class TransactionItem
		//{
		//	public string ItemID { get; set; }
		//	public string Title { get; set; }
		//	public string SKU { get; set; }
		//}
	}
}
