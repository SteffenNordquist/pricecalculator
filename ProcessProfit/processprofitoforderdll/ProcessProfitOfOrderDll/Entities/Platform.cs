﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll.Entities
{
    public class Platform : PlatformEntity
    {
        public Platform(string category, PlatformConditions platformConditions, double sellPrice)
        {
            Category = category;
            FeePercentage = Math.Round(platformConditions.DefaultPlatformFeeRate, 2);
            FeeinEuro = Math.Round(FeePercentage / 100 * (sellPrice + 3), 2);
            VariableFixcost = Math.Round(platformConditions.PaymentFeeFix, 2);
            PaymentFeeEuro = 0;
            PaymentFeePercentage = 0;
        }
    }
}
