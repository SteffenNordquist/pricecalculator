﻿using DN_Classes.Conditions;
using DN_Classes.Entities.ProcessProfitOrder;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using eBay.Service.Core.Soap;
using MongoDB.Bson;
using PriceCalculation;
using ProcessProfitOfOrderDll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class EbayCalculatedProfit : EbayCalculatedEntity
	{
		public EbayCalculatedProfit(string agentName, BuyPriceRow buyPriceRow, OrderType ebayOrder, string category, ProductSize productSize, ConditionSet conditions, Shipping shipping, double profit, BsonDocument plus, double ShippingIncome, double ItemPrice)
		{
			string sku = new GetFields(ebayOrder).getSKU();
			SellerSku = sku;
			Agentname = agentName;
			ArticleNumber = buyPriceRow.artnr;
			VE = buyPriceRow.packingUnit == 0 ? 1 : buyPriceRow.packingUnit;
			Ean = new Regex(@"\d{13}").Match(sku).ToString();
			PurchasedDate = ebayOrder.CreatedTime;

			SupplierName = sku.Contains("JTL") ? "JTL"
						 : sku.Contains("SWW") ? "SWW"
						 : buyPriceRow.suppliername;
			this.plus = plus;
			this.Platform = new ProcessProfitOfOrderDll.Entities.Platform(category, conditions.Platform, ItemPrice);
			this.Income = new Income(ShippingIncome, buyPriceRow, conditions.Platform, ItemPrice);
			this.Item = new Item(buyPriceRow, conditions.Profit, (SimpleSize)productSize, profit);
			this.Tax = new Tax(buyPriceRow, ItemPrice);
			this.Shipping = shipping;
			this.Packing = new Packing(productSize);
			//this.EbayOrder = ebayOrder;
			this.EbayOrder = new EbayOrderDetails(ebayOrder);
		}
	}
}
