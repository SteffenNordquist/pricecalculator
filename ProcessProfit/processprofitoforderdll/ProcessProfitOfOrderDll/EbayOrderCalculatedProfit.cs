﻿using DN_Classes.Conditions;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using eBay.Service.Core.Soap;
using PriceCalculation;
using ProcessProfitOfOrderDll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class EbayOrderCalculatedProfit : EbayCalculatedEntityV2
	{
		public EbayOrderCalculatedProfit(string agentName, BuyPriceRow buyPriceRow, OrderType ebayOrder, string category, ProductSize productSize, ConditionSet conditions, Shipping shipping, double profit)
		{
			string sku = new GetFields(ebayOrder).getSKU();
			SellerSku = sku;
			Agentname = agentName;
			ArticleNumber = buyPriceRow.artnr;
			VE = buyPriceRow.packingUnit == 0 ? 1 : buyPriceRow.packingUnit;
			Ean = new Regex(@"\d{13}").Match(sku).ToString();
			PurchasedDate = ebayOrder.CreatedTime;


			SupplierName = sku.Contains("JTL") ? "JTL"
						 : sku.Contains("SWW") ? "SWW"
						 : buyPriceRow.suppliername;


			this.Platform = new ProcessProfitOfOrderDll.Entities.Platform(category, conditions.Platform, ebayOrder.Total.Value);
			this.Income = new Income(3, buyPriceRow, conditions.Platform, ebayOrder.Total.Value);
			this.Item = new Item(buyPriceRow, conditions.Profit, (SimpleSize)productSize, profit);
			this.Tax = new Tax(buyPriceRow, ebayOrder.Total.Value);
			this.Shipping = shipping;
			this.Packing = new Packing(productSize);
			//this.EbayOrder = ebayOrder;

		}
	}
}
