﻿using DN_Classes.Amazon.Orders;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessProfitOfOrderDll
{
    [BsonIgnoreExtraElements]
    public class SingleOrderItem
    {
        public ObjectId id { get; set; }
        public AmazonOrder Order { get; set; }
        public BsonDocument plus { get; set; }

    }
}
