﻿using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Products.Price;
using PriceCalculation;
using ProcessProfitOfOrderDll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class Default_Error
	{
		public CalculatedProfit setDefaulterros(DN_Classes.Amazon.Orders.AmazonOrder amazonOrder)
		{
			string amazonApiError = "amazon api error";
			Shipping shipping = new Shipping(amazonApiError, 0, 0, 0);
			BuyPriceRow emptyBuyPriceRow = new BuyPriceRow(amazonOrder.SellerSKU.ToString(), amazonApiError);

			ConditionSet conditionSet = new CategoryConditions().GetConditionSet(amazonApiError, "Amazon", null);

			var price = 0;

			CalculatedProfit calculatedProfit = new CalculatedProfit(amazonApiError, emptyBuyPriceRow, amazonOrder, amazonApiError, new ProductSize(), conditionSet, shipping, -3.33, -3.33, new MongoDB.Bson.BsonDocument());


			return calculatedProfit;
		}
	}
}
