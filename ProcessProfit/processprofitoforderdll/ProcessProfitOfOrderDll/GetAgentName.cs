﻿
using DN_Classes.Agent;
using DN_Classes.Amazon.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
    public class GetAgentName
    {
        public string GetAgentNameByOrder(string strAgentID)
        {
            string agentName = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(strAgentID))
                {
                    int agentId = 0;
                    int.TryParse(strAgentID, out agentId);
                    agentName = new Agent(agentId).agentEntity.agent.agentname;
                }
            }
            catch
            {
                agentName = "agent not found";
            }
            return agentName;
        }
    }
}
