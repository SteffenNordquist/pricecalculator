﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Logger;
using DN_Classes.Products;
using HtmlAgilityPack;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
    public class GetCategory
    {
        static CategoriesDB categoriesDB = new CategoriesDB();
		public double ShippingCost = 0.00;


        public List<string> GetCategoryByOrderId(string orderId)
        {
            List<string> categories = new List<string>();
            string split = orderId.Split('-')[0].ToString();

            WebClient wc = new WebClient();
			try
			{
				string html = wc.DownloadString(@"http://www.ebay.de/itm/" + split);
				HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
				doc.LoadHtml(html);

				string nodeFormat = ".//td[@class='vi-VR-brumblnkLst vi-VR-brumb-hasNoPrdlnks']/table/tbody/tr/td/h2/ul";//.//ul[@id='bc']/table/tbody/tr/td[@class='vi-VR-brumblnkLst vi-VR-brumb-hasNoPrdlnks']/table/tbody/tr/td/h2/ul/td[@class='bc-w']
				var li = doc.DocumentNode.SelectSingleNode(@"//li[@class='bc-w']");

				//string nodeShippingSummary = @".\\div[@id='shippingSummary']\div[3]\div\span\span\";

				foreach (var singleLI in doc.DocumentNode.SelectNodes(@"//li[@class='bc-w']"))
				{
					categories.Add(singleLI.SelectSingleNode(singleLI.XPath + "//a").Attributes["href"].Value.ToString().Split('/')[5].ToString());
				}

			}
			catch {
				categories.Add("no category");
			}
            return categories;
        }


        public List<string> GetCategoryFromAsinorEan(string eanAsin, string marketPlatform)
        {
            List<string> categoryNames = new List<string>();
            try
            {

                //categoryNames = (marketPlatform.ToLower() == "amazon") ?
                //    categoriesDB.collection.Find(Query.EQ("_id", eanAsin)).SetFields("category").Select(x => x.category).ToList() :
                //    categoriesDB.ebaycollection.Find(Query.EQ("_id", eanAsin)).ToList();

                if (marketPlatform.ToLower() == "amazon")
                {
                    var singleCat = categoriesDB.collection.FindOne(Query.EQ("_id", eanAsin)).category.ToString();

                    categoryNames.Add(singleCat);
                }

                else
                {
                   
                    List<string> getListCat = categoriesDB.ebaycollection.FindOne(Query.EQ("_id", eanAsin)).categories.Select(x => x.categoryText.ToString()).ToList();

                    categoryNames = getListCat;
                }


            }
            catch { }

            if (categoryNames.Count() == 0)
            {
                categoryNames.Add("empty category");
            }

            return categoryNames;
        }


		public double GetShipping(string source, string agentName)
		{
			HtmlDocument document = new HtmlDocument();
			document.LoadHtml(source);

			HtmlNode node = document.GetElementbyId("fshippingCost");

			double shippingCost = agentName.ToUpper() == "Albert Minin".ToUpper() ? 2.9 : 0; ;

			if (node != null)
			{
				string shippingCostText = "0";

				var regex = new System.Text.RegularExpressions.Regex(@"\d+[\.\,]\d+");
				var matches = regex.Matches(node.InnerText);

				if (matches.Count > 0)
				{
					shippingCostText = matches[0].Value;
				}
				else
				{
					Console.WriteLine("Item Does not match");
				}

				//AppLogger.Log(shippingCostText + " extracted from " + node.InnerText);

				double.TryParse(shippingCostText, NumberStyles.Number, CultureInfo.CreateSpecificCulture("de-DE"), out shippingCost);
			}

			return shippingCost;
		}
    }
}
