﻿using DN_Classes.Conditions;
using DN_Classes.Ebay.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace ProcessProfitOfOrderDll
{
    public class CategoryConditions
    {
        public ConditionSet GetConditionSet(string category,string strMarketPlatform, FeeRule feeRule)
        {
            ProfitConditions defaultProfitConditions = new ProfitConditions(0, 1, 0.3);
            ContractConditions contractconditions = new ContractConditions(0, 0, 0);
            PlatformConditions platformConditionCategory = PlatformConditionByCategory.GetPlatformConditionsFromCategory(category, strMarketPlatform, feeRule);

            return new ConditionSet(defaultProfitConditions, contractconditions, platformConditionCategory);
        }
    }
}
