﻿using DN_Classes.Products;
using DN_Classes.Products.Price;
using MongoDB.Driver.Builders;
using PriceCalculation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
    public class ProductMeasurements
    {
		ProductSizeDB productSizesDB = new ProductSizeDB();
		PackingList packingList = new PackingList();
		BuyPriceRow cheapestItem = new BuyPriceRow();
		string ean = "";
		bool fakeSizes = false;

		public ProductMeasurements(ProductSizeDB productSizesDB, string ean, BuyPriceRow cheapestItem)
		{
			this.productSizesDB = productSizesDB;
			this.cheapestItem = cheapestItem;
			this.ean = ean;
		}

		/// <summary>
		///	Get product sizes of an item. Return default values if our database doesnt have any sizes
		/// </summary>
		/// <returns>ProductSize</returns>
        public ProductSize getProductMeasurements()
        {
            int length;
            int width;
            int height;
            int weight;
            ProductSize productmeasurements;

			// default sizes
			length = 100;
			width = 100;
			height = 100;
			weight = 900;

            if (cheapestItem.simpleSize.isValid)
            {
                length = cheapestItem.simpleSize.Length;
                width = cheapestItem.simpleSize.Width;
                height = cheapestItem.simpleSize.Height;
                weight = cheapestItem.simpleSize.Weight;
            }
            else
            {
                var productsizesFromDb = productSizesDB.ProductSizeCollection().FindOne(Query.EQ("ean", ean));
				if (productsizesFromDb != null)
				{
					/// default if item has no measurements.
					if (productsizesFromDb.measure.length == 0 && productsizesFromDb.measure.width == 0 && productsizesFromDb.measure.height == 0 && productsizesFromDb.measure.weight == 0)
					{
						length = 100;
						width = 100;
						height = 100;
						weight = 900;
						this.fakeSizes = true;
					}
					else
					{
						length = productsizesFromDb.measure.length;
						width = productsizesFromDb.measure.width;
						height = productsizesFromDb.measure.height;
						weight = productsizesFromDb.measure.weight;
					}
				}
				else
				{
					this.fakeSizes = true;
				}
				
            }

            productmeasurements = new ProductSize(length.ToString(),
                               width.ToString(),
                              height.ToString(),
                              weight.ToString(),
                                    packingList);
            return productmeasurements;
        }


		/// <summary>
		/// Fake sizes is the default size
		/// </summary>
		/// <returns>bool</returns>
		public bool ModifyIfFakeSizes()
		{
			return this.fakeSizes;
		}

    }
}
