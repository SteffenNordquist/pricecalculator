﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class NotAvailable
	{
		public string SellerSKU { get; set; }
		public int NumberOfDays { get; set; }
		public bool Identifier { get; set; }
	}
}
