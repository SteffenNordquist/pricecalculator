﻿using DN_Classes.Amazon.Orders;
using DN_Classes.Entities;
using EbayOrderDB;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class InsertAndUpdateToDatabase
	{
		public SingleOrderDB singleOrdersDB = new SingleOrderDB();
		public EbayOrderMongoDB ebayOrdersDB = new EbayOrderMongoDB();
		public CalculatedProfitDB cpdb = new CalculatedProfitDB();

		public void SendToDatabase(AmazonOrder singleItem, CalculatedProfit calculatedProfitEntity)
		{
			cpdb.collection.Insert(calculatedProfitEntity);
			singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", singleItem.AmazonOrderId),
								Update.Set("plus.OrderProcessed", DateTime.Now));
		}


		public void ebaySendToDatabase(EbayOrderDB.EbayOrderEntity singleItem, EbayCalculatedProfit calculatedProfitEntity)
		{

			var ebay = new EbayOrderMongoDB().SetCollections();

			cpdb.ebaycollection.Insert(calculatedProfitEntity);
			ebay.Update(Query.EQ("order.OrderID", singleItem.order.OrderID),
								Update.Set("plus.OrderProcessed", DateTime.Now));
		}

		/// <summary>
		/// if the order is unavailable, It update the order to identify for 10days to find the available
		/// </summary>
		/// <param name="orderItem" type="EbayOrderEntity">
		/// Single Amazonorder
		/// </param>
		/// <returns type="void" />
		public void updateItemForNotavailableOrderForEbay(EbayOrderDB.EbayOrderEntity orderItem)
		{
			EbayOrderDB.EbayOrderEntity orderTemplate = new EbayOrderDB.EbayOrderEntity();

			int countdays = 0;

			try
			{
				orderTemplate = ebayOrdersDB.ebayOrderNotAvailable.FindOne(Query.EQ("order.OrderID", orderItem.order.OrderID));
				countdays = orderTemplate.plus["NumberOfDays"].AsInt32 + 1;

				ebayOrdersDB.ebayOrderNotAvailable.Update(Query.EQ("order.OrderID", orderItem.order.OrderID),
										 Update.Set("plus.DateIdentified", DateTime.Now)  ///.Set("plus.NumberOfDays", count)
									   .Set("plus.ItemAvailability", false)
									   .Set("plus.NumberOfDays", countdays));
			}
			catch
			{
				BsonDocument newplus = new BsonDocument();
				newplus["NumberOfDays"] = 1;
				newplus["DateIdentified"] = DateTime.Now;
				newplus["ItemAvailability"] = false;
				newplus["AgentID"] = orderItem.plus["AgentID"].AsInt32;//plus["AgentID"].AsInt32

				orderTemplate = new EbayOrderDB.EbayOrderEntity() { order = orderItem.order, plus = newplus };
				ebayOrdersDB.ebayOrderNotAvailable.Insert(orderTemplate);


			}


		}

		public void updateBugs(EbayOrderDB.EbayOrderEntity orderItem, EbayCalculatedProfit calculatedProfitEntity)
		{
			var ExtractedFields = new GetFields(orderItem.order);

			string sellerSKU = ExtractedFields.getSKU();

			string ean = getEan(ExtractedFields.getTitle(), sellerSKU);

			cpdb.ebaycollection.Update(Query.And(Query.EQ("EbayOrder.OrderID", calculatedProfitEntity.EbayOrder.OrderID),
												Query.EQ("Ean", ean)),
								Update.Set("Platform", calculatedProfitEntity.Platform.ToBsonDocument())
									.Set("Income", calculatedProfitEntity.Income.ToBsonDocument())
									.Set("Item", calculatedProfitEntity.Item.ToBsonDocument())
									.Set("Tax" , calculatedProfitEntity.Tax.ToBsonDocument())
									.Set("Shipping", calculatedProfitEntity.Shipping.ToBsonDocument())
									.Set("Packing",calculatedProfitEntity.Packing.ToBsonDocument())
									.Set("EbayOrder", calculatedProfitEntity.EbayOrder.ToBsonDocument())
									.Set("Agentname", calculatedProfitEntity.Agentname)
									.Set("SellerSku", calculatedProfitEntity.SellerSku)
									.Set("SupplierName", calculatedProfitEntity.SupplierName)
									.Set("Ean",calculatedProfitEntity.Ean)
									.Set("ArticleNumber", calculatedProfitEntity.ArticleNumber)
									.Set("VE", calculatedProfitEntity.VE)
									.Set("PurchasedDate", calculatedProfitEntity.PurchasedDate)
									, UpdateFlags.Multi);

		}

		private string getEan(string title, string sellerSKU)
		{
			string ean = "";

			var RegEx = new Regex(@"\d{13}");//.Match(sellerSKU).ToString();
			//var Match = RegEx.IsMatch(sellerSKU);

			if (RegEx.IsMatch(sellerSKU))
			{
				ean = RegEx.Match(sellerSKU).ToString();
			}
			else if (RegEx.IsMatch(title))
			{
				ean = RegEx.Match(title).ToString();
			}


			return ean;
		}


		public void updateItemForNotavailableOrder(AmazonOrder orderItem)
		{
			SingleOrderItem orderTemplate = new SingleOrderItem();
			int countdays = 0;
			try
			{

				orderTemplate = singleOrdersDB.collection2.FindOne(Query.And(Query.EQ("Order.SellerSKU",orderItem.SellerSKU),Query.EQ("Order.AmazonOrderId", orderItem.AmazonOrderId)));

				countdays = orderTemplate.plus["NumberOfDays"].AsInt32 + 1;

				singleOrdersDB.collection2.Update(Query.And(Query.EQ("Order.SellerSKU",orderItem.SellerSKU),Query.EQ("Order.AmazonOrderId", orderItem.AmazonOrderId)),
										 Update.Set("plus.DateIdentified", DateTime.Now)  ///.Set("plus.NumberOfDays", count)
									   .Set("plus.ItemAvailability", false)
									   .Set("plus.NumberOfDays", countdays), UpdateFlags.Multi);
			}
			catch
			{
				BsonDocument newplus = new BsonDocument();
				newplus["NumberOfDays"] = 1;
				newplus["DateIdentified"] = DateTime.Now;
				newplus["ItemAvailability"] = false;

				orderTemplate = new SingleOrderItem { Order = orderItem, plus = newplus };
				singleOrdersDB.collection2.Insert(orderTemplate);
			}

			#region prev code
			//int count = 0;
			//string sellerSKU = orderItem.SellerSKU.AsString;
			//int numberOfDays = 1;
			//bool Identifier = false;

			//List<NotAvailable> notAvailableList = new List<NotAvailable>();

			//try
			//{
			//	foreach (var item in singleItem.plus["NotAvailableItems"].AsBsonArray)
			//	{
			//		if (item["SellerSKU"].AsString == orderItem.SellerSKU.AsString)
			//		{
			//			numberOfDays = item["NumberOfDays"].AsInt32 + 1;
			//			Identifier = item["Identifier"].AsBoolean;

			//			notAvailableList.Add(new NotAvailable { SellerSKU = orderItem.SellerSKU.AsString, NumberOfDays = numberOfDays, Identifier = Identifier });
			//		}
			//		else
			//		{
			//			notAvailableList.Add(new NotAvailable { SellerSKU = item["SellerSKU"].AsString, NumberOfDays = item["NumberOfDays"].AsInt32, Identifier = item["Identifier"].AsBoolean });
			//		}
			//	}

			//	//numberOfDays = singleItem.plus["NotAvailableItems"].AsBsonArray.Where(x=> x["SellerSKU"].AsString == orderItem.SellerSKU.AsString).First()["NumberOfDays"].AsInt32;
			//}
			//catch
			//{

			//	notAvailableList.Add(new NotAvailable { SellerSKU = orderItem.SellerSKU.AsString, NumberOfDays = numberOfDays, Identifier = false });
			//}


			//try
			//{

			//	count = singleItem.plus["NumberOfDays"].AsInt32;
			//}
			//catch { }


			//var document = new BsonDocument { notAvailableList.ToBsonDocument() };

			//if (count < 10)
			//{
			//	count++;

			//	singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", orderItem.AmazonOrderId),
			//					 Update.Set("plus.DateIdentifier", DateTime.Now)  ///.Set("plus.NumberOfDays", count)
			//						   .Set("plus.ItemAvailable", false)
			//						   .Set("plus.NumberOfDays",count));


			//	//singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", orderItem.AmazonOrderId), Update.Unset("plus.NotAvailableItems"));

			//	////BsonArray newArray = new BsonArray(notAvailableList.ToArray().ToBsonDocument());
			//	//singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", orderItem.AmazonOrderId),
			//	//				 Update.Set("plus.DateIdentifier", DateTime.Now)  ///.Set("plus.NumberOfDays", count)
			//	//					   .Set("plus.ItemAvailable", false));
			//	//					  // .Set("plus.NotAvailableItems", newArray));
			//	//singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId", orderItem.AmazonOrderId),
			//	//			   Update.Push("plus.NotAvailableItems", notAvailableList.ToBsonDocument()));



			//	//var notAvailable = new NotAvailable { SellerSKU = orderItem.SellerSKU.AsString, NumberOfDays = 1 , Identifier = Identifier};
			//	//singleOrdersDB.collection.Update(Query.EQ("Order.AmazonOrderId" , orderItem.AmazonOrderId),
			//	//					Update.push

			//}
			#endregion
		}

		/// <summary>
		/// The unavailable order will be update to available
		/// </summary>
		/// <param name="singleItem" type="AmazonOrder">
		/// Single Amazonorder
		/// </param>
		/// <param name="calculatedProfitEntity" type="CalculatedProfit">
		/// Calculated Item with details
		/// </param>
		/// <returns type="void" />
		public void updateFoundOrder(AmazonOrder singleItem, CalculatedProfit calculatedProfitEntity)
		{

			singleOrdersDB.collection2.Update(Query.EQ("Order.AmazonOrderId", singleItem.AmazonOrderId),
								  Update.Set("plus.ItemFound", true)
										.Set("plus.DateIdentified", DateTime.Now)
										.Set("plus.ItemAvailable", true));

			cpdb.collection.Update(Query.And(Query.EQ("AmazonOrder.AmazonOrderId", calculatedProfitEntity.AmazonOrder.AmazonOrderId),
											 Query.EQ("Ean", calculatedProfitEntity.Ean)),
									Update.Set("Agentname", calculatedProfitEntity.Agentname)
										  .Set("SellerSku", calculatedProfitEntity.SellerSku)
										  .Set("SupplierName", calculatedProfitEntity.SupplierName)
										  .Set("Ean", calculatedProfitEntity.Ean)
										  .Set("ArticleNumber", calculatedProfitEntity.ArticleNumber)
										  .Set("VE", calculatedProfitEntity.VE)
										  .Set("PurchasedDate", calculatedProfitEntity.PurchasedDate)
										  .Set("Platform", calculatedProfitEntity.Platform.ToBsonDocument())
										  .Set("Income", calculatedProfitEntity.Income.ToBsonDocument())
										  .Set("Item", calculatedProfitEntity.Item.ToBsonDocument())
										  .Set("Tax", calculatedProfitEntity.Tax.ToBsonDocument())
										  .Set("Shipping", calculatedProfitEntity.Shipping.ToBsonDocument())
										  .Set("Packing", calculatedProfitEntity.Packing.ToBsonDocument())
										  .Set("AmazonOrder", singleItem.ToBsonDocument())
										  , UpdateFlags.Multi);


		}

		public void updateFoundBugOrder(AmazonOrder singleItem, CalculatedProfit calculatedProfitEntity)
		{

			cpdb.collection.Update(Query.And(Query.EQ("AmazonOrder.AmazonOrderId", calculatedProfitEntity.AmazonOrder.AmazonOrderId),
											 Query.EQ("Ean", calculatedProfitEntity.Ean)),
									Update.Set("Agentname", calculatedProfitEntity.Agentname)
										  .Set("SellerSku", calculatedProfitEntity.SellerSku)
										  .Set("SupplierName", calculatedProfitEntity.SupplierName)
										  .Set("Ean", calculatedProfitEntity.Ean)
										  .Set("ArticleNumber", calculatedProfitEntity.ArticleNumber)
										  .Set("VE", calculatedProfitEntity.VE)
										  .Set("PurchasedDate", calculatedProfitEntity.PurchasedDate)
										  .Set("Platform", calculatedProfitEntity.Platform.ToBsonDocument())
										  .Set("Income", calculatedProfitEntity.Income.ToBsonDocument())
										  .Set("Item", calculatedProfitEntity.Item.ToBsonDocument())
										  .Set("Tax", calculatedProfitEntity.Tax.ToBsonDocument())
										  .Set("Shipping", calculatedProfitEntity.Shipping.ToBsonDocument())
										  .Set("Packing", calculatedProfitEntity.Packing.ToBsonDocument())
										  .Set("AmazonOrder", singleItem.ToBsonDocument())
										  , UpdateFlags.Multi);


		}

		public void updateEbayFoundOrder(EbayOrderDB.EbayOrderEntity ebayOrderEntity, EbayCalculatedProfit ebayCalculatedProfit)
		{
			var orders = new EbayOrderMongoDB();

			orders.ebayOrderNotAvailable.Update(Query.EQ("order.OrderID",ebayOrderEntity.order.OrderID),
				 Update.Set("plus.NumberOfDays", 10)
										.Set("plus.DateIdentified", DateTime.Now)
										.Set("plus.ItemAvailable", true));

			
			cpdb.ebaycollection.Update(Query.And(Query.EQ("EbayOrder.OrderID", ebayCalculatedProfit.EbayOrder.OrderID),
											 Query.EQ("Ean", ebayCalculatedProfit.Ean)),
									Update.Set("Agentname", ebayCalculatedProfit.Agentname)
										  .Set("SellerSku", ebayCalculatedProfit.SellerSku)
										  .Set("SupplierName", ebayCalculatedProfit.SupplierName)
										  .Set("Ean", ebayCalculatedProfit.Ean)
										  .Set("ArticleNumber", ebayCalculatedProfit.ArticleNumber)
										  .Set("VE", ebayCalculatedProfit.VE)
										  .Set("PurchasedDate", ebayCalculatedProfit.PurchasedDate)
										  .Set("Platform", ebayCalculatedProfit.Platform.ToBsonDocument())
										  .Set("Income", ebayCalculatedProfit.Income.ToBsonDocument())
										  .Set("Item", ebayCalculatedProfit.Item.ToBsonDocument())
										  .Set("Tax", ebayCalculatedProfit.Tax.ToBsonDocument())
										  .Set("Shipping", ebayCalculatedProfit.Shipping.ToBsonDocument())
										  .Set("Packing", ebayCalculatedProfit.Packing.ToBsonDocument())
										  .Set("EbayOrder", ebayCalculatedProfit.EbayOrder.ToBsonDocument())
										  , UpdateFlags.Multi);
			//throw new NotImplementedException();
		}

		public Dictionary<string, List<string>> getAllAvailableIDsOrdersForAmazon()
		{
			Dictionary<string, List<string>> allOrders = new Dictionary<string, List<string>>();

			var allPossibleItems = cpdb.collection.Find(Query.EQ("SupplierName", "not available")).SetFields("SellerSku", "AmazonOrder.AmazonOrderId", "PurchasedDate").Where(x => DateTime.Now.AddDays(-30).Date < x.PurchasedDate.Date).ToList();

			foreach (var item in allPossibleItems.GroupBy(x => x.AmazonOrder.AmazonOrderId))
			{
				List<string> skus = new List<string>();

				skus = item.Select(x => x.SellerSku).ToList();

				allOrders.Add(item.Key, skus);
			}

			return allOrders;
		}


		public Dictionary<string, List<string>> getAllOrderIDAvailableOrdersForEbay()
		{
			Dictionary<string, List<string>> allOrders = new Dictionary<string, List<string>>();

			var allPossibleItems = cpdb.ebaycollection.Find(Query.EQ("SupplierName", "not available")).SetFields("SellerSku", "EbayOrder.OrderID", "PurchasedDate").Where(x => DateTime.Now.AddDays(-30).Date < x.PurchasedDate.Date).ToList();

			foreach (var item in allPossibleItems.GroupBy(x => x.EbayOrder.OrderID))
			{
				List<string> skus = new List<string>();

				skus = item.Select(x => x.SellerSku).ToList();

				allOrders.Add(item.Key, skus);
			}

			return allOrders;
		}

	}
}
