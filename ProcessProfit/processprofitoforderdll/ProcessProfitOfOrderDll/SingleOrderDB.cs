﻿using DN_Classes.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using WhereToBuy;

namespace ProcessProfitOfOrderDll
{
	public class SingleOrderDB
	{

		string ConnectionString = Properties.Settings.Default.DBConnection;
		MongoClient Client = null;
		MongoServer Server = null;
		MongoDatabase Database = null;

		public MongoCollection<SingleOrderItem> collection = null;
		public MongoCollection<SingleOrderItem> collection2 = null;
		public CalculatedProfitDB cpdb = new CalculatedProfitDB();
		public List<string> excludedSuppliers = new List<string>();

		MongoClient Client2 = null;
		MongoServer Server2 = null;
		MongoDatabase Database2 = null;
		public int sampleCount = 0;


		public SingleOrderDB()
		{
			Client = new MongoClient(ConnectionString);
			Server = Client.GetServer();
			Database = Server.GetDatabase("Orders");
			collection = Database.GetCollection<SingleOrderItem>("SingleOrderItems");

			excludedSuppliers = ExcludedSuppliers.GetExcludedSupplierNames().ConvertAll(x => x.ToUpper());


			Client2 = new MongoClient(ConnectionString);
			Server2 = Client2.GetServer();
			Database2 = Server2.GetDatabase("Orders");
			collection2 = Database2.GetCollection<SingleOrderItem>("AmazonUnavailableOrderItems");
		}

		/// <summary>
		/// Gets Processed unavailable items but not calculated. 
		/// </summary>
		/// <returns></returns>
		public List<SingleOrderItem> GetUnAvailableItemsForAmazon()
		{
			List<SingleOrderItem> allDocuments = new List<SingleOrderItem>();
			IMongoQuery notAvailableQuery = Query.And(Query.NE("Order.OrderStatus", "Pending"),
												  Query.NE("Order.OrderStatus", "Canceled"),
												  Query.Exists("plus.DateIdentified"),
												  Query.LT("plus.NumberOfDays", 10),
												  Query.EQ("plus.ItemAvailability", false));

			allDocuments = collection2.Find(notAvailableQuery).Where(x => filterOrder(x)).ToList();

			//allDocuments = allDocuments.Where(x => filterSupplier(identifyIfArrayOrString(x))).ToList();
			#region Testing
			//IMongoQuery tempQuery = Query.And(
			//                                    Query.NE("Order.OrderStatus", "Pending"),
			//                                     Query.NE("Order.OrderStatus", "Canceled"),
			//                                     Query.Or(Query.Matches("Order.SellerSKU", BsonRegularExpression.Create(new Regex("^WUR"))),
			//                                              Query.Matches("Order.SellerSKU", BsonRegularExpression.Create(new Regex("^JOB")))));

			//allDocuments = collection.Find(tempQuery).ToList();

			#endregion

			return allDocuments;
		}

		private bool filterOrder(SingleOrderItem x)
		{
			///DateIdentifier
			try
			{
				if (DateTime.Now.Date != x.plus["DateIdentified"].AsDateTime.Date && x.plus["NumberOfDays"].AsInt32 != 10 /*&& x.plus["ItemAvailability"].AsBoolean != true*/)
				{
					return true;
				}
			}
			catch
			{
				return true;
			}


			return false;
		}

		public List<SingleOrderItem> GetCanceledProcessedOrders()
		{
			IMongoQuery firstQuery = Query.And(Query.EQ("Order.OrderStatus", "Canceled"),
												 Query.Exists("plus.OrderProcessed")
												);

			var allDocuments = collection.Find(firstQuery).ToList();

			return allDocuments;
		}

		/// <summary>
		/// Gets all fetched orders in Amazon to be calculated. (Not splitted)
		/// </summary>
		/// <returns></returns>
		public List<SingleOrderItem> GetOrders()
		{
			IMongoQuery firstQuery = Query.And(Query.NE("Order.OrderStatus", "Pending"),
												 Query.NE("Order.OrderStatus", "Canceled"),
												 Query.NE("Order.FulfillmentChannel","AFN"),
												 Query.NotExists("plus.OrderProcessed")
												);

			var allDocuments = collection.Find(firstQuery).ToList();//.Where(x => filterSupplier(identifyIfArrayOrString(x))).ToList();

			return allDocuments;
		}


		public List<SingleOrderItem> sampleOrders(List<string> tobedeleted)
		{
			List<SingleOrderItem> sample = new List<SingleOrderItem>();
			#region Sample List
			sample = collection.Find(Query.Or(Query.EQ("Order.AmazonOrderId", "306-2659934-2938731"))).ToList();
				//Query.EQ("Order.AmazonOrderId", "304-9838494-6656368"),
				//Query.EQ("Order.AmazonOrderId", "303-3709909-2808365"),
										//Query.EQ("Order.AmazonOrderId", "305-2152933-3475544"))).ToList();
			//							//Query.EQ("Order.AmazonOrderId", "302-9797154-5780354"),
			//							//Query.EQ("Order.AmazonOrderId", "305-1333306-6597122"),
			//							//Query.EQ("Order.AmazonOrderId", "305-2100669-5971526"),
			//							//Query.EQ("Order.AmazonOrderId", "028-8967901-5517956"),
			//							//Query.EQ("Order.AmazonOrderId", "028-9475625-6575564"),
			//							//Query.EQ("Order.AmazonOrderId", "028-3355404-9301932"),
			//							//Query.EQ("Order.AmazonOrderId", "303-8392072-4151525"),
			//							//Query.EQ("Order.AmazonOrderId", "303-0081743-7875561"),
			//							//Query.EQ("Order.AmazonOrderId", "304-4822565-7169969"),
			//							//Query.EQ("Order.AmazonOrderId", "028-1573012-3573159"),
			//							//Query.EQ("Order.AmazonOrderId", "028-8876720-8173922"),
			//							//Query.EQ("Order.AmazonOrderId", "302-5740659-4430751"),
			//							//Query.EQ("Order.AmazonOrderId", "304-3753707-2381108"),
			//							//Query.EQ("Order.AmazonOrderId", "303-1075918-0870735"),
			//							//Query.EQ("Order.AmazonOrderId", "303-9580181-7162715"),
			//							//Query.EQ("Order.AmazonOrderId", "304-7536491-4459536"),
			//							//Query.EQ("Order.AmazonOrderId", "028-3927745-0663528"),
			//							//Query.EQ("Order.AmazonOrderId", "028-6101111-1270721"),
			//							//Query.EQ("Order.AmazonOrderId", "028-6188119-6559503"),
			//							//Query.EQ("Order.AmazonOrderId", "306-6771709-8418700"),
			//							//Query.EQ("Order.AmazonOrderId", "303-4914898-9874718"),
			//							//Query.EQ("Order.AmazonOrderId", "304-6481236-8243515"),
			//							//Query.EQ("Order.AmazonOrderId", "028-5181944-4689902"),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId", ""),
			//							//Query.EQ("Order.AmazonOrderId3942", ""),
			//							Query.EQ("Order.AmazonOrderId", "305-5906838-2024350")
			//							)).ToList();
			//							Query.EQ("Order.AmazonOrderId", "028-8274266-0939557"),
			//							Query.EQ("Order.AmazonOrderId", "302-0468211-7593908"),
			//							Query.EQ("Order.AmazonOrderId", "303-9352579-5222728"),
			//							Query.EQ("Order.AmazonOrderId", "303-3536173-0270703"),
			//							Query.EQ("Order.AmazonOrderId", "028-2725798-4251527"),
			//							Query.EQ("Order.AmazonOrderId", "304-8340124-4161131"),
			//							Query.EQ("Order.AmazonOrderId", "302-9527709-5134736"),
			//							Query.EQ("Order.AmazonOrderId", "305-0589961-8197103"),


			//							Query.EQ("Order.AmazonOrderId", "028-1740134-0567567"),

			//							Query.EQ("Order.AmazonOrderId", "306-1510379-5360339"),
			//							Query.EQ("Order.AmazonOrderId", "302-2497787-9906714"),
			//							Query.EQ("Order.AmazonOrderId", "303-6179636-5221165"),

			//							Query.EQ("Order.AmazonOrderId", "302-9640968-7440353")
			//							)).ToList();
			#endregion

			//foreach (var item in tobedeleted)
			//{
			//	sample.Add(collection.FindOne(Query.EQ("Order.AmazonOrderId", item)));
			//}
			sample = collection.Find(Query.EQ("Order.AmazonOrderId", "305-9214212-1811564")).ToList();

			return sample;

		}

		private string GetProperSKU(BsonValue bsonValue)
		{
			string returnthis = "";
			if (bsonValue.IsString)
			{
				returnthis = bsonValue.AsString;
			}
			if (bsonValue.IsBsonArray)
			{
				returnthis = bsonValue.AsBsonArray.First().AsString;
			}

			return returnthis;
		}


		private DateTime GetProperDatetime(string date)
		{
			DateTime dt = new DateTime();

			if (date.Contains("."))
			{
				dt = DateTime.Parse(date, new CultureInfo("de-DE"));

			}
			else
			{
				dt = DateTime.ParseExact(date, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
			}

			if (dt.Date == DateTime.Now.AddDays(-1).Date)
			{
				sampleCount++;
				Console.WriteLine(dt.Date + "\t Test");
			}
			return dt;
		}

		private bool aorstr(SingleOrderItem x)
		{
			string sellerSKU = "";

			sellerSKU = (x.Order.SellerSKU.IsBsonArray) ? x.Order.SellerSKU.AsBsonArray.First().ToString() : x.Order.SellerSKU.ToString();


			string purchaseDate = x.Order.PurchaseDate.ToString();

			DateTime PurchasedDate = new DateTime();

			PurchasedDate = GetProperDatetime(purchaseDate);

			if (sellerSKU.StartsWith("JPC") && PurchasedDate.Date == DateTime.Now.AddDays(-2).Date)
			{
				return true;
			}

			return false;
		}

		private string identifyIfArrayOrString(SingleOrderItem x)
		{
			string sku = "";

			if (x.Order.SellerSKU.IsBsonArray)
			{
				sku = x.Order.SellerSKU.AsBsonArray.First().ToString();
			}
			else
			{
				sku = x.Order.SellerSKU.AsString.ToString();
			}

			TextCleaner tc = new TextCleaner();
			return tc.getSuppliernameFromPrefixOfSellerSKU(sku);
		}



		public bool checkOrder(SingleOrderItem x, int index)
		{
			var findOrder = cpdb.collection.Find(Query.EQ("AmazonOrder.AmazonOrderId", x.Order.AmazonOrderId.ToString())).Count();
			bool modify = findOrder == 0 ? true : false;

			Console.WriteLine(string.Format("{0}\t{1}\t{2}", x.Order.AmazonOrderId.ToString(), index, modify));

			return modify;
		}



	}
}