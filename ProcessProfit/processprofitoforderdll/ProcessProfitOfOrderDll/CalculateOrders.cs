﻿using DN_Classes.Amazon.Orders;
using DN_Classes.AppStatus;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using DN_Classes.Rules.StockRules;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using PriceCalculation;
using PriceCalculation.Shipping;
using ProcessProfitOfOrderDll.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WhereToBuy;
 

namespace ProcessProfitOfOrderDll
{
    public class CalculateOrders
    {
        static ProductSizeDB productSizesDB = new ProductSizeDB();

		/// <summary>
		/// Calculate items each splitted items.
		/// </summary>
		/// <param name="totalOrders"></param>
		/// <returns>Dictionary<AmazonOrder, CalculatedProfit></returns>
        public Dictionary<AmazonOrder, CalculatedProfit> ProcessAllItems(List<AmazonOrder> totalOrders)
        {
			List<string> excludedSuppliersFromOrder = new List<string>();
			excludedSuppliersFromOrder = ExcludedSuppliers.GetExcludedSupplierNames();
			

            Dictionary<AmazonOrder, CalculatedProfit> allCalculatedOrders = new Dictionary<AmazonOrder, CalculatedProfit>();
            using (ApplicationStatus appStatus = new ApplicationStatus("ProcessProfit_Amazon_148"))
            {
				
                foreach (var singleItem in totalOrders)
                {
					BsonDocument plus = new BsonDocument();

					string availability = "(Available)";
                    string strMarketPlatform = "Amazon";

					// Amazon finds the category by asin.
                    List<string> category = new GetCategory().GetCategoryFromAsinorEan(singleItem.ASIN.AsString, strMarketPlatform);

					// fee rule is not aplicable 
                    ConditionSet conditionSet = new CategoryConditions().GetConditionSet(category.First(), strMarketPlatform,null);

                    string ean = new Regex(@"\d{13}").Match(singleItem.SellerSKU.AsString).ToString();

                    string agentName = new GetAgentName().GetAgentNameByOrder(singleItem.AgentId);

                    try
                    {
						// get the cheapest item from all our databases. 
						var cheapestItem = new CheapestPrice().GetCheapestPriceByEan(ean, singleItem.ASIN.AsString);

						if (excludedSuppliersFromOrder.ConvertAll(x => x.ToUpper()).ToList().Contains(cheapestItem.suppliername.ToUpper()))
						{
							plus["excludeFromOrdering"] = true;
							throw new ItemNotAvailableException("Item " + ean + " is not available");
						}



						bool fixPrice = cheapestItem.fixedPrice == "04" ? true : false;

                        #region Product Measurements
						ProductMeasurements productMeasurements = new ProductMeasurements(productSizesDB,
                                                                                    ean,
																					cheapestItem);

						ProductSize productExtractedMeasurements = productMeasurements.getProductMeasurements();

						
						// from the product measurements
						plus["FakeSizes"] = productMeasurements.ModifyIfFakeSizes();
						plus["ShippingPrice"] = Convert.ToDouble(singleItem.ShippingPrice["Amount"].ToDouble());
 
						#endregion

                        #region Shipping Cost Variation
						// Ship cost by the Price and Product size. 
						var shippingCostVariation = new ShippingCostFinder(productExtractedMeasurements,
                                                                                singleItem.GetItemPrice(),
                                                                                fixPrice)
                                                                                .getShippingVariation();

                        #endregion

                        #region Shipping
                        Shipping shipping = new Shipping(shippingCostVariation.name,
                                                             shippingCostVariation.price,
                                                             conditionSet.Profit.ItemHandlingCosts,
															 productExtractedMeasurements.packing.netPrice);
                        #endregion

                        #region Set all required Entity for Product
						Product product = new Product(singleItem,
                                                        conditionSet,
														cheapestItem,
														productExtractedMeasurements,
                                                        shippingCostVariation.price);
                        #endregion

						// Gets the possible Profit by Item Price and by shipping income. 
                        double overAllProfit = product.getProfit(singleItem.GetItemPrice(), 3);

                        #region Set Calculated Profit Entity
                        CalculatedProfit calculatedProfitEntity = new CalculatedProfit(agentName,
																						cheapestItem,
                                                                                        singleItem,
                                                                                        category.First(),
																						productExtractedMeasurements,
                                                                                        conditionSet,
                                                                                        shipping,
                                                                                        overAllProfit,
                                                                                        singleItem.GetItemPrice(),
																						plus);
          				
                        #endregion

						
                        allCalculatedOrders.Add(singleItem, calculatedProfitEntity);


                    }
                    catch (Exception ex)
                    {
						if (ex is NoProductFoundException || ex is ItemNotAvailableException)
						{
							#region Set Default "Not Available" values

							Shipping shipping = new Shipping("not available", 0, 0, 0);
							BuyPriceRow emptyBuyPriceRow = new BuyPriceRow(singleItem.SellerSKU.ToString());
							CalculatedProfit calculatedProfit = new CalculatedProfit(agentName, emptyBuyPriceRow, singleItem, category.First(), new ProductSize(), conditionSet, shipping, -3.33, singleItem.GetItemPrice(), plus);
							availability = "(Not Available)";
							new InsertAndUpdateToDatabase().updateItemForNotavailableOrder(singleItem);
							#endregion

							allCalculatedOrders.Add(singleItem, calculatedProfit);
						}
						else
						{
							appStatus.AddMessagLine(ex.Message.ToString());

						}
                    }


					Console.WriteLine("Processed " + singleItem.AmazonOrderId + " order " + availability);
                }
				appStatus.AddMessagLine("Success");
                appStatus.Successful();
            }

            return allCalculatedOrders;
        }


    }
}
