﻿
using DN_Classes.Amazon.Orders;
using DN_Classes.Conditions;
using DN_Classes.Entities;
using DN_Classes.Products;
using DN_Classes.Products.Price;
using MongoDB.Bson;
using PriceCalculation;
using ProcessProfitOfOrderDll.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ProcessProfitOfOrderDll
{
	/// <summary>
	/// Sets all calculated item.
	/// </summary>
	public class CalculatedProfit : CalculatedProfitEntity
	{
		public CalculatedProfit(string agentName, BuyPriceRow buyPriceRow, AmazonOrder amazonOrder, string category, ProductSize productSize, ConditionSet conditions, Shipping shipping, double profit, double itemPrice,BsonDocument plus)
		{
			Agentname = agentName;

			VE = buyPriceRow.packingUnit == 0 ? 1 : buyPriceRow.packingUnit;

			try
			{
				Ean = new Regex(@"\d{13}").Match(amazonOrder.SellerSKU.AsString).ToString();
			}
			catch { Ean = "Amazon Error"; }

			SetPurchaseDate(amazonOrder.PurchaseDate);
			SellerSku = amazonOrder.SellerSKU.AsString; //buyPriceRow.sku;

			ArticleNumber = (buyPriceRow.artnr == "not available") ?
							(SellerSku.Contains("SWW") ? buyPriceRow.artnr.Split('_').Last()
														: buyPriceRow.artnr)
							: buyPriceRow.artnr;

			//SupplierName = SellerSku.Split('-').First().Contains("JTL") ? "JTL"
			//	: SellerSku.Split('-').First().Contains("SWW") || SellerSku.Contains("_") ? (SellerSku.Split('_').First().Contains("SWW") ? "SWW" : buyPriceRow.suppliername)
			//	: buyPriceRow.suppliername;

			
			// SWW has different SKU
			SupplierName = SetProperSupplierName(SellerSku, buyPriceRow.suppliername);
			this.plus = plus;
			this.Platform = new ProcessProfitOfOrderDll.Entities.Platform(category, conditions.Platform, itemPrice);
			this.Income = new Income(3, buyPriceRow, conditions.Platform, itemPrice);
			this.Item = new Item(buyPriceRow, conditions.Profit, (SimpleSize)productSize, profit);
			this.Tax = new Tax(buyPriceRow, itemPrice);
			this.Shipping = shipping;
			this.Packing = new Packing(productSize);
			this.AmazonOrder = amazonOrder;

		}

		private string SetProperSupplierName(string name, string buypriceSupplierName)
		{
			string suppliername = buypriceSupplierName;

			if (SellerSku.Split('-').First() == "JTL")
			{
				suppliername = "JTL";
			}

			if (SellerSku.Split('-').First() == "SWW")
			{
				suppliername = "SWW";
			}
			else
			{
				if (SellerSku.Contains('_'))
				{
					suppliername = "SWW";
				}
			}

			return suppliername;
		}

		/// <summary>
		/// Filter the right date
		/// </summary>
		/// <param name="purchaseDate"></param>
		private void SetPurchaseDate(string purchaseDate)
		{
			if (purchaseDate.Contains("."))
			{
				PurchasedDate = DateTime.Parse(purchaseDate, new CultureInfo("de-DE"));
			}
			else
			{
				//PurchasedDate = DateTime.ParseExact(purchaseDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);

				if (purchaseDate.Contains("AM") || purchaseDate.Contains("PM"))
				{
					PurchasedDate = DateTime.ParseExact(purchaseDate, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
				}
				else
				{
					PurchasedDate = DateTime.ParseExact(purchaseDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
				}
			}
		}
	}
}
