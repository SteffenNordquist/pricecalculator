﻿using DN_Classes.Entities.ProcessProfitOrder;
using eBay.Service.Core.Soap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class EbayOrderDetails : EbayOrderType
	{
		private EbayOrderType ebayOrderType;

		public EbayOrderDetails(OrderType ebayOrder)
		{
			this.OrderID = ebayOrder.OrderID;
			this.CreatedTime = ebayOrder.CreatedTime;

			TotalPrice totalPrice = new TotalPrice();
			totalPrice.CurrencyID = Convert.ToInt32(ebayOrder.Total.currencyID);
			totalPrice.Value = Convert.ToDouble(ebayOrder.Total.Value);

			this.Total = totalPrice;
			this.SellerUserID = ebayOrder.SellerUserID;
			this.SellerEmail = ebayOrder.SellerEmail;

			transactionArray singletransaction = new transactionArray();
			foreach (dynamic trans in ebayOrder.TransactionArray)
			{
				singletransaction.QuantityPurchased = trans.QuantityPurchased;
				singletransaction.OrderLineItemID = trans.OrderLineItemID;

				TransactionPrice tp = new TransactionPrice();
				tp.currencyID = Convert.ToInt32(trans.TransactionPrice.currencyID);
				tp.Value = Convert.ToDouble(trans.TransactionPrice.Value);
				singletransaction.TransactionPrice = tp;

				item item = new item();
				item.ItemID = trans.Item.ItemID;
				item.SKU = trans.Item.SKU;
				item.Title = trans.Item.Title;
				singletransaction.Item = item;

				break;
			}
			List<transactionArray> transactionArray = new List<transactionArray>();

			transactionArray.Add(singletransaction);

			this.TransactionArray = transactionArray.ToArray();
		}

	
	}
}
