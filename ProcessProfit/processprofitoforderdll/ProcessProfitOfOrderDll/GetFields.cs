﻿using eBay.Service.Core.Soap;
using EbayOrderDB;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessProfitOfOrderDll
{
	public class GetFields
	{
		OrderType singleItem = null;

		public GetFields(OrderType singleItem)
		{
			this.singleItem = singleItem;
		}

		public string getSKU()
		{
			TransactionTypeCollection orderTrans = this.singleItem.TransactionArray;

			string strSKU = "sku not available";

			foreach (var transaction in orderTrans)
			{
				dynamic dyTransaction = new ExpandoObject();
				dyTransaction = transaction;

				dynamic dyItem = new ExpandoObject();
				dyItem = dyTransaction.Item;

				strSKU = dyItem.SKU;
			}

			return strSKU;
		}

		public int getQuantity(EbayOrderEntity singleitem)
		{
			TransactionTypeCollection orderTrans = singleitem.order.TransactionArray;

			int quantity = 0;


			foreach (var item in orderTrans)
			{
				dynamic getTransactionTemp = new ExpandoObject();
				getTransactionTemp = item;
				quantity = getTransactionTemp.QuantityPurchased;

				break;
			}
			return quantity;
		}

		public string getOrderIdByItem()
		{
			string orderId = "";

			TransactionTypeCollection orderTrans = this.singleItem.TransactionArray;

			foreach (dynamic item in orderTrans)
			{
				orderId = item.OrderLineItemID;
				break;
			}

			return orderId;
		}

		public int CountTransaction()
		{
			int count = 0;

			TransactionTypeCollection orderTrans = this.singleItem.TransactionArray;

			foreach (dynamic item in orderTrans)
			{
				count++;
			}

			return count;
		}

		public double getItemPerPrice()
		{
			double price = 0;

			TransactionTypeCollection orderTrans = this.singleItem.TransactionArray;

			foreach (dynamic item in orderTrans)
			{
				price = item.TransactionPrice.Value;
				break;
			}

			return price;
		}

		public string getCountryName()
		{
			string countryname = "";
			dynamic dyTransaction = new ExpandoObject();
			dyTransaction = this.singleItem.ShippingAddress.CountryName;

			return countryname;

		}


		public string getTitle()
		{
			TransactionTypeCollection orderTrans = this.singleItem.TransactionArray;

			string title = "";

			foreach (var transaction in orderTrans)
			{
				dynamic dyTransaction = new ExpandoObject();
				dyTransaction = transaction;

				dynamic dyItem = new ExpandoObject();
				dyItem = dyTransaction.Item;

				title = dyItem.Title;
			}

			return title;
		}

	}
}
