﻿using DN_Classes.Amazon.Orders;
using ProcessProfitOfOrderDll;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingOrders
{
    class Program
    {
        static SingleOrderDB singleOrdersDB = new SingleOrderDB();
        static CalculateOrders processOrders = new CalculateOrders();

        static void Main(string[] args)
        {
            List<AmazonOrder> totalOrders = GetSingleOrderItems();

            processOrders.ProcessAllItems(totalOrders);
        }

        private static List<AmazonOrder> GetSingleOrderItems()
        {
            var orders = singleOrdersDB.GetOrders();
            Console.WriteLine("Number of items that need to calculate: " + orders.Count().ToString());

            var singleOrder = orders.Where(x => x.Order.ASIN.IsString).ToList();
            var multiorders = orders.Where(x => x.Order.ASIN.IsBsonArray).ToList();

            var totalOrdersTemp = singleOrder.Select(x => x.Order).ToList();
            totalOrdersTemp.AddRange(SplitMultiOrders(multiorders));

            List<AmazonOrder> totalOrders = SplitQuantity(totalOrdersTemp);
            return totalOrders;
        }

        private static List<AmazonOrder> SplitMultiOrders(List<SingleOrderItem> multiorders)
        {
            List<AmazonOrder> totalOrdersTemp = new List<AmazonOrder>();
            foreach (var multiOrder in multiorders)
            {
                int count = multiOrder.Order.ASIN.AsBsonArray.Count();

                AmazonOrder amazonOrder = multiOrder.Order;
                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        AmazonOrder amazonOrderTemplate = (AmazonOrder)multiOrder.Order.Clone();

                        #region

                        amazonOrderTemplate.ASIN = multiOrder.Order.ASIN.AsBsonArray[i];
                        amazonOrderTemplate.SellerSKU = multiOrder.Order.SellerSKU.AsBsonArray[i];
                        amazonOrderTemplate.OrderItemId = multiOrder.Order.OrderItemId.AsBsonArray[i];
                        amazonOrderTemplate.Title = multiOrder.Order.Title.AsBsonArray[i];
                        amazonOrderTemplate.QuantityOrdered = multiOrder.Order.QuantityOrdered.AsBsonArray[i];
                        amazonOrderTemplate.QuantityShipped = multiOrder.Order.QuantityShipped.AsBsonArray[i];
                        amazonOrderTemplate.ItemPrice = multiOrder.Order.ItemPrice.AsBsonValue[i];
                        amazonOrderTemplate.ShippingPrice = multiOrder.Order.ShippingPrice.AsBsonValue[i];
                        amazonOrderTemplate.GiftWrapPrice = multiOrder.Order.GiftWrapPrice.AsBsonValue[i];
                        amazonOrderTemplate.ItemTax = multiOrder.Order.ItemTax.AsBsonValue[i];
                        amazonOrderTemplate.ShippingTax = multiOrder.Order.ShippingTax.AsBsonValue[i];
                        amazonOrderTemplate.GiftWrapTax = multiOrder.Order.GiftWrapTax.AsBsonValue[i];
                        amazonOrderTemplate.ShippingDiscount = multiOrder.Order.ShippingDiscount.AsBsonValue[i];
                        amazonOrderTemplate.PromotionDiscount = multiOrder.Order.PromotionDiscount.AsBsonValue[i];
                        amazonOrderTemplate.ConditionNote = multiOrder.Order.ConditionNote.AsBsonValue[i];
                        amazonOrderTemplate.ConditionId = multiOrder.Order.ConditionId.AsBsonValue[i];
                        amazonOrderTemplate.ConditionSubtypeId = multiOrder.Order.ConditionSubtypeId.AsBsonValue[i];

                        #endregion

                        totalOrdersTemp.Add(amazonOrderTemplate);
                    }
                    catch { }
                }
            }
            return totalOrdersTemp;
        }

        private static List<AmazonOrder> SplitQuantity(List<AmazonOrder> totalOrdersTemp)
        {
            List<AmazonOrder> totalOrders = new List<AmazonOrder>();
            foreach (var order in totalOrdersTemp)
            {
                try
                {
                    int quantity = int.Parse(order.QuantityOrdered.AsString);
                    var newItemPrice = double.Parse(order.ItemPrice["Amount"].ToString(), CultureInfo.InvariantCulture) / quantity;

                    for (int i = 0; i < quantity; i++)
                    {
                        var orderClone = (AmazonOrder)order.Clone();
                        orderClone.QuantityOrdered = 1;
                        orderClone.ItemPrice = newItemPrice;

                        totalOrders.Add(orderClone);
                    }
                }
                catch { }
            }

            return totalOrders;
        }
    }
}
